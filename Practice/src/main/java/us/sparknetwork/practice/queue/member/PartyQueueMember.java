package us.sparknetwork.practice.queue.member;

import com.shawckz.ipractice.party.Party;

/**
 * Created by 360 on 9/13/2015.
 */
public interface PartyQueueMember {

    Party getParty();

}
