package us.sparknetwork.practice.match;

import java.util.Set;

import com.shawckz.ipractice.ladder.Ladder;
import com.shawckz.ipractice.match.handle.MatchManager;
import com.shawckz.ipractice.player.IPlayer;

import org.bukkit.entity.Player;
import us.sparknetwork.practice.ladder.Ladder;
import us.sparknetwork.practice.match.handle.MatchManager;
import us.sparknetwork.practice.player.IPlayer;

public interface PracticeMatch {

    String getId();

    void startMatch(MatchManager matchManager);

    void endMatch();

    boolean isStarted();

    boolean isOver();

    Set<Player> getPlayers();

    MatchType getType();

    Ladder getLadder();

    String getOpponent(IPlayer player);

}
