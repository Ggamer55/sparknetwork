package us.sparknetwork.practice.player;

public enum PlayerState {

    AT_SPAWN,
    BUILDING_KIT,
    IN_MATCH,
    SPECTATING_MATCH

}
