package us.sparknetwork.practice.serial;

import com.shawckz.ipractice.configuration.AbstractSerializer;
import org.bukkit.Material;
import us.sparknetwork.practice.configuration.AbstractSerializer;

public class MaterialSerializer extends AbstractSerializer<Material> {

    @Override
    public String toString(Material data) {
        return data.toString();
    }

    @Override
    public Material fromString(Object data) {
        return Material.valueOf(((String)data).toUpperCase());
    }
}
