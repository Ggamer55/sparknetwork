package us.sparknetwork.practice.spawn.item;

import com.shawckz.ipractice.player.IPlayer;
import us.sparknetwork.practice.player.IPlayer;

public interface SpawnItemAction {

    void onClick(IPlayer player);

}
