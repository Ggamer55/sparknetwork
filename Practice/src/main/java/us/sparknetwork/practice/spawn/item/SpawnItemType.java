package us.sparknetwork.practice.spawn.item;

public enum SpawnItemType {

    NORMAL,
    PARTY,
    SPECTATOR

}
