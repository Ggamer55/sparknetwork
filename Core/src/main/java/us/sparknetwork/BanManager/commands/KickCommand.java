package us.sparknetwork.BanManager.commands;

import org.bukkit.command.CommandSender;

import us.sparknetwork.BanManager.BanManager;
import us.sparknetwork.api.bans.BansAPI.Type;
import us.sparknetwork.api.command.CommandModule;

public class KickCommand extends CommandModule {
	
	BanManager plugin;

	public KickCommand(BanManager plugin) {
		super("kick", 1, 4096, "Usage /(command) <player> [reason] [-s]");
		this.plugin = plugin;
	}

	@Override
	public boolean run(CommandSender sender, String[] args) {
		plugin.punish(args, sender, Type.KICK);
		return true;
	}

}
