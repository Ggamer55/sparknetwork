package us.sparknetwork.BanManager.commands;

import org.bukkit.command.CommandSender;

import com.google.common.collect.ImmutableList;

import us.sparknetwork.BanManager.BanManager;
import us.sparknetwork.api.bans.BansAPI.Type;
import us.sparknetwork.api.command.CommandModule;

public class BlacklistCommand extends CommandModule {

	BanManager plugin;

	public BlacklistCommand(BanManager plugin) {
		super("blacklist", 1, 4096, "Usage /(command) <player> [reason] [-s]",
				ImmutableList.of("banip", "ipban", "ip-ban", "ban-ip"));
		this.plugin = plugin;
	}

	@Override
	public boolean run(CommandSender sender, String[] args) {
		plugin.punish(args, sender, Type.IPBAN);
		return true;
	}

}
