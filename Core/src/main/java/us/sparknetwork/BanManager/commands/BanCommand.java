package us.sparknetwork.BanManager.commands;

import org.bukkit.command.CommandSender;

import us.sparknetwork.BanManager.BanManager;
import us.sparknetwork.api.bans.BansAPI.Type;
import us.sparknetwork.api.command.CommandModule;

public class BanCommand extends CommandModule {
	
	BanManager plugin;

	public BanCommand(BanManager plugin) {
		super("ban", 1, 4096, "Usage /(command) <player> [time] [reason] [-s]", "tempban");
		this.plugin = plugin;
	}

	@Override
	public boolean run(CommandSender sender, String[] args) {
		plugin.punish(args, sender, Type.BAN);
		return true;
	}

}
