package us.sparknetwork.BanManager.commands;

import java.util.Arrays;

import org.bukkit.command.CommandSender;

import us.sparknetwork.BanManager.BanManager;
import us.sparknetwork.api.bans.BansAPI.Type;
import us.sparknetwork.api.command.CommandModule;

@SuppressWarnings("deprecation")
public class MuteCommand extends CommandModule {

	public MuteCommand() {
		super("mute", 0, -1, "Usage /<command> <playerName> [time] [reason]", Arrays.asList("tempmute"));
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean run(CommandSender sender, String[] args) {
		return BanManager.getInstance().punish(args, sender, Type.MUTE);
	}

}
