package us.sparknetwork.BanManager;

import lombok.Getter;
import net.md_5.bungee.api.ChatColor;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;
import us.sparknetwork.BanManager.commands.*;
import us.sparknetwork.api.event.PlayerPunishEvent;
import us.sparknetwork.BanManager.listeners.onChatListener;
import us.sparknetwork.BanManager.listeners.onJoinListener;
import us.sparknetwork.api.bans.BansAPI;
import us.sparknetwork.api.bans.Database;
import us.sparknetwork.api.bans.Punishment;
import us.sparknetwork.api.command.CommandModule;
import us.sparknetwork.core.CoreConstants;
import us.sparknetwork.core.CorePlugin;
import us.sparknetwork.core.StaffPriority;
import us.sparknetwork.core.commands.ReflectionCommandHandler;
import us.sparknetwork.core.server.ServerSettings;
import us.sparknetwork.util.PlayerUtils;
import us.sparknetwork.util.TimeParser;
import us.sparknetwork.util.TimeUtils;

import javax.annotation.Nullable;
import java.util.*;

public class BanManager implements BansAPI {

    @Getter
    private static BanManager instance;
    @Getter
    private Database database;
    @SuppressWarnings("unused")
    private BukkitTask autorefresh;
    @Getter
    private CorePlugin plugin;
    @Getter
    private boolean enabled;
    @Getter
    private Datastore datastore;

    public void onEnable(CorePlugin plugin) {
        this.plugin = plugin;
        instance = this;
        BansConfig.load();
        ReflectionCommandHandler cmdhandler = (ReflectionCommandHandler) plugin.getCmdhandler();
        List<CommandModule> cmdlist = new ArrayList<>();
        cmdlist.add(new BanCommand(this));
        cmdlist.add(new KickCommand(this));
        cmdlist.add(new UnbanCommand(this));
        cmdlist.add(new BlacklistCommand(this));
        cmdlist.add(new MuteCommand());
        for (CommandModule cmd : cmdlist) {
            cmd.setPermission("banmanager.command." + cmd.getLabel());
        }
        datastore = plugin.getMorphia().createDatastore(plugin.getMongoConnectionManager().getConnection(), "bans");
        cmdhandler.registerAll(cmdlist);
        plugin.getServer().getPluginManager().registerEvents(new onJoinListener(), plugin);
        plugin.getServer().getPluginManager().registerEvents(new onChatListener(), plugin);
        this.database = new BMDatabase(datastore);
        autorefresh = (new AutoRefreshScheduler()).runTaskTimer(plugin, BansConfig.AUTO_REFRESH_TICKS,
                BansConfig.AUTO_REFRESH_TICKS);
        this.enabled = true;
    }

    public void displayPunishmentMessage(Punishment punish) {
        if (punish.isSilent()) {
            switch (punish.getType()) {
                case BAN:
                    if (punish.isPermanent()) {
                        if (punish.isIppunishment()) {
                            Bukkit.broadcast(ChatColor.DARK_GRAY + "[" + ChatColor.RED + "Silent" + ChatColor.DARK_GRAY
                                    + "] " + ChatColor.GREEN + String.format("%1$s was ip-banned from the network by %2$s.",
                                    ChatColor.AQUA + Bukkit.getOfflinePlayer(punish.getPlayer()).getName() + ChatColor.GREEN,
                                    ChatColor.AQUA + punish.getPunisher() + ChatColor.GREEN), "banmanager.punishment.silent");
                            return;
                        } else {
                            Bukkit.broadcast(ChatColor.DARK_GRAY + "[" + ChatColor.RED + "Silent" + ChatColor.DARK_GRAY
                                    + "] " + ChatColor.GREEN + String.format("%1$s was banned from the network by %2$s.",
                                    ChatColor.AQUA + Bukkit.getOfflinePlayer(punish.getPlayer()).getName() + ChatColor.GREEN,
                                    ChatColor.AQUA + punish.getPunisher() + ChatColor.GREEN), "banmanager.punishment.silent");
                            return;
                        }
                    } else {
                        Bukkit.broadcast(ChatColor.DARK_GRAY + "[" + ChatColor.RED + "Silent" + ChatColor.DARK_GRAY
                                        + "] " + ChatColor.GREEN
                                        + String.format("%1$s was temp-banned from the network by %2$s for the time of %3$s.",
                                ChatColor.AQUA + Bukkit.getOfflinePlayer(punish.getPlayer()).getName() + ChatColor.GREEN,
                                ChatColor.AQUA + punish.getPunisher() + ChatColor.GREEN,
                                TimeUtils.getMSG(punish.getExpire() - punish.getPunishtime())),
                                "banmanager.punishment.silent");
                        return;
                    }
                case KICK:
                    Bukkit.broadcast(
                            ChatColor.DARK_GRAY + "[" + ChatColor.RED + "Silent" + ChatColor.DARK_GRAY + "] "
                                    + ChatColor.GREEN
                                    + String.format("%1$s was kicked from the network by %2$s.",
                                    ChatColor.AQUA + Bukkit.getOfflinePlayer(punish.getPlayer()).getName() + ChatColor.GREEN,
                                    ChatColor.AQUA + punish.getPunisher() + ChatColor.GREEN),
                            "banmanager.punishment.silent");
                    return;
                case MUTE:
                    if (punish.isPermanent()) {
                        if (punish.isIppunishment()) {
                            Bukkit.broadcast(ChatColor.DARK_GRAY + "[" + ChatColor.RED + "Silent" + ChatColor.DARK_GRAY
                                    + "] " + ChatColor.GREEN + String.format("%1$s was ip-muted from the network by %2$s.",
                                    ChatColor.AQUA + Bukkit.getOfflinePlayer(punish.getPlayer()).getName() + ChatColor.GREEN,
                                    ChatColor.AQUA + punish.getPunisher() + ChatColor.GREEN), "banmanager.punishment.silent");
                        return;
                        } else {
                            Bukkit.broadcast(ChatColor.DARK_GRAY + "[" + ChatColor.RED + "Silent" + ChatColor.DARK_GRAY
                                    + "] " + ChatColor.GREEN + String.format("%1$s was muted from the network by %2$s.",
                                    ChatColor.AQUA + Bukkit.getOfflinePlayer(punish.getPlayer()).getName() + ChatColor.GREEN,
                                    ChatColor.AQUA + punish.getPunisher() + ChatColor.GREEN), "banmanager.punishment.silent");
                            return;
                        }
                    } else {
                        Bukkit.broadcast(ChatColor.DARK_GRAY + "[" + ChatColor.RED + "Silent" + ChatColor.DARK_GRAY
                                        + "] " + ChatColor.GREEN
                                        + String.format("%1$s was temp-muted from the network by %2$s for the time of %3$s.",
                                ChatColor.AQUA + Bukkit.getOfflinePlayer(punish.getPlayer()).getName() + ChatColor.GREEN,
                                ChatColor.AQUA + punish.getPunisher() + ChatColor.GREEN,
                                TimeUtils.getMSG(punish.getExpire() - punish.getPunishtime())),
                                "banmanager.punishment.silent");
                        return;
                    }
            }
        } else {
            switch (punish.getType()) {
                case BAN:
                    if (punish.isPermanent()) {
                        if (punish.isIppunishment()) {
                            Bukkit.broadcastMessage(ChatColor.GREEN + String.format("%1$s was ip-banned from the network by %2$s.",
                                    ChatColor.AQUA + Bukkit.getOfflinePlayer(punish.getPlayer()).getName() + ChatColor.GREEN,
                                    ChatColor.AQUA + punish.getPunisher() + ChatColor.GREEN));
                            return;
                        } else {
                            Bukkit.broadcastMessage(ChatColor.GREEN + String.format("%1$s was banned from the network by %2$s.",
                                    ChatColor.AQUA + Bukkit.getOfflinePlayer(punish.getPlayer()).getName() + ChatColor.GREEN,
                                    ChatColor.AQUA + punish.getPunisher() + ChatColor.GREEN));
                            return;
                        }
                    } else {
                        Bukkit.broadcastMessage(ChatColor.GREEN
                                + String.format("%1$s was temp-banned from the network by %2$s for the time of %3$s.",
                                ChatColor.AQUA + Bukkit.getOfflinePlayer(punish.getPlayer()).getName() + ChatColor.GREEN,
                                ChatColor.AQUA + punish.getPunisher() + ChatColor.GREEN,
                                TimeUtils.getMSG(punish.getExpire() - punish.getPunishtime())));
                        return;
                    }
                case KICK:
                    Bukkit.broadcastMessage(ChatColor.GREEN + String.format("%1$s was kicked from the network by %2$s.",
                            ChatColor.AQUA + Bukkit.getOfflinePlayer(punish.getPlayer()).getName() + ChatColor.GREEN,
                            ChatColor.AQUA + punish.getPunisher() + ChatColor.GREEN));
                    return;
                case MUTE:
                    if (punish.isPermanent()) {
                        if (punish.isIppunishment()) {
                            Bukkit.broadcastMessage(ChatColor.GREEN + String.format("%1$s was ip-muted from the network by %2$s.",
                                    ChatColor.AQUA + Bukkit.getOfflinePlayer(punish.getPlayer()).getName() + ChatColor.GREEN,
                                    ChatColor.AQUA + punish.getPunisher() + ChatColor.GREEN));
                            return;
                        } else {
                            Bukkit.broadcastMessage(ChatColor.GREEN + String.format("%1$s was muted from the network by %2$s.",
                                    ChatColor.AQUA + Bukkit.getOfflinePlayer(punish.getPlayer()).getName() + ChatColor.GREEN,
                                    ChatColor.AQUA + punish.getPunisher() + ChatColor.GREEN));
                            return;
                        }
                    } else {
                        Bukkit.broadcastMessage(ChatColor.GREEN
                                + String.format("%1$s was temp-muted from the network by %2$s for the time of %3$s.",
                                ChatColor.AQUA + Bukkit.getOfflinePlayer(punish.getPlayer()).getName() + ChatColor.GREEN,
                                ChatColor.AQUA + punish.getPunisher() + ChatColor.GREEN,
                                TimeUtils.getMSG(punish.getExpire() - punish.getPunishtime())));
                            return;
                    }
            }
        }

    }

    public List<Punishment> getLastBans(Long lasttime) {
        if (!this.isEnabled()) {
            return Collections.emptyList();
        }
        List<Punishment> lastbans = new ArrayList<>();
        Database.get().getUnsafeConnection().find(Punishment.class).disableValidation().field("punishtime").greaterThan(lasttime).field("active").equal(true).asList().stream().sorted((p1, p2) -> {
            long a = p1.getExpire() - p2.getExpire();
            int i = a < 0 ? -1 : a > 0 ? 1 : 0;
            return i;
        }).forEachOrdered(lastbans::add);
        return lastbans;
    }

    public void overridePunishment(Punishment punish, UUID player, Type type) {
        Query<Punishment> query = Database.get().getUnsafeConnection().createQuery(Punishment.class).disableValidation().field("_id").equal(player).field("type").equal(type).field("active").equal(true);
        UpdateOperations<Punishment> update = Database.get().getUnsafeConnection().createUpdateOperations(Punishment.class)
                .set("punisher", punish.getPunisher())
                .set("reason", punish.getReason())
                .set("expire", punish.getExpire())
                .set("ip", punish.getIp())
                .set("active", punish.isActive())
                .set("ippunishment", punish.isIppunishment())
                .set("silent", punish.isSilent());
        Database.get().getConnection().update(query, update);
    }

    public Long getActualTime() {
        if (!this.isEnabled()) {
            return System.currentTimeMillis();
        }
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("America/Monterrey"));
        Date currentDate = calendar.getTime();
        return currentDate.getTime();
    }

    public void unpunishPlayer(UUID player, Type type) {
        if (!this.isEnabled()) {
            return;
        }
        plugin.getServer().getScheduler().runTaskAsynchronously(plugin, () -> {
            Query<Punishment> query = Database.get().getConnection().createQuery(Punishment.class).disableValidation().field("_id").equal(player).field("active").equal(true).field("type").equal(type.hasBase() ? type.getBase() : type);
            UpdateOperations<Punishment> update = Database.get().getConnection().createUpdateOperations(Punishment.class).set("active", false);
            Database.get().getConnection().update(query, update);
        });
    }

    // Usage /[ban|kick|mute] <player> [reason] [-s]
    public boolean punish(String[] args, CommandSender sender, Type type) {
        if (!this.isEnabled()) {
            return false;
        }
        if (args.length == 0) {
            return false;
        }
        if (type == Type.BAN) {
            if (args.length == 1) {
                if (!sender.hasPermission("banmanager.command.ban.perma")) {
                    sender.sendMessage(ChatColor.RED + "You don't have permission to create permanent bans");
                    return false;
                }
                OfflinePlayer target = PlayerUtils.getOfflinePlayer(args[0]);
                if (Database.get().isPlayerBanned(target.getUniqueId(), null)) {
                    if (!sender.hasPermission("banmanager.command.ban.override")) {
                        sender.sendMessage(
                                ChatColor.RED + "You don't have permissions to override bans, contact an admin.");
                        return false;
                    }
                    this.unpunishPlayer(target.getUniqueId(), type);
                    sender.sendMessage(ChatColor.YELLOW
                            + String.format("The past ban of %1$s was overrided for this action", target.getName()));
                }
                if (BansConfig.UNPUNISHABLE_PLAYERS.contains(target.getName())) {
                    sender.sendMessage(ChatColor.RED + "You can't punish that player.");
                    return false;
                }
                this.insertPermanentBan(target.getUniqueId(), sender.getName(), null, false);
                if (!target.hasPlayedBefore()) {
                    sender.sendMessage(ChatColor.DARK_RED + "WARNING: " + ChatColor.RED
                            + String.format("The player %1$s don't played before on this server.", args[0]));
                }
                if (target.isOnline()) {
                    if (!BansConfig.IS_LOBBY) {

                        target.getPlayer()
                                .kickPlayer(ChatColor.RED + String.format(
                                        "Your account was banned from %1$s \n Reason: %2$s \n Banned by: %3$s",
                                        BansConfig.SERVER_NAME, "None", sender.getName()));
                    }
                }
                return true;
            } else {
                OfflinePlayer target = PlayerUtils.getOfflinePlayer(args[0]);

                if (Database.get().isPlayerBanned(target.getUniqueId(), null)) {
                    if (!sender.hasPermission("banmanager.command.ban.override")) {
                        sender.sendMessage(
                                ChatColor.RED + "You don't have permissions to override bans, contact an admin.");
                        return false;
                    }
                    this.unpunishPlayer(target.getUniqueId(), type);
                    sender.sendMessage(ChatColor.YELLOW
                            + String.format("The past ban of %1$s was overrided for this action", target.getName()));
                }
                String larg = args[args.length - 1];
                Long time = 0l;
                try {
                    time = TimeParser.parse(args[1]);
                } catch (NumberFormatException e) {
                    time = 0l;
                }
                if (time > 0) {
                    if (!sender.hasPermission("banmanager.command.ban.temp")) {
                        sender.sendMessage(ChatColor.RED + "You don't have permission to create temporal bans");
                        return false;
                    }
                    if (BansConfig.UNPUNISHABLE_PLAYERS.contains(target.getName())) {
                        sender.sendMessage(ChatColor.RED + "You can't punish that player.");
                        return false;
                    }
                    StringBuilder reason = new StringBuilder();
                    if (args.length > 2) {
                        for (int i = 2; i < args.length; i++) {
                            if (args[i].equalsIgnoreCase("-s")) {
                                continue;
                            }
                            reason.append(args[i]);
                            reason.append(" ");
                        }
                    }
                    String banreason = StringUtils.isBlank(reason.toString()) ? "None"
                            : reason.toString();

                    if (!target.hasPlayedBefore()) {
                        sender.sendMessage(ChatColor.DARK_RED + "WARNING: " + ChatColor.RED
                                + String.format("The player %1$s don't played before on this server.", args[0]));
                    }
                    if (target.isOnline()) {
                        if (!BansConfig.IS_LOBBY) {

                            target.getPlayer().kickPlayer(ChatColor.RED + String.format(
                                    "Your account was temporally banned from %1$s \n Reason: %2$s \n Banned by: %3$s \n Expiration: %4$s",
                                    BansConfig.SERVER_NAME, banreason, sender.getName(),
                                    TimeUtils.getMSG(TimeParser.parse(args[1]))));
                        }
                    }
                    boolean silent = larg.equalsIgnoreCase("-s");

                    this.insertTemporalBan(target.getUniqueId(), TimeParser.parse(args[1]), sender.getName(),
                            banreason, silent);
                    return true;
                }
                if (!sender.hasPermission("banmanager.command.ban.perma")) {
                    sender.sendMessage(ChatColor.RED + "You don't have permission to create permanent bans");
                    return false;
                }
                if (BansConfig.UNPUNISHABLE_PLAYERS.contains(target.getName())) {
                    sender.sendMessage(ChatColor.RED + "You can't punish that player.");
                    return false;
                }
                StringBuilder reason = new StringBuilder();
                for (int i = 1; i < args.length; i++) {
                    if (args[i].equalsIgnoreCase("-s")) {
                        continue;
                    }
                    reason.append(args[i]);
                    reason.append(" ");
                }
                String banreason = StringUtils.isBlank(reason.toString()) ? "None"
                        : reason.toString();
                if (!target.hasPlayedBefore()) {
                    sender.sendMessage(ChatColor.DARK_RED + "WARNING:" + ChatColor.RED
                            + String.format("The player %1$s don't played before on this server.", args[0]));
                }
                if (target.isOnline()) {
                    if (!BansConfig.IS_LOBBY) {
                        target.getPlayer()
                                .kickPlayer(ChatColor.RED + String.format(
                                        "Your account was banned from %1$s \n Reason: %2$s \n Banned by: %3$s",
                                        BansConfig.SERVER_NAME, banreason, sender.getName()));
                    }

                }
                boolean silent = larg.equalsIgnoreCase("-s");

                this.insertPermanentBan(target.getUniqueId(), sender.getName(), banreason, silent);
                return true;
            }
        }
        if (type == Type.KICK) {
            if (args.length == 1) {
                Player target = PlayerUtils.getPlayer(args[0]);
                if (target == null) {
                    sender.sendMessage(String.format(CoreConstants.PLAYER_WITH_NAME_OR_UUID, args[0]));
                    return false;
                }
                if (BansConfig.UNPUNISHABLE_PLAYERS.contains(target.getName())) {
                    sender.sendMessage(ChatColor.RED + "You can't punish that player.");
                    return false;
                }
                this.insertKick(target.getUniqueId(), sender.getName(), null, false);
                target.kickPlayer(String.format(
                        ChatColor.RED + "Your account was kicked from %1$s \n Reason: %2$s \n Kicked by: %3$s",
                        BansConfig.SERVER_NAME, "None", sender.getName()));

                return true;

            } else {
                String larg = args[args.length - 1];
                StringBuilder reason = new StringBuilder();
                for (int i = 1; i < args.length; i++) {
                    if (args[i].equalsIgnoreCase("-s")) {
                        continue;
                    }
                    reason.append(args[i]);
                    reason.append(" ");
                }
                String kickreason = StringUtils.isBlank(reason.toString()) ? "None"
                        : reason.toString();
                Player target = PlayerUtils.getPlayer(args[0]);
                if (target == null) {
                    sender.sendMessage(String.format(CoreConstants.PLAYER_WITH_NAME_OR_UUID, args[0]));
                    return false;
                }
                if (BansConfig.UNPUNISHABLE_PLAYERS.contains(target.getName())) {
                    sender.sendMessage(ChatColor.RED + "You can't punish that player.");
                    return false;
                }
                if (!BansConfig.IS_LOBBY) {

                    target.kickPlayer(String.format(
                            ChatColor.RED + "Your account was kicked from %1$s \n Reason: %2$s \n Kicked by: %3$s",
                            BansConfig.SERVER_NAME, kickreason, sender.getName()));
                }
                boolean silent = larg.equalsIgnoreCase("-s");
                this.insertKick(target.getUniqueId(), sender.getName(), reason.toString(), silent);

                return true;
            }
        }
        if (type == Type.IPBAN) {
            // Usage /blacklist <player> [reason] [-s]
            if (args.length == 1) {
                OfflinePlayer target = PlayerUtils.getOfflinePlayer(args[0]);
                if (!target.hasPlayedBefore()) {
                    sender.sendMessage(ChatColor.DARK_RED + "WARNING:" + ChatColor.RED
                            + String.format("The player %1$s don't played before on this server.", args[0]));
                }
                if (BansConfig.UNPUNISHABLE_PLAYERS.contains(target.getName())) {
                    sender.sendMessage(ChatColor.RED + "You can't punish that player.");
                    return false;
                }
                if (Database.get().isPlayerBanned(target.getUniqueId(), null)) {
                    if (!sender.hasPermission("banmanager.command.ban.override")) {
                        sender.sendMessage(
                                ChatColor.RED + "You don't have permissions to override bans, contact an admin.");
                        return false;
                    }
                    this.unpunishPlayer(target.getUniqueId(), Type.BAN);
                    sender.sendMessage(ChatColor.YELLOW
                            + String.format("The past bans of %1$s was overrided for this action", target.getName()));
                }
                this.insertIpban(target.getUniqueId(),
                        target.isOnline() ? target.getPlayer().getAddress().getHostString() : "##offline##",
                        sender.getName(), null, false);
                if (target.isOnline()) {
                    if (!BansConfig.IS_LOBBY) {
                        target.getPlayer()
                                .kickPlayer(String.format(
                                        ChatColor.RED + "Your account was ip-banned from %1$s \n Reason: %2$s",
                                        BansConfig.SERVER_NAME, "None"));
                    }
                }
                return true;
            } else {
                OfflinePlayer target = PlayerUtils.getOfflinePlayer(args[0]);
                if (!target.hasPlayedBefore()) {
                    sender.sendMessage(ChatColor.DARK_RED + "WARNING:" + ChatColor.RED
                            + String.format("The player %1$s don't played before on this server.", args[0]));
                }
                if (BansConfig.UNPUNISHABLE_PLAYERS.contains(target.getName())) {
                    sender.sendMessage(ChatColor.RED + "You can't punish that player.");
                    return false;
                }
                if (Database.get().isPlayerBanned(target.getUniqueId(), null)) {
                    if (!sender.hasPermission("banmanager.command.ban.override")) {
                        sender.sendMessage(
                                ChatColor.RED + "You don't have permissions to override bans, contact an admin.");
                        return false;
                    }
                    this.unpunishPlayer(target.getUniqueId(), Type.BAN);
                    sender.sendMessage(ChatColor.YELLOW
                            + String.format("The past bans of %1$s was overrided for this action", target.getName()));
                }
                String larg = args[args.length - 1];
                StringBuilder reason = new StringBuilder();
                for (int i = 1; i < args.length; i++) {
                    if (args[i].equalsIgnoreCase("-s")) {
                        continue;
                    }
                    reason.append(args[i]);
                    reason.append(" ");
                }
                String blreason = StringUtils.isBlank(reason.toString()) ? "None" : reason.toString();
                if (target.isOnline()) {
                    if (!BansConfig.IS_LOBBY) {

                        target.getPlayer()
                                .kickPlayer(String.format(
                                        ChatColor.RED + "Your account was ip-banned from %1$s \n Reason: %2$s",
                                        BansConfig.SERVER_NAME, blreason));
                    }
                }
                boolean silent = larg.equalsIgnoreCase("-s");
                this.insertIpban(target.getUniqueId(),
                        target.isOnline() ? target.getPlayer().getAddress().getHostString() : "##offline##",
                        sender.getName(), blreason, silent);
            }
        }
        if (type == Type.MUTE) {
            if (args.length == 1) {
                if (!sender.hasPermission("banmanager.command.mute.perma")) {
                    sender.sendMessage(ChatColor.RED + "You don't have permission to create permanent bans");
                    return false;
                }
                OfflinePlayer target = PlayerUtils.getOfflinePlayer(args[0]);
                if (BansConfig.UNPUNISHABLE_PLAYERS.contains(target.getName())) {
                    sender.sendMessage(ChatColor.RED + "You can't punish that player.");
                    return true;
                }
                if (sender instanceof Player) {
                    if (target.isOnline()) {
                        StaffPriority tsp = StaffPriority.getByPlayer(target.getPlayer());
                        StaffPriority ssp = StaffPriority.getByPlayer((Player) sender);
                        if (tsp.isMoreThan(ssp)) {
                            sender.sendMessage(ChatColor.RED + "You can't mute staff with mayor priority than you.");
                            return true;
                        }
                    }
                }
                if (Database.get().isPlayerMuted(target.getUniqueId(), null)) {
                    if (!sender.hasPermission("banmanager.command.mute.override")) {
                        sender.sendMessage(
                                ChatColor.RED + "You don't have permissions to override mutes, contact an admin.");
                        return true;
                    }
                    this.unpunishPlayer(target.getUniqueId(), Type.MUTE);
                    sender.sendMessage(ChatColor.YELLOW
                            + String.format("The past mute of %1$s was overrided for this action", target.getName()));
                }

                this.insertPermanentMute(target.getUniqueId(), sender.getName(), null, false);
                if (!target.hasPlayedBefore()) {
                    sender.sendMessage(ChatColor.DARK_RED + "WARNING: " + ChatColor.RED
                            + String.format("The player %1$s don't played before on this server.", args[0]));
                }
                return true;
            } else {
                OfflinePlayer target = PlayerUtils.getOfflinePlayer(args[0]);

                if (BansConfig.UNPUNISHABLE_PLAYERS.contains(target.getName())) {
                    sender.sendMessage(ChatColor.RED + "You can't punish that player.");
                    return false;
                }
                if (sender instanceof Player) {
                    if (target.isOnline()) {
                        StaffPriority tsp = StaffPriority.getByPlayer(target.getPlayer());
                        StaffPriority ssp = StaffPriority.getByPlayer((Player) sender);
                        if (tsp.isMoreThan(ssp)) {
                            sender.sendMessage(ChatColor.RED + "You can't mute staff with mayor priority than you.");
                            return true;
                        }
                    }
                }
                if (Database.get().isPlayerMuted(target.getUniqueId(), null)) {
                    if (!sender.hasPermission("banmanager.command.mute.override")) {
                        sender.sendMessage(
                                ChatColor.RED + "You don't have permissions to override mutes, contact an admin.");
                        return true;
                    }
                    this.unpunishPlayer(target.getUniqueId(), Type.MUTE);
                    sender.sendMessage(ChatColor.YELLOW
                            + String.format("The past mute of %1$s was overrided for this action", target.getName()));
                }
                Long time = 0L;
                try {
                    time = TimeParser.parse(args[1]);
                } catch (NumberFormatException e) {
                    time = 0l;
                }
                if (time > 0) {
                    if (!sender.hasPermission("banmanager.command.mute.temp")) {
                        sender.sendMessage(ChatColor.RED + "You don't have permission to create temporal mutes.");
                        return false;
                    }

                    StringBuilder reason = new StringBuilder();
                    if (args.length > 2) {
                        for (int i = 2; i < args.length; i++) {
                            reason.append(args[i]);
                            reason.append(" ");
                        }
                    }
                    String banreason = StringUtils.isBlank(reason.toString()) ? "None" : reason.toString();
                    this.insertTemporalMute(target.getUniqueId(), TimeParser.parse(args[1]), sender.getName(),
                            banreason, false);
                    if (!target.hasPlayedBefore()) {
                        sender.sendMessage(ChatColor.DARK_RED + "WARNING: " + ChatColor.RED
                                + String.format("The player %1$s don't played before on this server.", args[0]));
                    }
                    return true;
                }
                if (!sender.hasPermission("banmanager.command.mute.perma")) {
                    sender.sendMessage(ChatColor.RED + "You don't have permission to create permanent mutes.");
                    return false;
                }
                if (BansConfig.UNPUNISHABLE_PLAYERS.contains(target.getName())) {
                    sender.sendMessage(ChatColor.RED + "You can't punish that player.");
                    return false;
                }
                StringBuilder reason = new StringBuilder();
                for (int i = 1; i < args.length; i++) {
                    reason.append(args[i]);
                    reason.append(" ");
                }
                String banreason = StringUtils.isBlank(reason.toString()) ? "None" : reason.toString();
                this.insertPermanentMute(target.getUniqueId(), sender.getName(), banreason, false);
                if (!target.hasPlayedBefore()) {
                    sender.sendMessage(ChatColor.DARK_RED + "WARNING:" + ChatColor.RED
                            + String.format("The player %1$s don't played before on this server.", args[0]));
                }
                return true;
            }
        }
        return false;
    }

    private void insertPunishment(Punishment punish) {
        plugin.getServer().getScheduler().runTaskAsynchronously(plugin, () -> Database.get().getConnection().save(punish));
        Bukkit.getPluginManager().callEvent(new PlayerPunishEvent(punish.getPlayer(), punish));
        this.displayPunishmentMessage(punish);
    }

    public void insertPermanentBan(final UUID punished, String punisher, String reason, boolean silent) {
        if (!this.isEnabled()) {
            return;
        }
        if (punished == null)
            return;
        punisher = StringUtils.isBlank(punisher) ? "Console" : punisher;
        reason = StringUtils.isBlank(punisher) ? "None" : reason;
        String ip = Bukkit.getPlayer(punished) != null
                ? Bukkit.getPlayer(punished).getAddress().getAddress().getHostAddress()
                : "##offline##";
        Punishment punish = new Punishment(punished, punisher, reason, 0L, this.getActualTime(), ip, Type.BAN, true, false, silent, ServerSettings.NAME);
        this.insertPunishment(punish);
    }

    public void insertPermanentMute(final UUID punished, String punisher, String reason, boolean silent) {
        if (!this.isEnabled()) {
            return;
        }
        if (punished == null)
            return;
        punisher = StringUtils.isBlank(punisher) ? "Console" : punisher;
        reason = StringUtils.isBlank(punisher) ? "None" : reason;
        String ip = Bukkit.getPlayer(punished) != null
                ? Bukkit.getPlayer(punished).getAddress().getAddress().getHostAddress()
                : "##offline##";
        Punishment punish = new Punishment(punished, punisher, reason, 0l, this.getActualTime(), ip, Type.MUTE, true, false, silent, ServerSettings.NAME);
        this.insertPunishment(punish);

    }

    public void insertTemporalMute(final UUID punished, long expire, String punisher,
                                   String reason, boolean silent) {
        if (!this.isEnabled()) {
            return;
        }
        if (punished == null)
            return;
        punisher = StringUtils.isBlank(punisher) ? "Console" : punisher;
        reason = StringUtils.isBlank(punisher) ? "None" : reason;
        long expiration = this.getActualTime() + expire;

        String ip = Bukkit.getPlayer(punished) != null
                ? Bukkit.getPlayer(punished).getAddress().getAddress().getHostAddress()
                : "##offline##";
        Punishment punish = new Punishment(punished, punisher, reason, expiration, this.getActualTime(), ip, Type.MUTE, true, false, silent, ServerSettings.NAME);
        this.insertPunishment(punish);
    }

    public void insertIpMute(final UUID punished, long expire, String punisher, String reason, boolean silent) {
        if (!this.isEnabled()) {
            return;
        }
        if (punished == null)
            return;
        punisher = StringUtils.isBlank(punisher) ? "Console" : punisher;
        reason = StringUtils.isBlank(punisher) ? "None" : reason;
        long expiration = this.getActualTime() + expire;

        String ip = Bukkit.getPlayer(punished) != null
                ? Bukkit.getPlayer(punished).getAddress().getAddress().getHostAddress()
                : "##offline##";
        Punishment punish = new Punishment(punished, punisher, reason, expiration, this.getActualTime(), ip, Type.MUTE, true, true, silent, ServerSettings.NAME);
        this.insertPunishment(punish);
    }

    public void insertKick(final UUID punished, String punisher, String reason, boolean silent) {
        if (!this.isEnabled()) {
            return;
        }
        if (punished == null)
            return;
        punisher = punisher == null || punisher.equalsIgnoreCase("") ? "Console" : punisher;
        reason = reason == null || reason.equalsIgnoreCase("") ? "None" : reason;
        Punishment punish = new Punishment(punished, punisher, reason, 0l, this.getActualTime(), "##offline##", Type.KICK, true, false, silent, ServerSettings.NAME);
        this.insertPunishment(punish);
    }

    public void insertIpban(final UUID punished, String ip, String punisher, String reason, boolean silent) {
        if (!this.isEnabled()) {
            return;
        }
        if (punished == null)
            return;
        punisher = StringUtils.isBlank(punisher) ? "Console" : punisher;
        reason = StringUtils.isBlank(punisher) ? "None" : reason;

        ip = Bukkit.getPlayer(punished) != null
                ? Bukkit.getPlayer(punished).getAddress().getAddress().getHostAddress()
                : "##offline##";
        Punishment punish = new Punishment(punished, punisher, reason, 0l, this.getActualTime(), ip, Type.BAN, true, true, silent, ServerSettings.NAME);
        this.insertPunishment(punish);
    }

    public void insertTemporalBan(final UUID punished, long expire, @Nullable String punisher,
                                  String reason, boolean silent) {
        if (!this.isEnabled()) {
            return;
        }
        if (punished == null)
            return;
        punisher = StringUtils.isBlank(punisher) ? "Console" : punisher;
        reason = StringUtils.isBlank(punisher) ? "None" : reason;
        long expiration = this.getActualTime() + expire;

        String ip = Bukkit.getPlayer(punished) != null
                ? Bukkit.getPlayer(punished).getAddress().getAddress().getHostAddress()
                : "##offline##";
        Punishment punish = new Punishment(punished, punisher, reason, expiration, this.getActualTime(), ip, Type.BAN, true, false, silent, ServerSettings.NAME);
        this.insertPunishment(punish);
    }

}