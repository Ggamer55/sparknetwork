package us.sparknetwork.core.commands.chat;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import us.sparknetwork.api.command.CommandModule;
import us.sparknetwork.core.CoreConstants;
import us.sparknetwork.core.CorePlugin;
import us.sparknetwork.core.ServerHandler;

public class CommandAnnouncement extends CommandModule {

    public CommandAnnouncement() {
        super("announcement", 1, -1, "Usage /(command) <delay [seconds] | toggle | add <announce>> ", "ann");
        // TODO Auto-generated constructor stub
    }

    @Override
    public boolean run(CommandSender sender, String[] args) {
        ServerHandler sh = CorePlugin.getInstance().getServerHandler();
        if (args[0].equalsIgnoreCase("toggle")) {
            sh.setAnnouncerEnabled(!sh.isAnnouncerEnabled());
            String mode = sh.isAnnouncerEnabled() ? "enabled" : "disabled";
            CorePlugin.getInstance().registerSchedulers();
            broadcastCommandMessage(sender, String.format(CoreConstants.YELLOW + "The announcer is now %1$s.", mode));
            return true;
        }
        if (args[0].equalsIgnoreCase("delay")) {
            if (args.length == 1) {
                return false;
            }
            int number;
            try {
                number = Integer.parseInt(args[1]);

            } catch (NumberFormatException e) {
                sender.sendMessage(CoreConstants.GOLD + "You must provide a valid quantity of seconds.");
                return true;
            }
            if (number == 0) {
                sender.sendMessage(CoreConstants.YELLOW + "You must provide a quantity of seconds greater than 0.");
                return true;
            }
            sh.setAnnouncerDelay(number);
            CorePlugin.getInstance().registerSchedulers();
            broadcastCommandMessage(sender, String.format(CoreConstants.YELLOW + "Set the announcer delay to %1$s seconds.", number));
            return true;
        }

        if (args[0].equalsIgnoreCase("add")) {
            if (args.length < 1) {
                return false;
            }
            StringBuilder message = new StringBuilder();
            for (int i = 2; i < args.length; i++) {
                message.append(args[i]);
                if (i < args.length) {
                    message.append(" ");
                }
            }
            String announcement = ChatColor.translateAlternateColorCodes('&', message.toString());
            sh.getAnnouncements().add(announcement);
            broadcastCommandMessage(sender, CoreConstants.YELLOW+"Sucessfully added a new announcement into the announcer.");
            return true;
        }
        return false;

    }

}
