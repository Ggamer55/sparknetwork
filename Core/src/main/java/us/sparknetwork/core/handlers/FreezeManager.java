package us.sparknetwork.core.handlers;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import us.sparknetwork.core.CoreConstants;
import us.sparknetwork.core.CoreConstants;

public class FreezeManager implements Listener {

	public static FreezeManager instance;
	public JavaPlugin plugin;
	public List<UUID> freezedplayers;
	public BukkitRunnable freezerun;

	public FreezeManager(JavaPlugin plugin) {
		instance = this;
		this.plugin = plugin;
		this.freezedplayers = new ArrayList<>();
		Bukkit.getPluginManager().registerEvents(this, plugin);
		freezerun = new BukkitRunnable() {
			@Override
			public void run() {
				freezedplayers.forEach(p -> {
					Player pl = Bukkit.getPlayer(p);
					if (pl != null)
						pl.sendMessage(ChatColor.translateAlternateColorCodes('&',
								"&7&m--------  &6&m--------  &7&m--------  &6&m-------- &7&m-------- &6&m------ \n &c     You where frozen \n      enter to "
										+ CoreConstants.TEAMSPEAK
										+ "\n &7&m--------  &6&m--------  &7&m--------  &6&m-------- &7&m-------- &6&m------- "));
				});

			}

		};
	}

	public static FreezeManager getInstance() {
		return instance;
	}

	public boolean FreezePlayer(Player player) {
		if (!this.freezedplayers.isEmpty()) {
			if (this.freezedplayers.contains(player.getUniqueId())) {
				return false;
			}
		}
		if (player.hasPermission("core.command.freeze.bypass")) {
			return false;
		}
		this.freezedplayers.add(player.getUniqueId());
		player.setCanPickupItems(false);
		return true;
	}

	public void UnfreezePlayer(Player player) {
		if (this.freezedplayers.isEmpty()) {
			return;
		}
		if (!this.freezedplayers.contains(player.getUniqueId())) {
			return;
		}
		player.setCanPickupItems(true);
		freezedplayers.remove(player.getUniqueId());
	}

	@EventHandler
	public void onLeave(PlayerQuitEvent e) {
		if (this.freezedplayers.isEmpty()) {
			return;
		}
		if (this.freezedplayers.contains(e.getPlayer().getUniqueId())) {
			Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
					"ban %player% &cRefusing to SS".replaceAll("%player%", e.getPlayer().getName()));
		}
	}

	@EventHandler
	public void onMove(PlayerMoveEvent e) {
		Player p = e.getPlayer();
		if (this.freezedplayers.contains(p.getUniqueId())
				&& (e.getFrom().getZ() != e.getTo().getZ() || e.getFrom().getX() != e.getTo().getX())) {
			e.setTo(e.getFrom());
			p.sendMessage(
					"&7&m--------  &6&m--------  &7&m--------  &6&m-------- &7&m-------- &6&m------ \n &c     You where frozen \n      enter to "
							+ CoreConstants.TEAMSPEAK
							+ "\n &7&m--------  &6&m--------  &7&m--------  &6&m-------- &7&m-------- &6&m------- "
									.replace("&", "§"));

		}
	}

	@EventHandler
	public void onInventory(PlayerDropItemEvent e) {
		if (this.freezedplayers.isEmpty()) {
			return;
		}
		if (this.freezedplayers.contains(e.getPlayer().getUniqueId())) {
			e.setCancelled(true);
		}
	}

	@EventHandler
	public void OnInteract(PlayerInteractEvent e) {
		if (this.freezedplayers.isEmpty()) {
			return;
		}
		if (this.freezedplayers.contains(e.getPlayer().getUniqueId())) {
			e.setCancelled(true);
		}
	}

	@EventHandler
	public void onBreak(BlockBreakEvent e) {
		if (this.freezedplayers.isEmpty()) {
			return;
		}
		if (this.freezedplayers.contains(e.getPlayer().getUniqueId())) {
			e.setCancelled(true);
		}
	}

	@EventHandler
	public void onInventory2(InventoryClickEvent e) {
		if (this.freezedplayers.isEmpty()) {
			return;
		}
		if (e.getWhoClicked() instanceof Player) {
			Player player = (Player) e.getWhoClicked();
			if (this.freezedplayers.contains(player.getUniqueId())) {
				e.setCancelled(true);
			}
		}
	}

	@EventHandler
	public void FreezedDamage(EntityDamageByEntityEvent e) {
		if (freezedplayers.isEmpty()) {
			return;
		}
		Entity en = e.getDamager();
		if (en instanceof Player) {
			Player pl = (Player) en;
			if (this.freezedplayers.contains(pl.getUniqueId())) {
				pl.sendMessage(CoreConstants.YELLOW + "You can't damage players when you are freezed");
				e.setCancelled(true);
			}
		}
	}

	@EventHandler
	public void FreezedDamage2(EntityDamageByEntityEvent e) {
		if (freezedplayers.isEmpty()) {
			return;
		}
		Entity en1 = e.getDamager();
		Entity en = e.getEntity();
		if (en instanceof Player && en1 instanceof Player) {
			Player pl1 = (Player) en1;
			Player pl = (Player) en;
			if (this.freezedplayers.contains(pl.getUniqueId())) {
				pl1.sendMessage(CoreConstants.YELLOW + "You can't damage freezed players");
				e.setCancelled(true);
			}
		}
	}

}
