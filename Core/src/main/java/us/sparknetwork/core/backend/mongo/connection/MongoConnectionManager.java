package us.sparknetwork.core.backend.mongo.connection;

import java.util.Collections;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;

import us.sparknetwork.core.backend.MongoCredentials;

public class MongoConnectionManager {
	
	public MongoConnectionManager(MongoCredentials credentials) {
		this.credentials = credentials;
		this.init();
	}

	private MongoClient dataSource;
	private MongoCredentials credentials;

	public MongoClient getConnection() {
		return this.dataSource;
	}
	
	public void closeMongo() {
		this.dataSource.close();
	}
	
	
	private void init() {
		MongoCredential credential = MongoCredential.createCredential(this.credentials.getUsername(), credentials.getDatabase(), credentials.getPassword().toCharArray());
		ServerAddress address = new ServerAddress(credentials.getHostname(),credentials.getPort());
		if(credentials.isAuthenticable()) {
			dataSource = new MongoClient(address, Collections.singletonList(credential));
		} else {
			dataSource = new MongoClient(address);
		}
	}

}
