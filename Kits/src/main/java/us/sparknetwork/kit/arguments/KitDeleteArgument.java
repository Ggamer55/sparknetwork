package us.sparknetwork.kit.arguments;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import us.sparknetwork.api.kit.IKitManager;
import us.sparknetwork.core.CoreConstants;
import us.sparknetwork.kit.KitManager;
import us.sparknetwork.kit.KitsPlugin;
import us.sparknetwork.util.CommandArgument;

public class KitDeleteArgument extends CommandArgument {

    public KitDeleteArgument() {
        super("delete", "Deletes a specified kit");
        this.permission = "core.command.kit.argument." + this.getName();
    }

    @Override
    public String getUsage(String label) {
        return "/" + label + " delete <kitName>";
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (args.length == 2) {
            IKitManager kitm = KitsPlugin.getPlugin().getKitManager();
            if (!kitm.getKit(args[1]).isPresent()) {
                sender.sendMessage(ChatColor.RED + "Kit with name " + args[1] + " not found.");
                return true;
            }
            kitm.removeKit(kitm.getKit(args[1]).get());
            Command.broadcastCommandMessage(sender,
                    CoreConstants.YELLOW + String.format("Sucessfully deleted the kit %1$s.", args[1]));
            return true;
        }
        sender.sendMessage(ChatColor.RED + "Usage: " + this.getUsage(label));
        return false;
    }

}
