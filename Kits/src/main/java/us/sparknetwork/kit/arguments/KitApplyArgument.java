package us.sparknetwork.kit.arguments;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import us.sparknetwork.api.kit.IKit;
import us.sparknetwork.kit.KitsPlugin;
import us.sparknetwork.util.CommandArgument;

public class KitApplyArgument extends CommandArgument {

    public KitApplyArgument() {
        super("apply", "Applies to a specified player a kit");
        this.permission = "core.command.kit.argument." + this.getName();
    }

    @Override
    public String getUsage(String label) {
        return String.valueOf(String.valueOf('/')) + label + ' ' + this.getName() + " <kitName> <playerName>";
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length < 3) {
            sender.sendMessage(ChatColor.RED + "Usage: " + this.getUsage(label));
            return true;
        }
        Optional<IKit> kit = KitsPlugin.getPlugin().getKitManager().getKit(args[1]);
        if (!kit.isPresent()) {
            sender.sendMessage(ChatColor.RED + "Kit with name " + args[1] + " not found.");
            return true;
        }
        Player target = Bukkit.getPlayer(args[2]);
        if (target == null || sender instanceof Player && !((Player) sender).canSee(target)) {
            sender.sendMessage(ChatColor.RED + "Player '" + ChatColor.GRAY + args[2] + ChatColor.RED + "' not found.");
            return true;
        }
        if (kit.get().applyTo(target, true, true)) {
            sender.sendMessage(ChatColor.GRAY + "Applied kit '" + kit.get().getName() + "' to '" + target.getName() + "'.");
            return true;
        }
        sender.sendMessage(ChatColor.RED + "Failed to apply kit " + kit.get().getName() + " to " + target.getName() + '.');
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 2) {
            List<IKit> kits =  KitsPlugin.getPlugin().getKitManager().getKits();
            ArrayList<String> results = new ArrayList<String>(kits.size());
            for (IKit kit : kits) {
                results.add(kit.getName());
            }
            return results;
        }
        if (args.length == 3) {
            return null;
        }
        return Collections.emptyList();
    }

}
