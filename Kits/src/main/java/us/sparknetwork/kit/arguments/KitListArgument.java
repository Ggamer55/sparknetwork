package us.sparknetwork.kit.arguments;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import us.sparknetwork.api.kit.IKit;
import us.sparknetwork.core.CoreConstants;
import us.sparknetwork.kit.KitsPlugin;
import us.sparknetwork.kit.user.KitsUser;
import us.sparknetwork.util.CommandArgument;

import java.util.ArrayList;
import java.util.List;

public class KitListArgument extends CommandArgument {

    public KitListArgument() {
        super("list", "Lists all the core available for you");
        this.setPermission("core.command.kit.argument." + this.getName());
    }

    @Override
    public String getUsage(String label) {
        return "/" + label + " list";
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        sender.sendMessage(CoreConstants.GOLD + "Kits list:");
        if (sender instanceof Player) {
            KitsUser user = KitsPlugin.getPlugin().getUserManager().getUser(((Player) sender).getUniqueId());
            if (!sender.hasPermission(this.getPermission() + ".showall")) {
                List<IKit> availablekits = new ArrayList<>();

                KitsPlugin.getPlugin().getKitManager().getKits().forEach(kit -> {
                    if (sender.hasPermission(kit.getKitPermission())) {
                        if (user.getKitUses(kit) < kit.getMaximumUses()
                                && user.getRemainingKitCooldown(kit) < kit.getDelayMillis()) {
                            availablekits.add(kit);
                        }
                    }
                });
                if (availablekits.isEmpty()) {
                    sender.sendMessage(ChatColor.RED + "No available kits for you.");
                    return true;
                }
                availablekits.forEach(
                        kit -> sender.sendMessage(CoreConstants.GRAY + "Kit " + CoreConstants.YELLOW + kit.getName()));
                return true;
            }

            if (KitsPlugin.getPlugin().getKitManager().getKits().isEmpty()) {
                sender.sendMessage(ChatColor.RED + "No kits available.");
                return true;
            }
            KitsPlugin.getPlugin().getKitManager().getKits().forEach(
                    kit -> sender.sendMessage(CoreConstants.GRAY + "Kit " + CoreConstants.YELLOW + kit.getName()));
            return true;
        } else {
            KitsPlugin.getPlugin().getKitManager().getKits().forEach(kit -> sender.sendMessage(CoreConstants.GRAY + "Kit " + CoreConstants.YELLOW + kit.getName()));
        }

        return true;
    }

}
