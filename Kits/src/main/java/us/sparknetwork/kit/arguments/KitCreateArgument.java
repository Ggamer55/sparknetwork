package us.sparknetwork.kit.arguments;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import us.sparknetwork.core.CoreConstants;
import us.sparknetwork.kit.Kit;
import us.sparknetwork.kit.KitsPlugin;
import us.sparknetwork.util.CommandArgument;

public class KitCreateArgument extends CommandArgument {

    public KitCreateArgument() {
        super("create", "Creates a kit");
        this.permission = "core.command.kit.argument." + this.getName();
    }

    @Override
    public String getUsage(String label) {
        return String.format("/%1$s create <kitName>", label);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (args.length == 2) {
            KitsPlugin.getPlugin().getKitManager().createKit(new Kit.builder(args[1]).build());
            Command.broadcastCommandMessage(sender,
                    CoreConstants.YELLOW + String.format("Sucessfully created the kit %1$s.", args[1]));
            return true;
        }
        sender.sendMessage(ChatColor.RED + "Usage: " + this.getUsage(label));
        return false;
    }

}
