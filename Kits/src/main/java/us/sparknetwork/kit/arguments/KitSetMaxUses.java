package us.sparknetwork.kit.arguments;

import java.util.Optional;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import us.sparknetwork.api.kit.IKit;
import us.sparknetwork.core.CoreConstants;
import us.sparknetwork.kit.KitsPlugin;
import us.sparknetwork.util.CommandArgument;
import us.sparknetwork.util.JavaUtils;

public class KitSetMaxUses extends CommandArgument {

    public KitSetMaxUses() {
        super("setmaxuses", "Set max uses to a specified kit");
        this.permission = "core.command.kit.argument." + this.getName();
    }

    @Override
    public String getUsage(String label) {
        return "/" + label + " setmaxuses <kitName> <uses>";
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (args.length == 3) {
            Optional<IKit> optkit = KitsPlugin.getPlugin().getKitManager().getKit(args[1]);
            if (!optkit.isPresent()) {
                sender.sendMessage(ChatColor.RED + "Kit with name " + args[1] + " not found");
                return true;
            }
            IKit kit = optkit.get();
            int uses = JavaUtils.tryParseInt(args[2]);
            kit.setMaximumUses(uses);
            Command.broadcastCommandMessage(sender, CoreConstants.YELLOW
                    + String.format("Sucessfully set to the kit %1$s maximum usages of %2$s.", kit.getName(), uses));
            return true;
        }
        sender.sendMessage(ChatColor.RED + "Usage: " + this.getUsage(label));
        return false;
    }

}
