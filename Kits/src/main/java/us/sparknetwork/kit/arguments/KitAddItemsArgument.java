package us.sparknetwork.kit.arguments;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import us.sparknetwork.api.kit.IKit;
import us.sparknetwork.core.CoreConstants;
import us.sparknetwork.kit.KitsPlugin;
import us.sparknetwork.util.CommandArgument;

public class KitAddItemsArgument extends CommandArgument {

    public KitAddItemsArgument() {
        super("additems", "Adds items to a specified kit");
        this.permission = "core.command.kit.argument." + this.getName();
    }

    @Override
    public String getUsage(String label) {
        return String.format("/%1$s additems <kitName>", label);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Only players can execute this command.");
            return true;
        }
        if (args.length == 2) {
            Player player = (Player) sender;
            PlayerInventory inv = player.getInventory();
            Optional<IKit> optkit = KitsPlugin.getPlugin().getKitManager().getKit(args[1]);
            if (!optkit.isPresent()) {
                sender.sendMessage(ChatColor.RED + "Kit with name " + args[1] + " not found");
                return true;
            }
            IKit kit = optkit.get();
            List<ItemStack> items = new ArrayList<>();
            for (ItemStack item : inv.getContents()) {
                items.add(item);
            }

            List<ItemStack> armour = new ArrayList<>();
            for (ItemStack item : inv.getArmorContents()) {
                armour.add(item);
            }
            kit.setItems(items);
            kit.setArmour(armour);
            sender.sendMessage(CoreConstants.YELLOW
                    + String.format("Sucessfully added the items in your inventory to the kit %1$s.", kit.getName()));
            return true;
        }
        sender.sendMessage(ChatColor.RED + "Usage: " + this.getUsage(label));
        return false;
    }

}
