package us.sparknetwork.kit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.potion.PotionEffect;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import us.sparknetwork.api.kit.IKit;
import us.sparknetwork.api.kit.event.KitApplyEvent;
import us.sparknetwork.api.kit.event.KitEvent;
import us.sparknetwork.api.kit.event.KitPlayerEvent;
import us.sparknetwork.api.kit.event.KitRenameEvent;
import us.sparknetwork.core.CoreConstants;
import us.sparknetwork.util.GenericUtils;

@AllArgsConstructor(access = AccessLevel.MODULE)
public class Kit implements ConfigurationSerializable,IKit {

    private static final ItemStack DEFAULT_IMAGE = new ItemStack(Material.EMERALD, 1);
    @Getter
    protected UUID uniqueID;
    @Getter
    @Setter(value = AccessLevel.PRIVATE)
    protected String name;
    @Getter
    @Setter
    protected List<ItemStack> items;
    @Getter
    @Setter
    protected List<ItemStack> armour;
    @Getter
    @Setter
    protected Collection<PotionEffect> effects;
    @Getter
    @Setter
    protected ItemStack image;
    @Getter
    @Setter
    protected boolean enabled;
    @Getter
    @Setter
    protected long delayMillis;
    @Getter
    @Setter
    protected long minPlaytimeMillis;
    @Getter
    @Setter
    protected int maximumUses;

    public Kit(String name, PlayerInventory inventory, Collection<PotionEffect> effects) {
        this(name, inventory, effects, 0);
    }

    public Kit(String name, Inventory inventory, Collection<PotionEffect> effects, long milliseconds) {
        this.enabled = true;
        this.uniqueID = UUID.randomUUID();
        this.name = name;
        this.setItems(Arrays.asList(inventory.getContents()));
        if (inventory instanceof PlayerInventory) {
            PlayerInventory playerInventory = (PlayerInventory) inventory;
            this.setArmour(Arrays.asList(playerInventory.getArmorContents()));
            this.setImage(playerInventory.getItemInHand());
        }
        this.effects = effects;
        this.delayMillis = milliseconds;
        this.maximumUses = Integer.MAX_VALUE;
    }

    public Kit(Map<String, Object> map) {
        this.uniqueID = UUID.fromString((String) map.get("kitUID"));
        setName((String) map.get("name"));
        setEnabled((Boolean) map.get("enabled"));
        setEffects(GenericUtils.createList(map.get("effects"), PotionEffect.class));
        List<ItemStack> items = GenericUtils.createList(map.get("items"), ItemStack.class);
        setItems(items);
        List<ItemStack> armour = GenericUtils.createList(map.get("armour"), ItemStack.class);
        setArmour(armour);
        setImage((ItemStack) map.get("image"));
        setDelayMillis(Long.parseLong((String) map.get("delay")));
        setMaximumUses((Integer) map.get("maxUses"));
    }

    @Override
    public Map<String, Object> serialize() {
        LinkedHashMap<String, Object> map = new LinkedHashMap<String, Object>();
        map.put("kitUID", this.uniqueID.toString());
        map.put("name", this.name);
        map.put("enabled", this.enabled);
        map.put("effects", this.effects);
        map.put("items", this.items);
        map.put("armour", this.armour);
        map.put("image", this.image);
        map.put("delay", Long.toString(this.delayMillis));
        map.put("maxUses", this.maximumUses);
        return map;
    }

    @Override
    public void renameKit(String name) {
        KitEvent event = new KitRenameEvent(this, this.name, name);
        Bukkit.getPluginManager().callEvent(event);
        if (!event.isCancelled())
            this.name = name;
    }

    @Override
    public boolean applyTo(Player player, boolean force, boolean inform) {
        KitPlayerEvent event = new KitApplyEvent(player, this, force);
        Bukkit.getPluginManager().callEvent(event);
        if (event.isCancelled()) {
            return false;
        } else {
            player.addPotionEffects(effects);
            ItemStack cursor = player.getItemOnCursor();
            if (cursor != null && cursor.getType() != Material.AIR) {
                player.setItemOnCursor(new ItemStack(Material.AIR));
                player.getWorld().dropItemNaturally(player.getLocation(), cursor);
            }
            PlayerInventory inv = player.getInventory();
            int n = this.items.size();
            int n2 = 0;
            while (n2 < n) {
                ItemStack item = this.items.get(n2);
                if (item != null && item.getType() != Material.AIR) {
                    item = item.clone();
                    for (Map.Entry<Integer, ItemStack> excess : inv.addItem(new ItemStack[]{item.clone()}).entrySet()) {
                        player.getWorld().dropItemNaturally(player.getLocation(), excess.getValue());
                    }
                }
                ++n2;
            }
            if (!this.armour.isEmpty()) {
                int i = Math.min(3, this.armour.size());
                while (i >= 0) {
                    ItemStack stack = this.armour.get(i);
                    if (stack != null && stack.getType() != Material.AIR) {
                        int armourSlot = i + 36;
                        ItemStack previous = player.getInventory().getItem(armourSlot);
                        stack = stack.clone();
                        if (previous != null && previous.getType() != Material.AIR) {
                            player.getWorld().dropItemNaturally(player.getLocation(), stack);
                        } else {
                            player.getInventory().setItem(armourSlot, stack);
                        }
                    }
                    --i;
                }
            }
            player.updateInventory();
            if (inform) {
                player.sendMessage(
                        CoreConstants.YELLOW + String.format("Sucessfully applied the kit %1$s.", this.getName()));
            }

            return true;
        }
    }

    @Override
    public String getKitPermission() {
        return "core.kit." + name;
    }

    public static class builder {
        @Getter
        protected final UUID id;
        @Getter
        protected String name;
        @Getter
        @Setter
        protected List<ItemStack> items;
        @Getter
        @Setter
        protected List<ItemStack> armour;
        @Getter
        @Setter
        protected Collection<PotionEffect> effects;
        @Getter
        @Setter
        protected ItemStack image;
        @Getter
        @Setter
        protected boolean enabled;
        @Getter
        @Setter
        protected long delayMillis;
        @Getter
        @Setter
        protected long minPlaytimeMillis;
        @Getter
        @Setter
        protected int maximumUses;

        public builder(String name) {
            id = UUID.randomUUID();
            this.name = name;
            image = DEFAULT_IMAGE;
            effects = new ArrayList<>();
            armour = new ArrayList<>();
            items = new ArrayList<>();
            enabled = true;
            delayMillis = 0;
            minPlaytimeMillis = 0;
            maximumUses = Integer.MAX_VALUE;
        }

        public Kit build() {
            return new Kit(id, name, armour, armour, effects, image, enabled, delayMillis, delayMillis, maximumUses);
        }
    }

}
