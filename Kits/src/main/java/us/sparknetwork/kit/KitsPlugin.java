package us.sparknetwork.kit;

import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import us.sparknetwork.api.API;
import us.sparknetwork.api.command.CommandHandler;
import us.sparknetwork.api.kit.IKitManager;
import us.sparknetwork.core.commands.ReflectionCommandHandler;
import us.sparknetwork.kit.user.KitsUserManager;

public class KitsPlugin extends JavaPlugin {

    @Getter
    private static KitsPlugin plugin;
    @Getter
    private IKitManager kitManager;
    @Getter
    private KitsUserManager userManager;

    @Override
    public void onEnable() {
        this.plugin = this;
        this.kitManager = new KitManager(this);
        this.userManager = new KitsUserManager(this);
        Bukkit.getServer().getPluginManager().registerEvents(new KitListener(), this);
        CommandHandler handler = new ReflectionCommandHandler(this);
        handler.register(new KitExecutor());

    }
}
