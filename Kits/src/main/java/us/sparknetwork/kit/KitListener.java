package us.sparknetwork.kit;

import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;

import org.apache.commons.lang.time.DurationFormatUtils;
import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import us.sparknetwork.api.API;
import us.sparknetwork.api.kit.IKit;
import us.sparknetwork.api.user.IUser;
import us.sparknetwork.api.kit.event.KitApplyEvent;
import us.sparknetwork.core.CorePlugin;
import us.sparknetwork.util.ParticleEffect;

public class KitListener implements Listener {

    @EventHandler(ignoreCancelled = true)
    public void onKitSign(PlayerInteractEvent event) {
        if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
            Block block = event.getClickedBlock();
            BlockState state = block.getState();
            if (!(state instanceof Sign)) {
                return;
            }
            Sign sign = (Sign) state;
            String[] lines = sign.getLines();
            if (lines.length >= 2 && lines[1].contains("Kit")) {
                Optional<IKit> kit = KitsPlugin.getPlugin().getKitManager().getKit(lines.length >= 3 ? lines[2] : null);
                if (!kit.isPresent()) {
                    return;
                }
                event.setCancelled(true);
                Player player = event.getPlayer();
                String[] fakeLines = Arrays.copyOf(sign.getLines(), 4);
                boolean applied = kit.get().applyTo(player, false, false);
                if (applied) {
                    fakeLines[0] = ChatColor.GREEN + "Successfully";
                    fakeLines[1] = ChatColor.GREEN + "equipped kit";
                    fakeLines[2] = kit.get().getName();
                    fakeLines[3] = "";
                } else {
                    fakeLines[0] = ChatColor.RED + "Failed to";
                    fakeLines[1] = ChatColor.RED + "equip kit";
                    fakeLines[2] = kit.get().getName();
                    fakeLines[3] = ChatColor.RED + "Check chat";
                }
                if (CorePlugin.getInstance().getSignHandler().showLines(player, sign, fakeLines, 100, false)
                        && applied) {
                    ParticleEffect.SPLASH.display(player, sign.getLocation().clone().add(0.5, 0.5, 0.5), 0.01f, 10);
                }
            }
        }
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGH)
    public void onKitApply(KitApplyEvent event) {
        if (event.force) {
            return;
        }
        Player player = event.getPlayer();
        IKit kit = event.getKit();
        if (!player.isOp() && !kit.isEnabled()) {
            event.setCancelled(true);
            player.sendMessage(ChatColor.RED + "The " + kit.getName() + " kit is currently disabled.");
            return;
        }
        String kitPermission = kit.getKitPermission();
        if (kitPermission != null && !player.hasPermission(kitPermission)) {
            event.setCancelled(true);
            player.sendMessage(ChatColor.RED + "You do not have permission to use this kit.");
            return;
        }
        UUID uuid = player.getUniqueId();
        /*
		 * long minPlaytimeMillis = kit.getMinPlaytimeMillis(); if (minPlaytimeMillis >
		 * 0 && this.plugin.getPlayTimeManager().getTotalPlayTime(uuid) <
		 * minPlaytimeMillis) { player.sendMessage((Object) ChatColor.RED +
		 * "You need at least " + kit.getMinPlaytimeWords() +
		 * " minimum playtime to use kit " + kit.getDisplayName() + '.');
		 * event.setCancelled(true); return;
		 */
        IUser baseUser = API.get().getUserManager().getUser(uuid);
        if (!event.force) {
            long remaining = baseUser.getRemainingKitCooldown(kit);
            if (remaining > 0) {
                player.sendMessage(ChatColor.RED + "You cannot use the " + kit.getName() + " kit for "
                        + DurationFormatUtils.formatDurationWords(remaining, true, true)
                        + '.');
                event.setCancelled(true);
                return;
            }
            int curUses = baseUser.getKitUses(kit);
            int maxUses = kit.getMaximumUses();
            if (maxUses > 0) {
                if (maxUses != Integer.MAX_VALUE && curUses >= maxUses) {
                    player.sendMessage(ChatColor.RED + "You have already used this kit " + curUses + '/'
                            + maxUses + " times.");
                    event.setCancelled(true);
                }
            }
        }

    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onKitApplyMonitor(KitApplyEvent event) {
        if (!event.force) {
            if (event.isCancelled()) {
                return;
            }
            IKit kit = event.getKit();
            IUser baseUser = API.get().getUserManager().getUser(event.getPlayer().getUniqueId());
            baseUser.incrementKitUses(kit);
            baseUser.updateKitCooldown(kit);
        }
    }
}
