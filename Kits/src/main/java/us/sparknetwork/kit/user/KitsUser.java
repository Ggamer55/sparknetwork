package us.sparknetwork.kit.user;

import gnu.trove.map.TObjectIntMap;
import lombok.Getter;
import org.mongodb.morphia.annotations.Entity;
import us.sparknetwork.api.kit.IKit;
import us.sparknetwork.core.user.Participator;
import us.sparknetwork.util.MapUtils;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

public class KitsUser extends Participator {

    private UUID uniqueId;

    @Getter
    private Map<UUID, Integer> kitusesmap = new HashMap<>();
    @Getter
    private Map<UUID, Long> kitcooldownmap = new HashMap<>();


    public KitsUser(UUID uniqueId) {
        this.uniqueId = uniqueId;
    }

    public KitsUser(KitsUser user) {
        this.uniqueId = user.getUniqueId();
        this.kitcooldownmap = user.getKitcooldownmap();
        this.kitusesmap = user.getKitusesmap();
    }

    @SuppressWarnings("unchecked")
    public KitsUser(Map<String, Object> map) {
        this.uniqueId = UUID.fromString((String) map.get("uniqueId"));
        if (map.containsKey("kit-cooldown-map") && map.get("kit-cooldown-map") instanceof Map<?, ?>) {
            Map<String, Long> kcm = (Map<String, Long>) map.get("kit-cooldown-map");
            Map<UUID, Long> ukcm = new HashMap<>();
            kcm.forEach((key, value) -> {
                UUID u = UUID.fromString(key);
                ukcm.put(u, value);
            });
            this.kitcooldownmap = ukcm;
        } else {
            this.kitcooldownmap = new HashMap<>();
        }
        if (map.containsKey("kit-uses-map") && map.get("kit-uses-map") instanceof TObjectIntMap<?>) {
            Map<String, Integer> kcm = (Map<String, Integer>) map.get("kit-uses-map");

            Map<UUID, Integer> ukcm = new HashMap<>();
            kcm.forEach((key, value) -> {
                UUID u = UUID.fromString(key);
                ukcm.put(u, value);
            });
            this.kitusesmap = ukcm;
        } else {
            this.kitusesmap = new HashMap<>();
        }
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> map = new LinkedHashMap<>();
        HashMap<String, Long> kcm = new HashMap<>();
        this.kitcooldownmap.keySet().forEach(u -> {
            long kit = kitcooldownmap.get(u);
            kcm.put(u.toString(), kit);
        });
        HashMap<String, Integer> kum = new HashMap<>();
        this.kitusesmap.keySet().forEach(u -> {
            int kit = kitusesmap.get(u);
            kum.put(u.toString(), kit);
        });
        map.put("kit-cooldown-map", kcm);
        map.put("kit-uses-map", kum);
        return map;
    }


    public long getRemainingKitCooldown(IKit kit) {
        long remaining = 0;
        if (this.kitcooldownmap.containsKey(kit.getUniqueID())) {
            remaining = this.kitcooldownmap.get(kit.getUniqueID());
        }
        if (remaining == 0) {
            return 0;
        }
        return remaining - System.currentTimeMillis();
    }

    public void updateKitCooldown(IKit kit) {
        this.kitcooldownmap.put(kit.getUniqueID(), System.currentTimeMillis() + kit.getDelayMillis());
    }

    public int getKitUses(IKit kit) {
        int result = this.kitusesmap.get(kit.getUniqueID()) == null ? 0 : this.kitusesmap.get(kit.getUniqueID());
        return result == 0 ? 0 : result;
    }

    public int incrementKitUses(IKit kit) {
        return MapUtils.AdjustOrPutValueIntMap(this.kitusesmap, kit.getUniqueID(), 1, 1);
    }
}
