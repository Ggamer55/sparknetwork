package us.sparknetwork.kit.user;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import us.sparknetwork.util.Config;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class KitsUserManager {

    public JavaPlugin plugin;
    private Map<UUID, KitsUser> users;
    private Config config;

    public KitsUserManager(JavaPlugin plugin) {
        this.plugin = plugin;
        this.users = new ConcurrentHashMap<>();
        this.loadUsers();
    }


    public Map<UUID, KitsUser> getUserMap() {
        return this.users;
    }

    public KitsUser getUser(UUID uuid) {
        KitsUser user = this.users.get(uuid);
        if (user == null) {
            return new KitsUser(uuid);
        }
        return user;
    }


    public void loadUsers() {
        config = new Config(plugin, "kitsusers");
        ConfigurationSection sec = config.getConfigurationSection("users");
        Map<String,Object> map = sec.getValues(false);
        map.forEach((uuid,userobj) -> {
            KitsUser user = (KitsUser) userobj;
            users.put(user.getUniqueId(), user);
        });
    }

    public void saveUsers() {
        users.values().forEach(user -> {
            ConfigurationSection sec = config.getConfigurationSection("users");
            sec.set(user.getUniqueId().toString(), user);
        });
    }
}
