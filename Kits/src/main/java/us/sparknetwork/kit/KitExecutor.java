package us.sparknetwork.kit;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import us.sparknetwork.api.kit.IKit;
import us.sparknetwork.core.CoreConstants;
import us.sparknetwork.kit.arguments.*;
import us.sparknetwork.util.ArgumentExecutor;
import us.sparknetwork.util.BukkitUtils;
import us.sparknetwork.util.CommandArgument;
import us.sparknetwork.util.CommandWrapper;

import java.util.*;

public class KitExecutor extends ArgumentExecutor {

    public KitExecutor() {
        super("kit");
        this.addArgument(new KitCreateArgument());
        this.addArgument(new KitAddItemsArgument());
        this.addArgument(new KitApplyArgument());
        this.addArgument(new KitDeleteArgument());
        this.addArgument(new KitSetDelayArgument());
        this.addArgument(new KitSetMaxUses());
        this.addArgument(new KitListArgument());
    }

    @Override
    public boolean run(final CommandSender sender, final String[] args) {
        if (args.length < 1) {
            Collection<CommandArgument> arguments = new HashSet<>();
            arguments.addAll(this.arguments);
            CommandWrapper.printUsage(sender, label, arguments);
            sender.sendMessage(
                    CoreConstants.YELLOW + "/" + label + " <kitName> " + ChatColor.GRAY + "- Applies a kit.");
            return true;
        }
        CommandArgument argument = this.getArgument(args[0]);
        String permission = argument == null ? null : argument.getPermission();
        if (argument == null || permission != null && !sender.hasPermission(permission)) {
            String kitPermission;
            Optional<IKit> kit = KitsPlugin.getPlugin().getKitManager().getKit(args[0]);
            if (sender instanceof Player && kit.isPresent() && ((kitPermission = kit.get().getKitPermission()) == null
                    || sender.hasPermission(kitPermission))) {
                Player player = (Player) sender;
                kit.get().applyTo(player, false, true);
                return true;
            }
            sender.sendMessage(ChatColor.RED + "Kit or sub-command " + args[0] + " not found.");
            return true;
        }
        argument.onCommand(sender, this, label, args);
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        if (args.length != 1) {
            return super.onTabComplete(sender, command, label, args);
        } else {
            List<String> previous = super.onTabComplete(sender, command, label, args);
            ArrayList<String> kitNames = new ArrayList<>();
            Iterator<IKit> iterator = KitsPlugin.getPlugin().getKitManager().getKits().iterator();

            IKit kit;
            String permission;
            do {
                if (!iterator.hasNext()) {
                    ArrayList<String> previous1;
                    if (previous != null && !previous.isEmpty()) {
                        previous1 = new ArrayList<>(previous);
                        previous1.addAll(0, kitNames);
                    } else {
                        previous1 = kitNames;
                    }

                    return BukkitUtils.getCompletions(args, previous1);
                }

                kit = iterator.next();
                permission = kit.getKitPermission();
            } while (permission != null && !sender.hasPermission(permission));

            kitNames.add(kit.getName());

        }
        return new ArrayList<>();
    }

}
