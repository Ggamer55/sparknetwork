package us.sparknetwork.kit;

import org.apache.commons.collections4.map.CaseInsensitiveMap;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import us.sparknetwork.api.kit.IKit;
import us.sparknetwork.api.kit.IKitManager;
import us.sparknetwork.api.kit.event.KitRenameEvent;
import us.sparknetwork.util.Config;
import us.sparknetwork.util.GenericUtils;

import java.util.*;
import java.util.stream.Collectors;

public class KitManager implements Listener, IKitManager {

    private final Map<String, IKit> kitNameMap = new CaseInsensitiveMap<>();
    private final Map<UUID, IKit> kitUUIDMap = new HashMap<>();
    private final KitsPlugin plugin;
    private Config config;
    private List<IKit> kits = new ArrayList<>();

    public KitManager(KitsPlugin plugin) {
        this.plugin = plugin;
        this.reloadKitData();
        Bukkit.getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void onKitRename(KitRenameEvent event) {
        this.kitNameMap.remove(event.getOldName());
        this.kitNameMap.put(event.getNewName(), event.getKit());
    }

    public List<IKit> getKits() {
        return this.kits;
    }

    public Optional<IKit> getKit(UUID uuid) {
        return Optional.ofNullable(this.kitUUIDMap.get(uuid));
    }

    public Optional<IKit> getKit(String id) {
        return Optional.ofNullable(this.kitNameMap.get(id));
    }

    public boolean containsKit(IKit kit) {
        return this.kits.contains(kit);
    }

    public void createKit(IKit kit) {
        if (this.kits.add(kit)) {
            this.kitNameMap.put(kit.getName(), kit);
            this.kitUUIDMap.put(kit.getUniqueID(), kit);
        }
    }

    public void removeKit(IKit kit) {
        if (this.kits.remove(kit)) {
            this.kitNameMap.remove(kit.getName());
            this.kitUUIDMap.remove(kit.getUniqueID());
        }
    }

    public void reloadKitData() {
        this.config = new Config(plugin, "kits");
        Object object = this.config.get("kits");
        this.plugin.getUserManager().loadUsers();
        if (object instanceof List) {
            List<Kit> kitz = GenericUtils.createList(object, Kit.class);
            this.kits = kitz.stream().map(k -> (IKit) k).collect(Collectors.toList());
            for (IKit kit : this.kits) {
                this.kitNameMap.put(kit.getName(), kit);
                this.kitUUIDMap.put(kit.getUniqueID(), kit);
            }
        }
    }

    public void saveKitData() {
        this.plugin.getUserManager().saveUsers();
        this.config.set("kits", this.kits.stream().map(k -> (Kit) k).collect(Collectors.toList()));
        this.config.save();
    }
}