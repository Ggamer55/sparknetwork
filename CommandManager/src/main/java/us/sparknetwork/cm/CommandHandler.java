package us.sparknetwork.cm;

import com.esotericsoftware.reflectasm.FieldAccess;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandException;
import org.bukkit.command.CommandMap;
import org.bukkit.command.CommandSender;
import us.sparknetwork.cm.annotation.Command;
import us.sparknetwork.cm.command.BaseCommand;
import us.sparknetwork.cm.command.CommandModule;
import us.sparknetwork.cm.command.arguments.CommandContext;
import us.sparknetwork.util.ArrayUtils;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.*;
import java.util.stream.Collectors;

public class CommandHandler {

    private Map<String, BaseCommand> commandMap;
    /*alias, name*/
    private Map<String, String> aliasMap;


    public boolean dispatchCommand(CommandSender sender, String command) {
        String[] commandArgs = command.split(" ");
        String[] args = ArrayUtils.subArrayFromIndex(commandArgs, 1);

        Optional<BaseCommand> optCommand = this.getCommand(command);
        if (optCommand.isPresent()) {
            BaseCommand commandBase = optCommand.get();
            return commandBase.execute(sender, commandArgs[0], args);
        } else {
            return false;
        }
    }

    public BaseCommand[] getCommandsFromClass(CommandListener listener) {
        List<Method> methods = new ArrayList<>(Arrays.asList(listener.getClass().getMethods()));
        methods = methods.stream().filter(method -> method.getAnnotation(Command.class) != null).collect(Collectors.toList());
        BaseCommand[] commands = new BaseCommand[methods.size()];
        int i = 0;
        for (Method mthd : methods) {
            Optional<BaseCommand> optcmd = this.getCommandFromCommandMethod(mthd, listener);
            if (optcmd.isPresent()) {
                commands[i] = optcmd.get();
            }
        }
        return commands;
    }

    public Optional<BaseCommand> getCommandFromCommandMethod(Method method, CommandListener listener) {
        BaseCommand command = null;
        Command commandAnot = method.getAnnotation(Command.class);
        Parameter[] parameters = method.getParameters();
        if (commandAnot != null && parameters[0].getType() == CommandSender.class && parameters[1].getType() == CommandContext.class && (method.getReturnType() == Boolean.TYPE || method.getReturnType() == Boolean.class)) {
            command = new CommandModule(commandAnot.names()[0], commandAnot.min(), commandAnot.max()) {
                @Override
                public boolean execute(CommandSender sender, CommandContext args) {
                    try {
                        return (boolean) method.invoke(listener, sender, args);
                    } catch (Throwable t) {
                        throw new CommandException(ChatColor.RED + "Internal error while executing a command", t);
                    }
                }
            };
            for (String completeFlag : commandAnot.valueFlags()) {
                String[] flag = completeFlag.split(":");
                command.getValueFlags().put(flag[0].toCharArray()[0], flag[1]);
            }
            for (char noValueFlag : commandAnot.flags()) {
                command.getFlags().add(noValueFlag);
            }
            for (int i = 1; i < commandAnot.names().length; i++) {
                command.getAliases().add(commandAnot.names()[i]);
            }
            command.setDescription(commandAnot.desc());
            command.setPermission(commandAnot.permission());
            command.setUsage(commandAnot.usage());
        }
        return Optional.ofNullable(command);
    }

    public void registerCommand(BaseCommand command) {
        commandMap.putIfAbsent(command.getName(), command);
        command.getAliases().forEach(alias -> aliasMap.putIfAbsent(alias, command.getName()));
    }

    public Optional<BaseCommand> getCommand(String name) {
        String realName = this.getCommandNameFromAlias(name);
        BaseCommand command = this.commandMap.get(realName);
        return Optional.ofNullable(command);
    }

    public String getCommandNameFromAlias(String name) {
        if (commandMap.containsKey(name)) {
            return name;
        }
        String alias = aliasMap.get(name);
        return StringUtils.isBlank(alias) ? "" : alias;
    }


    public class CommandRegistererRunnable implements Runnable {
        CommandMap cmdMap;

        public void run() {
            if (cmdMap == null) {
                cmdMap = (CommandMap) FieldAccess.get(Bukkit.getServer().getClass()).get(Bukkit.getServer(), "commandMap");
            }
            commandMap.forEach((name, command) -> {
                cmdMap.register(name, command);
            });
        }
    }
}
