package us.sparknetwork.cm.command.arguments;


import lombok.Getter;
import org.apache.commons.lang.StringUtils;

import java.util.*;
import java.util.regex.Pattern;

public class CommandContext {

    @Getter
    private String commandName;
    @Getter
    private String currentLabel;
    private String[] originalArgs;
    private List<String> parsedArgs;
    @Getter
    private List<Character> noValueFlags;
    @Getter
    private Map<Character, String> valueFlags;

    private static Pattern flagRegex = Pattern.compile("[-]{1,2}[a-zA-Z]{1}");
    private static Pattern removeDash = Pattern.compile("[-]{1,2}");


    /**
     * This class is a style like arguments for a command that parse the flags
     *
     * @param command    - The executed command name
     * @param label      - The executed command current label
     * @param args       - The executed command not parsed args
     * @param flags      - The command flags without any value
     * @param valueFlags - The command flags with a variable value
     */
    public CommandContext(String command, String label, String[] args, List<Character> flags, Map<Character, String> valueFlags) {
        this.commandName = command;
        this.currentLabel = label;
        this.originalArgs = args;
        parsedArgs = new ArrayList<>();

        List<String> parsingArgs = new ArrayList<>(Arrays.asList(originalArgs));
        Iterator<String> argz = parsingArgs.iterator();

        this.noValueFlags = new ArrayList<>();
        this.valueFlags = new HashMap<>();

        while (argz.hasNext()) {
            String arg = argz.next();
            if (StringUtils.isBlank(arg)) {
                continue;
            } else if (flagRegex.matcher(arg).matches()) {
                String flag = removeDash.matcher(arg).replaceAll("");
                if ((flag.length() > 0)) {
                    if (flags.contains(flag.toCharArray()[0])) {
                        noValueFlags.add(flag.toCharArray()[0]);
                    } else if (valueFlags.containsKey(flag.toCharArray()[0])) {
                        String flagValue = argz.hasNext() ? argz.next() : valueFlags.get(flag.toCharArray()[0]);
                        this.valueFlags.putIfAbsent(flag.toCharArray()[0], flagValue);
                    } else {
                        char[] flagCharArray = flag.toCharArray();
                        for (char flagChar : flagCharArray) {
                            if (flags.contains(flagChar)) {
                                noValueFlags.add(flagChar);
                            } else if (valueFlags.containsKey(flagChar)) {
                                String flagValue = argz.hasNext() ? argz.next() : valueFlags.get(flagChar);
                                this.valueFlags.putIfAbsent(flagChar, flagValue);
                            } else {
                                parsedArgs.add(arg);
                            }
                        }
                    }
                } else {
                    continue;
                }
            } else {
                parsedArgs.add(arg);
            }

        }

    }

    public List<String> getArguments() {
        return this.parsedArgs;
    }

    public String getArgument(int index) {
        return parsedArgs.get(index);
    }

    public String getJoinedArgs(int startIndex) {
        StringBuilder joinedArgsBuilder = new StringBuilder();
        for (int i = startIndex; i < parsedArgs.size(); i++) {
            joinedArgsBuilder.append(parsedArgs.get(i));
            if (i < parsedArgs.size()) {
                joinedArgsBuilder.append(" ");
            }
        }
        return joinedArgsBuilder.toString();
    }

    public String getJoinedArgs(int startIndex, int endIndex) {
        StringBuilder joinedArgsBuilder = new StringBuilder();
        int index = Math.min(parsedArgs.size(), endIndex);
        for (int i = startIndex; i < index; i++) {
            joinedArgsBuilder.append(parsedArgs.get(i));
            if (i < parsedArgs.size()) {
                joinedArgsBuilder.append(" ");
            }
        }
        return joinedArgsBuilder.toString();
    }

    public String getFlagValue(Character character) {
        return valueFlags.containsKey(character) ? valueFlags.get(character) : "";
    }

}
