package us.sparknetwork.cm.command;

import lombok.Getter;
import org.bukkit.command.Command;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class BaseCommand extends Command {


    @Getter
    protected int minArgs;
    @Getter
    protected int maxArgs;
    @Getter
    protected List<Character> flags = new ArrayList<>();
    @Getter
    protected Map<Character, String> valueFlags = new HashMap<>();

    public BaseCommand(String command, int minArgs, int maxArgs) {
        super(command);
        this.minArgs = minArgs;
        this.maxArgs = maxArgs;
    }


}
