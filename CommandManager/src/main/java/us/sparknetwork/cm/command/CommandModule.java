package us.sparknetwork.cm.command;

import org.apache.commons.lang.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandException;
import org.bukkit.command.CommandSender;
import us.sparknetwork.cm.command.arguments.CommandContext;
import us.sparknetwork.cm.command.executor.CommandBaseExecutor;

/**
 * This is a class that represents a command with min and max args, and flags with or without value
 * that needs to be extended to be useful
 */
public abstract class CommandModule extends BaseCommand implements CommandBaseExecutor<CommandSender> {


    public CommandModule(String command, int minArgs, int maxArgs) {
        super(command, minArgs, maxArgs);
    }

    @Override
    public boolean execute(CommandSender sender, String label, String[] args) {
        boolean sucess = false;
        if (args.length > minArgs) {
            try {
                if (maxArgs > -1 && args.length <= maxArgs) {
                    CommandContext context = new CommandContext(this.getName(), label, args, this.getFlags(), this.getValueFlags());
                    sucess = this.execute(sender, context);
                } else {
                    CommandContext context = new CommandContext(this.getName(), label, args, this.getFlags(), this.getValueFlags());
                    sucess = this.execute(sender, context);
                }
            } catch (Throwable t) {
                throw new CommandException(ChatColor.RED + "Internal command error while executing the command", t);
            }
        }
        if (!sucess && !StringUtils.isBlank(this.usageMessage)) {
            String[] usage = usageMessage.split("\n");
            for (String str : usage) {
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', str.replace("<command>", label).replace("(command)", label)));
            }
        }
        return sucess;
    }
}
