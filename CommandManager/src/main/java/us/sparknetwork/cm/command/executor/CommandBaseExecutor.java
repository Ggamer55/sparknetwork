package us.sparknetwork.cm.command.executor;

import org.bukkit.command.CommandSender;
import us.sparknetwork.cm.command.arguments.CommandContext;

public interface CommandBaseExecutor<Sender extends CommandSender> {

    boolean execute(Sender sender, CommandContext args);
}
