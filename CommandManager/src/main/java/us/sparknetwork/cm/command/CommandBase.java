package us.sparknetwork.cm.command;

import lombok.Getter;
import org.apache.commons.lang.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandException;
import org.bukkit.command.CommandSender;
import us.sparknetwork.cm.command.arguments.CommandContext;
import us.sparknetwork.cm.command.executor.CommandBaseExecutor;

/**
 * This is a class that represents a command with min and max args, and flags with or without value
 * that needs a executor to be useful
 */
public abstract class CommandBase extends BaseCommand {

    @Getter
    private CommandBaseExecutor<CommandSender> executor;

    public CommandBase(String command, int minArgs, int maxArgs, CommandBaseExecutor<CommandSender> executor) {
        super(command, minArgs, maxArgs);
        this.executor = executor;
    }

    @Override
    public boolean execute(CommandSender sender, String label, String[] args) {
        boolean sucess = false;
        if (args.length > minArgs) {
            try {
                if (maxArgs > -1 && args.length <= maxArgs) {
                    CommandContext context = new CommandContext(this.getName(), label, args, flags, valueFlags);
                    sucess = executor.execute(sender, context);
                } else {
                    CommandContext context = new CommandContext(this.getName(), label, args, flags, valueFlags);
                    sucess = executor.execute(sender, context);
                }
            } catch (Throwable t) {
                throw new CommandException(ChatColor.RED + "Internal command error while executing the command", t);
            }
        }
        if (!sucess && !StringUtils.isBlank(this.usageMessage)) {
            String[] usage = usageMessage.split("\n");
            for (String str : usage) {
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', str.replace("<command>", label).replace("(command)", label)));
            }
        }
        return sucess;
    }

}
