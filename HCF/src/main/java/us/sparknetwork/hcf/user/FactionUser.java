package us.sparknetwork.hcf.user;

import lombok.Data;
import org.bukkit.Bukkit;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.entity.Player;
import us.sparknetwork.core.CorePlugin;
import us.sparknetwork.hcf.deathban.Deathban;
import us.sparknetwork.util.GenericUtils;

import java.util.*;
import java.util.stream.Collectors;

@Data
public class FactionUser implements ConfigurationSerializable {

    private final Set<UUID> factionChatSpying = new HashSet<>();
    private final Set<String> shownScoreboardScores = new HashSet<>();

    private final UUID userUUID;

    private boolean capzoneEntryAlerts;
    private boolean showClaimMap;
    private boolean showLightning = true;
    private Deathban deathban;
    private long lastFactionLeaveMillis;
    private int kills;
    private int deaths;
    private boolean reclaimed;
    private int lives;
    private boolean nocobble;
    private boolean foundDiamondAlerts;
    private int spawnCredits;

    public FactionUser(UUID userUUID) {
        this.userUUID = userUUID;
    }

    public FactionUser(Map<String, Object> map) {
        this.shownScoreboardScores.addAll(GenericUtils.createList(map.get("shownScoreboardScores"), String.class));
        this.factionChatSpying.addAll(GenericUtils.createList(map.get("faction-chat-spying"), String.class).stream()
                .map(UUID::fromString).collect(Collectors.toList()));
        this.userUUID = UUID.fromString((String) map.get("userUUID"));
        this.capzoneEntryAlerts = (Boolean) map.get("capzoneEntryAlerts");
        this.showClaimMap = (Boolean) map.get("showClaimMap");
        this.nocobble = (Boolean) map.get("noCobble");
        this.foundDiamondAlerts = (Boolean) map.get("foundDiamondAlerts");
        this.reclaimed = (Boolean) map.get("reclaimed");
        this.showLightning = (Boolean) map.get("showLightning");
        this.deathban = map.get("deathban") != null ? (Deathban) map.get("deathban") : null;
        this.lastFactionLeaveMillis = Long.parseLong((String) map.get("lastFactionLeaveMillis"));
        this.kills = (Integer) map.get("kills");
        this.deaths = (Integer) map.get("deaths");
        this.spawnCredits = (Integer) map.get("spawnCredits");
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("shownScoreboardScores", new ArrayList<>(shownScoreboardScores));
        map.put("faction-chat-spying", factionChatSpying.stream().map(UUID::toString).collect(Collectors.toList()));
        map.put("userUUID", userUUID.toString());
        map.put("capzoneEntryAlerts", capzoneEntryAlerts);
        map.put("showClaimMap", showClaimMap);
        map.put("showLightning", showLightning);
        map.put("noCobble", nocobble);
        map.put("foundDiamondAlerts", foundDiamondAlerts);
        map.put("reclaimed", reclaimed);
        map.put("lives", lives);
        map.put("deathban", deathban);
        map.put("lastFactionLeaveMillis", Long.toString(lastFactionLeaveMillis));
        map.put("kills", kills);
        map.put("deaths", deaths);
        map.put("spawnCredits", spawnCredits);
        return map;
    }

    /**
     * Checks if this faction user if showing the claim map.
     *
     * @return true if faction user is showing claim map
     */
    public boolean isShowClaimMap() {
        return this.showClaimMap;
    }

    /**
     * Sets if this faction user if showing the claim map.
     *
     * @param showClaimMap if faction user should show claim map
     */
    public void setShowClaimMap(boolean showClaimMap) {
        this.showClaimMap = showClaimMap;
    }

    public Player getPlayer() {
        return Bukkit.getPlayer(userUUID);
    }
}
