package us.sparknetwork.hcf.reclaim;

import lombok.Getter;
import org.bukkit.configuration.serialization.ConfigurationSerializable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Rank implements ConfigurationSerializable {
    @Getter
    private String name;
    @Getter
    private String displayName;
    @Getter
    private String broadcastMessage;
    @Getter
    private List<String> commands = new ArrayList<>();

    public Rank(String name, String displayName, String broadcastmessage, ArrayList<String> commands) {
        this.name = name;
        this.displayName = displayName;
        this.broadcastMessage = broadcastmessage;
        this.commands.clear();
        this.commands.addAll(commands);
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> map = new HashMap<>();
        map.put("name", this.name);
        map.put("displayName", this.displayName);
        map.put("broadcastMsg", this.broadcastMessage);
        map.put("commands", this.commands);
        return map;
    }

    @SuppressWarnings("unchecked")
    public static Rank deserialize(Map<String, Object> map) {
        if (map.containsKey("name") &&
                map.containsKey("displayName") &&
                map.containsKey("broadcastMsg") &&
                map.containsKey("commands")) {
            return new Rank((String) map.get("name"),(String) map.get("displayName"), (String) map.get("broadcastMsg"),
                    (ArrayList<String>) map.get("commands"));
        }
        return null;
    }
}
