package us.sparknetwork.hcf.reclaim;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import lombok.Getter;
import net.md_5.bungee.api.ChatColor;
import us.sparknetwork.hcf.HCF;
import us.sparknetwork.hcf.user.FactionUser;
import us.sparknetwork.util.Config;

public class ReclaimManager {
	@Getter
	private static ReclaimManager instance;
	private HCF plugin;
	private Config Reclaimdata;
	private Map<String, Rank> ranklist = new LinkedHashMap<>();

	public ReclaimManager(HCF plugin) {
		this.plugin = plugin;
		instance = this;
		this.reloadConfig();
	}

	public boolean reclaimRank(Player player) {
		FactionUser pd = plugin.getUserManager().getUser(player.getUniqueId());
		if (pd.isReclaimed()) {
			return false;
		}
		Rank chosenRank = null;
		for (Rank rank : ranklist.values()) {
			if (!player.hasPermission("hcf.reclaim.rank." + rank.getName())) {
				continue;
			}
			chosenRank = rank;
		}
		if(chosenRank != null){
			for (String cmd : chosenRank.getCommands()) {
				Bukkit.dispatchCommand(Bukkit.getConsoleSender(), cmd.replace("%player%", player.getName()));
			}
			for (String bc : ChatColor.translateAlternateColorCodes('&', chosenRank.getBroadcastMessage()).replace("%rankname%", chosenRank.getName())
					.replace("%player%", player.getName()).split("\n")) {
				Bukkit.broadcastMessage(bc);
			}
			pd.setReclaimed(true);
			return true;
		}
		return false;

	}

	public void reloadConfig() {
		this.Reclaimdata = new Config(plugin, "reclaim");
		if (Reclaimdata.getConfigurationSection("ranks") == null) {
			Reclaimdata.createSection("ranks");
		}
		for (String string : Reclaimdata.getConfigurationSection("ranks").getKeys(false)) {
			Rank rnk = Rank.deserialize(Reclaimdata.getConfigurationSection("ranks." + string).getValues(false));
			ranklist.put(string, rnk);
		}
	}

}
