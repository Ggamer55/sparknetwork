package us.sparknetwork.hcf.faction;

import java.util.Arrays;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import us.sparknetwork.core.commands.ArgumentExecutor;
import us.sparknetwork.hcf.HCF;
import us.sparknetwork.hcf.faction.argument.FactionAcceptArgument;
import us.sparknetwork.hcf.faction.argument.FactionAllyArgument;
import us.sparknetwork.hcf.faction.argument.FactionAnnouncementArgument;
import us.sparknetwork.hcf.faction.argument.FactionChatArgument;
import us.sparknetwork.hcf.faction.argument.FactionClaimArgument;
import us.sparknetwork.hcf.faction.argument.FactionClaimChunkArgument;
import us.sparknetwork.hcf.faction.argument.FactionClaimsArgument;
import us.sparknetwork.hcf.faction.argument.FactionCreateArgument;
import us.sparknetwork.hcf.faction.argument.FactionDemoteArgument;
import us.sparknetwork.hcf.faction.argument.FactionDepositArgument;
import us.sparknetwork.hcf.faction.argument.FactionDisbandArgument;
import us.sparknetwork.hcf.faction.argument.FactionHelpArgument;
import us.sparknetwork.hcf.faction.argument.FactionHomeArgument;
import us.sparknetwork.hcf.faction.argument.FactionInviteArgument;
import us.sparknetwork.hcf.faction.argument.FactionInvitesArgument;
import us.sparknetwork.hcf.faction.argument.FactionKickArgument;
import us.sparknetwork.hcf.faction.argument.FactionLeaderArgument;
import us.sparknetwork.hcf.faction.argument.FactionLeaveArgument;
import us.sparknetwork.hcf.faction.argument.FactionListArgument;
import us.sparknetwork.hcf.faction.argument.FactionMapArgument;
import us.sparknetwork.hcf.faction.argument.FactionMessageArgument;
import us.sparknetwork.hcf.faction.argument.FactionOpenArgument;
import us.sparknetwork.hcf.faction.argument.FactionPromoteArgument;
import us.sparknetwork.hcf.faction.argument.FactionRenameArgument;
import us.sparknetwork.hcf.faction.argument.FactionSetHomeArgument;
import us.sparknetwork.hcf.faction.argument.FactionShowArgument;
import us.sparknetwork.hcf.faction.argument.FactionStuckArgument;
import us.sparknetwork.hcf.faction.argument.FactionSubclaimArgumentExecutor;
import us.sparknetwork.hcf.faction.argument.FactionUnallyArgument;
import us.sparknetwork.hcf.faction.argument.FactionUnclaimArgument;
import us.sparknetwork.hcf.faction.argument.FactionUninviteArgument;
import us.sparknetwork.hcf.faction.argument.FactionUnsubclaimArgument;
import us.sparknetwork.hcf.faction.argument.FactionWithdrawArgument;
import us.sparknetwork.hcf.faction.argument.staff.FactionChatSpyArgument;
import us.sparknetwork.hcf.faction.argument.staff.FactionClaimForArgument;
import us.sparknetwork.hcf.faction.argument.staff.FactionClearClaimsArgument;
import us.sparknetwork.hcf.faction.argument.staff.FactionForceDemoteArgument;
import us.sparknetwork.hcf.faction.argument.staff.FactionForceJoinArgument;
import us.sparknetwork.hcf.faction.argument.staff.FactionForceKickArgument;
import us.sparknetwork.hcf.faction.argument.staff.FactionForceLeaderArgument;
import us.sparknetwork.hcf.faction.argument.staff.FactionForcePromoteArgument;
import us.sparknetwork.hcf.faction.argument.staff.FactionRemoveArgument;
import us.sparknetwork.hcf.faction.argument.staff.FactionSetDeathbanMultiplierArgument;
import us.sparknetwork.hcf.faction.argument.staff.FactionSetDtrArgument;
import us.sparknetwork.hcf.faction.argument.staff.FactionSetDtrRegenArgument;
import us.sparknetwork.core.commands.ArgumentExecutor;

import us.sparknetwork.core.commands.CommandArgument;

/**
 * Class to handle the command and tab completion for the faction command.
 */
public class FactionExecutor extends ArgumentExecutor {

    private final CommandArgument helpArgument;

    public FactionExecutor(HCF plugin) {
        super("faction");
        this.setAliases(Arrays.asList("f"));
        addArgument(new FactionAcceptArgument(plugin));
        addArgument(new FactionAllyArgument(plugin));
        addArgument(new FactionAnnouncementArgument(plugin));
        addArgument(new FactionChatArgument(plugin));
        addArgument(new FactionChatSpyArgument(plugin));
        addArgument(new FactionClaimArgument(plugin));
        addArgument(new FactionClaimChunkArgument(plugin));
        addArgument(new FactionClaimForArgument(plugin));
        addArgument(new FactionClaimsArgument(plugin));
        addArgument(new FactionClearClaimsArgument(plugin));
        addArgument(new FactionCreateArgument(plugin));
        addArgument(new FactionDemoteArgument(plugin));
        addArgument(new FactionDepositArgument(plugin));
        addArgument(new FactionDisbandArgument(plugin));
        addArgument(new FactionSetDtrRegenArgument(plugin));
        addArgument(new FactionForceDemoteArgument(plugin));
        addArgument(new FactionForceJoinArgument(plugin));
        addArgument(new FactionForceKickArgument(plugin));
        addArgument(new FactionForceLeaderArgument(plugin));
        addArgument(new FactionForcePromoteArgument(plugin));
        addArgument(helpArgument = new FactionHelpArgument(this));
        addArgument(new FactionHomeArgument(this, plugin));
        addArgument(new FactionInviteArgument(plugin));
        addArgument(new FactionInvitesArgument(plugin));
        addArgument(new FactionKickArgument(plugin));
        addArgument(new FactionLeaderArgument(plugin));
        addArgument(new FactionLeaveArgument(plugin));
        addArgument(new FactionListArgument(plugin));
        addArgument(new FactionMapArgument(plugin));
        addArgument(new FactionMessageArgument(plugin));
        addArgument(new FactionOpenArgument(plugin));
        addArgument(new FactionRemoveArgument(plugin));
        addArgument(new FactionRenameArgument(plugin));
        addArgument(new FactionPromoteArgument(plugin));
        addArgument(new FactionSetDtrArgument(plugin));
        addArgument(new FactionSetDeathbanMultiplierArgument(plugin));
        addArgument(new FactionSetHomeArgument(plugin));
        addArgument(new FactionShowArgument(plugin));
        addArgument(new FactionStuckArgument(plugin));
        addArgument(new FactionSubclaimArgumentExecutor(plugin));
        addArgument(new FactionUnclaimArgument(plugin));
        addArgument(new FactionUnallyArgument(plugin));
        addArgument(new FactionUninviteArgument(plugin));
        addArgument(new FactionUnsubclaimArgument(plugin));
        addArgument(new FactionWithdrawArgument(plugin));
    }

    @Override
    public boolean run(CommandSender sender, String[] args) {
        if (args.length < 1) {
            helpArgument.onCommand(sender, this, this.nextCommandLabel, args);
            return true;
        }

        CommandArgument argument = (CommandArgument) getArgument(args[0]);
        if (argument != null) {
            String permission = argument.getPermission();
            if (permission == null || sender.hasPermission(permission)) {
                argument.onCommand(sender, this, label, args);
                return true;
            }
        }

        helpArgument.onCommand(sender, this, label, args);
        return true;
    }
}
