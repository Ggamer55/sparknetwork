package us.sparknetwork.hcf.faction.event.cause;

public enum ClaimChangeCause {

    UNCLAIM, CLAIM, RESIZE
}
