package us.sparknetwork.hcf.faction.type;

import java.util.Map;

import org.bukkit.command.CommandSender;

import us.sparknetwork.hcf.ConfigurationService;

/**
 * Represents the {@link WildernessFaction}.
 */
public class WildernessFaction extends Faction {

    public WildernessFaction() {
        super("Wilderness");
    }

    public WildernessFaction(Map<String, Object> map) {
        super(map);
    }

    @Override
    public String getDisplayName(CommandSender sender) {
        return ConfigurationService.WILDERNESS_COLOUR + "The Wilderness";
    }
}
