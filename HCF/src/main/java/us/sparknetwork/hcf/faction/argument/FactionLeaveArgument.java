package us.sparknetwork.hcf.faction.argument;

import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import us.sparknetwork.core.CoreConstants;
import us.sparknetwork.hcf.HCF;
import us.sparknetwork.hcf.faction.struct.Relation;
import us.sparknetwork.hcf.faction.struct.Role;
import us.sparknetwork.hcf.faction.type.PlayerFaction;
import us.sparknetwork.core.commands.CommandArgument;

public class FactionLeaveArgument extends CommandArgument {

    private final HCF plugin;

    public FactionLeaveArgument(HCF plugin) {
        super("leave", "Leave your current faction.");
        this.plugin = plugin;
    }

    @Override
    public String getUsage(String label) {
        return '/' + label + ' ' + getName();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Only players can leave faction.");
            return true;
        }

        Player player = (Player) sender;
        PlayerFaction playerFaction = plugin.getFactionManager().getPlayerFaction(player);

        if (playerFaction == null) {
            sender.sendMessage(ChatColor.RED + "You are not in a faction.");
            return true;
        }

        UUID uuid = player.getUniqueId();
        if (playerFaction.getMember(uuid).getRole() == Role.LEADER) {
            sender.sendMessage(ChatColor.RED + "You cannot leave factions as a leader. Either use " + CoreConstants.GOLD + '/' + label + " disband" + ChatColor.RED + " or " + CoreConstants.GOLD + '/' + label
                    + " leader" + ChatColor.RED + '.');

            return true;
        }

        if (playerFaction.removeMember(player, player, player.getUniqueId(), false, false)) {
            sender.sendMessage(CoreConstants.YELLOW + "Successfully left the faction.");
            playerFaction.broadcast(Relation.ENEMY.toChatColour() + sender.getName() + CoreConstants.YELLOW + " has left the faction.");
        }

        return true;
    }
}
