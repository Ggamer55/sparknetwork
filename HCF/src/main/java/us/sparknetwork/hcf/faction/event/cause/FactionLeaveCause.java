package us.sparknetwork.hcf.faction.event.cause;

public enum FactionLeaveCause {

    KICK, LEAVE, DISBAND
}