package us.sparknetwork.hcf.sotw;

import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.time.DurationFormatUtils;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

import com.google.common.collect.ImmutableList;

import us.sparknetwork.api.command.CommandModule;
import us.sparknetwork.hcf.ConfigurationService;
import us.sparknetwork.hcf.HCF;
import us.sparknetwork.util.BukkitUtils;
import us.sparknetwork.util.JavaUtils;

public class SotwCommand extends CommandModule implements TabCompleter {

	private static final List<String> COMPLETIONS = ImmutableList.of("start", "end");

	private final HCF plugin;

	public SotwCommand(HCF plugin) {
		super("sotw", 0);
		this.plugin = plugin;
	}

	@Override
	public boolean run(CommandSender sender, String[] args) {
		if (args.length > 0) {
			if (args[0].equalsIgnoreCase("start")) {
				if (args.length < 2) {
					SotwTimer.SotwRunnable sotwRunnable = plugin.getSotwTimer().getSotwRunnable();

					if (sotwRunnable != null) {
						sender.sendMessage(
								ChatColor.RED + "SOTW protection is already enabled, use /" + getLabel() + " cancel to end it.");
						return true;
					}

					plugin.getSotwTimer().start(ConfigurationService.HCF_DIFFICULTY.getSOTWTime());
					sender.sendMessage(ChatColor.RED + "Started SOTW protection for "
							+ DurationFormatUtils.formatDurationWords(ConfigurationService.HCF_DIFFICULTY.getSOTWTime(), true, true) + ".");
					return true;
				}

				long duration = JavaUtils.parse(args[1]);

				if (duration == -1L) {
					sender.sendMessage(ChatColor.RED + "'" + args[0] + "' is an invalid duration.");
					return true;
				}

				if (duration < 1000L) {
					sender.sendMessage(ChatColor.RED + "SOTW protection time must last for at least 20 ticks.");
					return true;
				}

				SotwTimer.SotwRunnable sotwRunnable = plugin.getSotwTimer().getSotwRunnable();

				if (sotwRunnable != null) {
					sender.sendMessage(
							ChatColor.RED + "SOTW protection is already enabled, use /" + getLabel() + " cancel to end it.");
					return true;
				}

				plugin.getSotwTimer().start(duration);
				sender.sendMessage(ChatColor.RED + "Started SOTW protection for "
						+ DurationFormatUtils.formatDurationWords(duration, true, true) + ".");
				return true;
			}

			if (args[0].equalsIgnoreCase("end") || args[0].equalsIgnoreCase("cancel")) {
				if (plugin.getSotwTimer().cancel()) {
					sender.sendMessage(ChatColor.RED + "Cancelled SOTW protection.");
					return true;
				}

				sender.sendMessage(ChatColor.RED + "SOTW protection is not active.");
				return true;
			}
		}

		sender.sendMessage(ChatColor.RED + "Usage: /" + getLabel() + " <start|end>");
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
		return args.length == 1 ? BukkitUtils.getCompletions(args, COMPLETIONS) : Collections.emptyList();
	}
}
