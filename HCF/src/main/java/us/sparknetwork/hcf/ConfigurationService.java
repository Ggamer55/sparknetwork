package us.sparknetwork.hcf;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.time.DurationFormatUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionType;

import com.google.common.collect.ImmutableList;

import us.sparknetwork.core.CoreConstants;
import us.sparknetwork.util.Config;
import us.sparknetwork.util.JavaUtils;
import us.sparknetwork.util.PersistableLocation;

public class ConfigurationService {

	public static void init(JavaPlugin plugin) {
		Config config = new Config(plugin, "settings");

		DISABLE_OBSIDIAN_GENERATORS = config.getBoolean("disable-obsidian-generators", DISABLE_OBSIDIAN_GENERATORS);

		SERVER_TIME_ZONE = TimeZone.getTimeZone(config.getString("server_time_zone", DEFAULT_SERVER_TIME_ZONE));
		WARZONE_RADIUS = config.getInt("warzone.radius", WARZONE_RADIUS);

		MAP_NUMBER = config.getDouble("map-number", MAP_NUMBER);

		KIT_MAP = config.getBoolean("kit-map.enabled", KIT_MAP);
		BALANCE_PER_KILL = config.getInt("kit-map.balance-per-kill", BALANCE_PER_KILL);

		List<String> disallowedFactionNames = config.getStringList("factions.disallowed-faction.names");
		DISALLOWED_FACTION_NAMES = disallowedFactionNames != null ? disallowedFactionNames : DISALLOWED_FACTION_NAMES;

		ConfigurationSection enc = config.getConfigurationSection("enchantment-limits");
		if (enc != null) {
			ENCHANTMENT_LIMITS.clear();
			for (String key : enc.getKeys(false)) {
				Enchantment e = Enchantment.getByName(key);
				if (e != null)
					ENCHANTMENT_LIMITS.put(e, enc.getInt(key));
				else
					plugin.getLogger().severe("Could not find enchantment with name " + key);
			}
		}
		for (java.util.Map.Entry<Enchantment, Integer> e : ENCHANTMENT_LIMITS.entrySet()) {
			plugin.getLogger().info(e.getKey().getName() + " : " + e.getValue());
		}

		ConfigurationSection pot = config.getConfigurationSection("potion-limits");
		if (pot != null) {
			POTION_LIMITS.clear();
			for (String key : pot.getKeys(false)) {
				PotionType potion = PotionType.valueOf(key);
				if (potion != null)
					POTION_LIMITS.put(potion, pot.getInt(key));
				else
					plugin.getLogger().severe("Could not find potion with name " + key);
			}
		}
		for (java.util.Map.Entry<PotionType, Integer> e : POTION_LIMITS.entrySet()) {
			plugin.getLogger().info(e.getKey().name() + " : " + e.getValue());
		}

		ConfigurationSection road = config.getConfigurationSection("roads.lengths");
		if (road != null) {
			ROAD_LENGTHS.clear();
			for (String key : road.getKeys(false)) {
				World.Environment env = World.Environment.valueOf(key);
				if (env != null)
					ROAD_LENGTHS.put(env, road.getInt(key));
				else
					plugin.getLogger().severe("Could not find world type with name " + key);
			}
		}

		ConfigurationSection bor = config.getConfigurationSection("border-sizes");
		if (bor != null) {
			BORDER_SIZES.clear();
			for (String key : bor.getKeys(false)) {
				World.Environment env = World.Environment.valueOf(key);
				if (env != null)
					BORDER_SIZES.put(env, bor.getInt(key));
				else
					plugin.getLogger().severe("Could not find world type with name " + key);
			}
		}

		ConfigurationSection spa = config.getConfigurationSection("spawn-radius-map");
		if (spa != null) {
			SPAWN_RADIUS_MAP.clear();
			for (String key : spa.getKeys(false)) {
				World.Environment env = World.Environment.valueOf(key);
				if (env != null)
					SPAWN_RADIUS_MAP.put(env, spa.getInt(key));
				else
					plugin.getLogger().severe("Could not find world type with name " + key);
			}
		}

		DIAMOND_ORE_ALERTS = config.getBoolean("diamond-ore-notifications", DIAMOND_ORE_ALERTS);

		SUBCLAIM_NAME_CHARACTERS_MIN = config.getInt("subclaim.min-name-characters", SUBCLAIM_NAME_CHARACTERS_MIN);
		SUBCLAIM_NAME_CHARACTERS_MAX = config.getInt("subclaim.max-name-characters", SUBCLAIM_NAME_CHARACTERS_MAX);

		FACTION_NAME_CHARACTERS_MIN = config.getInt("factions.min-name-characters", FACTION_NAME_CHARACTERS_MIN);
		FACTION_NAME_CHARACTERS_MAX = config.getInt("factions.max-name-characters", FACTION_NAME_CHARACTERS_MAX);
		MAX_MEMBERS_PER_FACTION = config.getInt("factions.max-members", MAX_MEMBERS_PER_FACTION);

		ROAD_MIN_HEIGHT = config.getInt("roads.min-height", ROAD_MIN_HEIGHT);
		ROAD_MAX_HEIGHT = config.getInt("roads.max-height", ROAD_MAX_HEIGHT);
		ROAD_WIDTH = config.getInt("roads.width", ROAD_WIDTH);

		END_PORTAL_RADIUS = config.getInt("end.portal-radius", END_PORTAL_RADIUS);
		END_PORTAL_CENTER = config.getInt("end.portal-center", END_PORTAL_CENTER);
		END_OPEN = config.getBoolean("end.open");
		END_SPAWN = (PersistableLocation) config.get("end.spawn");
		END_EXIT = (PersistableLocation) config.get("end.exit");

		DEFAULT_DEATHBAN_DURATION = config.getLong("deathban.base-duration", DEFAULT_DEATHBAN_DURATION);
		BASE_REGEN_DELAY = JavaUtils.parse(config.getString("deathban.base-regen-delay"));

		TEAMMATE_COLOUR = ChatColor.valueOf(config.getString("relation-colours.teammate", "GREEN"));
		ALLY_COLOUR = ChatColor.valueOf(config.getString("relation-colours.ally", "BLUE"));
		ENEMY_COLOUR = ChatColor.valueOf(config.getString("relation-colours.enemy", "RED"));

		SAFEZONE_COLOUR = ChatColor.valueOf(config.getString("relation-colours.safezone", "AQUA"));
		ROAD_COLOUR = ChatColor.valueOf(config.getString("relation-colours.road", "YELLOW"));
		WARZONE_COLOUR = ChatColor.valueOf(config.getString("relation-colours.warzone", "DARK_RED"));
		WILDERNESS_COLOUR = ChatColor.valueOf(config.getString("relation-colours.wilderness", "GREEN"));

		TITLE = config.getString("info.title");

		SCOREBOARD_TITLE = config.getString("scoreboard.sidebar.title", SCOREBOARD_TITLE);
		SCOREBOARD_TITLE = SCOREBOARD_TITLE.replaceAll("%MAP_NUMBER%", MAP_NUMBER + "");
		SCOREBOARD_TITLE = ChatColor.translateAlternateColorCodes('&', SCOREBOARD_TITLE).replace("%TITLE%", TITLE);
		SCOREBOARD_ENABLED = config.getBoolean("scoreboard.sidebar.enabled", SCOREBOARD_ENABLED);

		MAX_ALLIES_PER_FACTION = config.getInt("factions.max-allies", MAX_ALLIES_PER_FACTION);
		MAX_CLAIMS_PER_FACTION = config.getInt("factions.max-claims", MAX_CLAIMS_PER_FACTION);
		ALLOW_CLAIMING_BESIDES_ROADS = config.getBoolean("roads.allow-claim-besides", ALLOW_CLAIMING_BESIDES_ROADS);

		DTR_MILLIS_BETWEEN_UPDATES = config.getLong("deaths-till-raidable.millis-between-updates",
				DTR_MILLIS_BETWEEN_UPDATES);
		// DTR words not configurable

		DTR_INCREMENT_BETWEEN_UPDATES = config.getDouble("deaths-till-raidable.increment-between-updates",
				DTR_INCREMENT_BETWEEN_UPDATES);
		MAXIMUM_DTR = config.getDouble("deaths-till-raidable.maximum", MAXIMUM_DTR);

		EXP_MULTIPLIER_GENERAL = config.getDouble("exp-multiplier.global", EXP_MULTIPLIER_GENERAL);
		EXP_MULTIPLIER_FISHING = config.getDouble("exp-multiplier.fishing", EXP_MULTIPLIER_FISHING);
		EXP_MULTIPLIER_SMELTING = config.getDouble("exp-multiplier.smelting", EXP_MULTIPLIER_SMELTING);
		EXP_MULTIPLIER_LOOTING_PER_LEVEL = config.getDouble("exp-multiplier.looting-per-level",
				EXP_MULTIPLIER_LOOTING_PER_LEVEL);
		EXP_MULTIPLIER_LUCK_PER_LEVEL = config.getDouble("exp-multiplier.luck-per-level",
				EXP_MULTIPLIER_LUCK_PER_LEVEL);
		EXP_MULTIPLIER_FORTUNE_PER_LEVEL = config.getDouble("exp-multiplier.fortune-per-level",
				EXP_MULTIPLIER_FORTUNE_PER_LEVEL);

		CONQUEST_POINT_LOSS_PER_DEATH = config.getInt("conquest.point-loss-per-death", CONQUEST_POINT_LOSS_PER_DEATH);
		CONQUEST_REQUIRED_WIN_POINTS = config.getInt("conquest.victory-points", CONQUEST_REQUIRED_WIN_POINTS);

		FOUND_DIAMONDS_ALERTS = config.getBoolean("diamond-ore-notification", FOUND_DIAMONDS_ALERTS);
		COMBAT_LOG_DESPAWN_TICKS = config.getLong("combatlog.despawn-ticks", COMBAT_LOG_DESPAWN_TICKS);
		COMBAT_LOG_PREVENTION_ENABLED = config.getBoolean("combatlog.enabled", COMBAT_LOG_PREVENTION_ENABLED);

		SPAWNER_PRICE = config.getInt("spawner-shop.price");
		SPAWNERSHOP_ENABLED = config.getBoolean("spawner-shop.enabled");
		HCF_DIFFICULTY = HCFType.valueOf(config.getString("hcf-difficulty"));
	}

	public static boolean DISABLE_OBSIDIAN_GENERATORS = true;

	private static String DEFAULT_SERVER_TIME_ZONE = "GMT+1";
	public static TimeZone SERVER_TIME_ZONE = TimeZone.getTimeZone(DEFAULT_SERVER_TIME_ZONE);
	public static int WARZONE_RADIUS = 850;

	public static String TITLE = "HCF";

	public static double MAP_NUMBER = 2.0;

	public static boolean KIT_MAP = false;

	public static int BALANCE_PER_KILL = 250;

	public static List<String> DISALLOWED_FACTION_NAMES = ImmutableList.of("kohieotw", "kohisotw", "hcteams",
			"hcteamseotw", "hcteamssotw", "para", "parahcf", "parasotw", "paraeotw");

	public static Map<Enchantment, Integer> ENCHANTMENT_LIMITS = new HashMap<>();
	public static Map<PotionType, Integer> POTION_LIMITS = new EnumMap<>(PotionType.class);
	public static Map<World.Environment, Integer> ROAD_LENGTHS = new EnumMap<>(World.Environment.class);
	public static Map<World.Environment, Integer> ROAD_WIDTHS = new EnumMap<>(World.Environment.class);
	public static final Map<World.Environment, Integer> BORDER_SIZES = new EnumMap<>(World.Environment.class);
	public static Map<World.Environment, Integer> SPAWN_RADIUS_MAP = new EnumMap<>(World.Environment.class);
	public static boolean DIAMOND_ORE_ALERTS = false;
	public static boolean END_OPEN = true;
	public static PersistableLocation END_SPAWN = new PersistableLocation(Bukkit.getWorld("world_the_end"), 0, 90, 0, 0,
			0);
	public static PersistableLocation END_EXIT = new PersistableLocation(Bukkit.getWorld("world"), 0, 90, 0, 0, 0);
	public static int SPAWNER_PRICE = 40000;
	public static boolean SPAWNERSHOP_ENABLED = true;
	public static long BASE_REGEN_DELAY = TimeUnit.MINUTES.toMillis(40);

	static {
		POTION_LIMITS.put(PotionType.STRENGTH, 0);
		POTION_LIMITS.put(PotionType.WEAKNESS, 0);
		POTION_LIMITS.put(PotionType.SLOWNESS, 0);
		POTION_LIMITS.put(PotionType.INVISIBILITY, 0);
		POTION_LIMITS.put(PotionType.POISON, 0);

		ENCHANTMENT_LIMITS.put(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
		ENCHANTMENT_LIMITS.put(Enchantment.DAMAGE_ALL, 1);
		ENCHANTMENT_LIMITS.put(Enchantment.ARROW_KNOCKBACK, 0);
		ENCHANTMENT_LIMITS.put(Enchantment.KNOCKBACK, 0);
		ENCHANTMENT_LIMITS.put(Enchantment.FIRE_ASPECT, 0);
		ENCHANTMENT_LIMITS.put(Enchantment.THORNS, 0);
		ENCHANTMENT_LIMITS.put(Enchantment.ARROW_FIRE, 1);
		ENCHANTMENT_LIMITS.put(Enchantment.ARROW_DAMAGE, 4);

		ROAD_LENGTHS.put(World.Environment.NORMAL, 4000);
		ROAD_LENGTHS.put(World.Environment.NETHER, 4000);

		BORDER_SIZES.put(World.Environment.NORMAL, 2000);
		BORDER_SIZES.put(World.Environment.NETHER, 1000);
		BORDER_SIZES.put(World.Environment.THE_END, 1500);

		ROAD_WIDTHS.put(World.Environment.NORMAL, 25);
		ROAD_WIDTHS.put(World.Environment.NETHER, 10);

		SPAWN_RADIUS_MAP.put(World.Environment.NORMAL, 50);
		SPAWN_RADIUS_MAP.put(World.Environment.NETHER, 25);
		SPAWN_RADIUS_MAP.put(World.Environment.THE_END, 15);
	}

	public static int SUBCLAIM_NAME_CHARACTERS_MIN = 3;
	public static int SUBCLAIM_NAME_CHARACTERS_MAX = 16;

	public static int FACTION_NAME_CHARACTERS_MIN = 3;
	public static int FACTION_NAME_CHARACTERS_MAX = 16;
	public static int MAX_MEMBERS_PER_FACTION = 25;

	public static int ROAD_MIN_HEIGHT = 0; // 50 'this allowed people to claim below the roads, temp disabled;
	public static int ROAD_MAX_HEIGHT = 256; // 80 'this allowed people to claim above the roads, temp disabled;
	public static int ROAD_WIDTH = 3;

	public static int END_PORTAL_RADIUS = 20;
	public static int END_PORTAL_CENTER = 500;

	public static long DEFAULT_DEATHBAN_DURATION = TimeUnit.HOURS.toMillis(1L);

	// Faction tag colours.
	public static ChatColor TEAMMATE_COLOUR = ChatColor.GREEN;
	public static ChatColor ALLY_COLOUR = ChatColor.LIGHT_PURPLE;
	public static ChatColor ENEMY_COLOUR = ChatColor.RED;

	public static ChatColor SAFEZONE_COLOUR = ChatColor.AQUA;
	public static ChatColor ROAD_COLOUR = CoreConstants.GOLD;
	public static ChatColor WARZONE_COLOUR = ChatColor.LIGHT_PURPLE;
	public static ChatColor WILDERNESS_COLOUR = ChatColor.DARK_GREEN;

	public static String SCOREBOARD_TITLE = ChatColor.GREEN.toString() + ChatColor.BOLD + TITLE + ChatColor.RED
			+ "[Map " + MAP_NUMBER + "]   ";
	public static boolean SCOREBOARD_ENABLED = true;
	public static int MAX_ALLIES_PER_FACTION = 0;
	public static int MAX_CLAIMS_PER_FACTION = 8;
	public static boolean ALLOW_CLAIMING_BESIDES_ROADS = true;

	public static long DTR_MILLIS_BETWEEN_UPDATES = TimeUnit.SECONDS.toMillis(45L);
	public static String DTR_WORDS_BETWEEN_UPDATES = DurationFormatUtils.formatDurationWords(DTR_MILLIS_BETWEEN_UPDATES,
			true, true);

	public static double DTR_INCREMENT_BETWEEN_UPDATES = 0.1;
	public static double MAXIMUM_DTR = 6.0;

	public static double EXP_MULTIPLIER_GENERAL = 2.0;
	public static double EXP_MULTIPLIER_FISHING = 2.0;
	public static double EXP_MULTIPLIER_SMELTING = 2.0;
	public static double EXP_MULTIPLIER_LOOTING_PER_LEVEL = 1.5;
	public static double EXP_MULTIPLIER_LUCK_PER_LEVEL = 1.5;
	public static double EXP_MULTIPLIER_FORTUNE_PER_LEVEL = 1.5;

	public static int CONQUEST_POINT_LOSS_PER_DEATH = 20;
	public static int CONQUEST_REQUIRED_WIN_POINTS = 300;

	public static boolean FOUND_DIAMONDS_ALERTS = true;
	public static long COMBAT_LOG_DESPAWN_TICKS = TimeUnit.SECONDS.toMillis(30L) / 50L;
	public static boolean COMBAT_LOG_PREVENTION_ENABLED = true;

	public static HCFType HCF_DIFFICULTY = HCFType.EASY;
}
