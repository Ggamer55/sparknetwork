package us.sparknetwork.hcf.command;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import us.sparknetwork.api.command.CommandModule;
import us.sparknetwork.core.CoreConstants;
import us.sparknetwork.hcf.HCF;
import us.sparknetwork.hcf.scoreboard.PlayerBoard;

/**
 * Command used to toggle the sidebar for a {@link Player}.
 */
public class ToggleSidebarCommand extends CommandModule implements TabCompleter {

	private final HCF plugin;

	public ToggleSidebarCommand(HCF plugin) {
		super("togglesidebar", 0, 0, "Usage /(command)", Arrays.asList("togglescoreboard", "tsb"));
		this.plugin = plugin;
	}

	@Override
	public boolean run(CommandSender sender, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(ChatColor.RED + "This command is only executable by players.");
			return true;
		}

		PlayerBoard playerBoard = plugin.getScoreboardHandler().getPlayerBoard(((Player) sender).getUniqueId());
		boolean newVisibile = !playerBoard.isSidebarVisible();
		playerBoard.setSidebarVisible(newVisibile);

		sender.sendMessage(CoreConstants.YELLOW + "Scoreboard sidebar is "
				+ (newVisibile ? ChatColor.GREEN + "now" : ChatColor.RED + "no longer") + CoreConstants.YELLOW
				+ " visible.");
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
		return Collections.emptyList();
	}
}