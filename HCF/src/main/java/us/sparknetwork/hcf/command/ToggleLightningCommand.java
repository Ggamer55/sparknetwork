package us.sparknetwork.hcf.command;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.server.PluginDisableEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionType;
import us.sparknetwork.api.command.CommandModule;
import us.sparknetwork.core.CoreConstants;
import us.sparknetwork.hcf.ConfigurationService;
import us.sparknetwork.hcf.HCF;
import us.sparknetwork.hcf.user.FactionUser;
import us.sparknetwork.util.InventoryUtils;
import us.sparknetwork.util.ItemBuilder;

import java.util.*;

/**
 * Command used to toggle the lightning strikes on death for a {@link Player}.
 */
public class ToggleLightningCommand extends CommandModule implements TabCompleter {

    private final HCF plugin;

    public ToggleLightningCommand(HCF plugin) {
        super("togglelightning", 0, 0, "Usage /(command)");
        this.plugin = plugin;
    }

    @Override
    public boolean run(CommandSender sender, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "This command is only executable by players.");
            return true;
        }

        FactionUser factionUser = plugin.getUserManager().getUser(((Player) sender).getUniqueId());
        boolean newShowLightning = !factionUser.isShowLightning();
        factionUser.setShowLightning(newShowLightning);

        sender.sendMessage(ChatColor.AQUA + "You will now "
                + (newShowLightning ? ChatColor.GREEN + "able" : ChatColor.RED + "unable") + ChatColor.AQUA
                + " to see lightning strikes on death.");

        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        return Collections.emptyList();
    }

    public static class MapKitCommand extends CommandModule implements TabCompleter, Listener {

        private final Set<Inventory> tracking = new HashSet<>();

        public MapKitCommand(HCF plugin) {
            super("mapkit", 0, 0, "Usage /(command)", Arrays.asList("kitmap", "viewkit"));
            plugin.getServer().getPluginManager().registerEvents(this, plugin);
        }

        @Override
        public boolean run(CommandSender sender, String[] args) {
            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.RED + "This command is only executable by players.");
                return true;
            }

            List<ItemStack> items = new ArrayList<>();

            for (Map.Entry<Enchantment, Integer> entry : ConfigurationService.ENCHANTMENT_LIMITS.entrySet()) {
                items.add(new ItemBuilder(Material.ENCHANTED_BOOK).displayName(
                        CoreConstants.YELLOW + StringUtils.capitalize(entry.getKey().getName()) + ": " + ChatColor.GREEN + entry.getValue())
                        .build());
            }

            for (Map.Entry<PotionType, Integer> entry : ConfigurationService.POTION_LIMITS.entrySet()) {
                items.add(new ItemBuilder(new Potion(entry.getKey()).toItemStack(1))
                        .displayName(CoreConstants.YELLOW + WordUtils.capitalizeFully(entry.getKey().name().replace('_', ' '))
                                + ": " + ChatColor.GREEN + entry.getValue())
                        .build());
            }

            Player player = (Player) sender;
            Inventory inventory = Bukkit.createInventory(player, InventoryUtils.getSafestInventorySize(items.size()),
                    ConfigurationService.TITLE + " Map " + ConfigurationService.MAP_NUMBER + " Kit");
            tracking.add(inventory);
            for (ItemStack item : items) {
                inventory.addItem(item);
            }

            player.openInventory(inventory);
            return true;
        }

        @Override
        public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
            return Collections.emptyList();
        }

        @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
        public void onInventoryClick(InventoryClickEvent event) {
            if (tracking.contains(event.getInventory())) {
                event.setCancelled(true);
            }
        }

        @EventHandler(ignoreCancelled = true, priority = EventPriority.MONITOR)
        public void onPluginDisable(PluginDisableEvent event) {
            for (Inventory inventory : tracking) {
                Collection<HumanEntity> viewers = new HashSet<>(inventory.getViewers());
                for (HumanEntity viewer : viewers) {
                    viewer.closeInventory();
                }
            }
        }
    }
}
