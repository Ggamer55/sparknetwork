package us.sparknetwork.hcf.command;

import java.util.Collections;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import us.sparknetwork.api.command.CommandModule;
import us.sparknetwork.core.CoreConstants;
import us.sparknetwork.hcf.HCF;
import us.sparknetwork.hcf.faction.type.Faction;

/**
 * Command used to check current the current {@link Faction} at the position of
 * a given {@link Player}s {@link Location}.
 */
public class LocationCommand extends CommandModule implements TabCompleter {

	private final HCF plugin;

	public LocationCommand(HCF plugin) {
		super("location", 0, 1, "Usage /(command) [player]");
		this.plugin = plugin;
	}

	@Override
	public boolean run(CommandSender sender, String[] args) {
		Player target;
		if (args.length >= 1 && sender.hasPermission(getPermission() + ".others")) {
			target = Bukkit.getPlayer(args[0]);
		} else if (sender instanceof Player) {
			target = (Player) sender;
		} else {
			return false;
		}

		if (target == null || sender instanceof Player && !((Player) sender).canSee(target)) {
			sender.sendMessage(
					CoreConstants.GOLD + "Player '" + ChatColor.WHITE + args[0] + CoreConstants.GOLD + "' not found.");
			return true;
		}

		Location location = target.getLocation();
		Faction factionAt = plugin.getFactionManager().getFactionAt(location);
		sender.sendMessage(CoreConstants.YELLOW + target.getName() + " is in the territory of "
				+ factionAt.getDisplayName(sender) + CoreConstants.YELLOW + '('
				+ (factionAt.isSafezone() ? ChatColor.GREEN + "Non-Deathban" : ChatColor.RED + "Deathban")
				+ CoreConstants.YELLOW + ')');

		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
		return args.length == 1 && sender.hasPermission(command.getPermission() + ".others") ? null
				: Collections.emptyList();
	}
}
