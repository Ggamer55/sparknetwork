package us.sparknetwork.hcf.command;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import us.sparknetwork.api.command.CommandModule;
import us.sparknetwork.core.CoreConstants;
import us.sparknetwork.hcf.HCF;
import us.sparknetwork.hcf.user.FactionUser;
import us.sparknetwork.util.PlayerUtils;

public class SetReclaimedCommand extends CommandModule {

	HCF plugin;

	public SetReclaimedCommand(HCF plugin) {
		super("setreclaimed", 2, 2, "Usage /(command) <player> <true/false>");
		this.plugin = plugin;
	}

	@Override
	public boolean run(CommandSender sender, String[] args) {
		Player target = PlayerUtils.getPlayer(args[0]);
		if (target == null) {
			sender.sendMessage(String.format(CoreConstants.PLAYER_WITH_NAME_OR_UUID, args[0]));
			return true;
		}
		FactionUser pd = plugin.getUserManager().getUser(target.getUniqueId());
		boolean bool = false;
		bool = Boolean.parseBoolean(args[1]);
		pd.setReclaimed(bool);
		sender.sendMessage(CoreConstants.YELLOW + "Sucesfully set the reclaimed ranks of %player% to %bool%"
				.replace("%player%", target.getName()).replace("%bool%", bool + ""));
		return true;
	}

}
