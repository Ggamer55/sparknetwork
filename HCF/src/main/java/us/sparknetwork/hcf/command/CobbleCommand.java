package us.sparknetwork.hcf.command;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerPickupItemEvent;

import us.sparknetwork.api.command.CommandModule;
import us.sparknetwork.hcf.HCF;
import us.sparknetwork.hcf.user.FactionUser;

public class CobbleCommand extends CommandModule implements Listener {

	public CobbleCommand(HCF plugin) {
		super("cobble", 0, 0, "Usage: /(command)");
	}

	@Override
	public boolean run(CommandSender sender, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(ChatColor.RED + "Only players can execute this command.");
			return true;
		}

		Player player = (Player) sender;
		FactionUser user = HCF.getPlugin().getUserManager().getUser(player.getUniqueId());

		if (user.isNocobble()) {
			user.setNocobble(false);
			player.sendMessage(ChatColor.GREEN + "You have enabled cobblestone pickups.");
		} else {
			user.setNocobble(true);
			player.sendMessage(ChatColor.RED + "You have disabled cobblestone pickups.");
		}
		return true;

	}

	@EventHandler
	public void onPlayerPickup(PlayerPickupItemEvent event) {
		Material type = event.getItem().getItemStack().getType();
		FactionUser user = HCF.getPlugin().getUserManager().getUser(event.getPlayer().getUniqueId());

		if (type == Material.STONE || type == Material.COBBLESTONE) {
			if (user.isNocobble()) {
				event.setCancelled(true);
			}
		}
	}

}