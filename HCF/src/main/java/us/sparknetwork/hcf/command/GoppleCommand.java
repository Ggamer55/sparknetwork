package us.sparknetwork.hcf.command;

import java.util.Collections;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import us.sparknetwork.api.command.CommandModule;
import us.sparknetwork.core.CoreConstants;
import us.sparknetwork.hcf.DurationFormatter;
import us.sparknetwork.hcf.HCF;
import us.sparknetwork.hcf.timer.PlayerTimer;

/**
 * Command used to check remaining Notch Apple cooldown time for {@link Player}.
 */
public class GoppleCommand extends CommandModule implements TabCompleter {

	private final HCF plugin;

	public GoppleCommand(HCF plugin) {
		super("gopple", 0, 0, "Usage /(command)");
		this.plugin = plugin;
	}

	@Override
	public boolean run(CommandSender sender, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(ChatColor.RED + "This command is only executable by players.");
			return true;
		}

		Player player = (Player) sender;

		PlayerTimer timer = plugin.getTimerManager().getGappleTimer();
		long remaining = timer.getRemaining(player);

		if (remaining <= 0L) {
			sender.sendMessage(ChatColor.RED + "Your " + timer.getDisplayName() + ChatColor.RED
					+ " timer is currently not active.");
			return true;
		}

		sender.sendMessage(CoreConstants.YELLOW + "Your " + timer.getDisplayName() + CoreConstants.YELLOW
				+ " timer is active for another " + ChatColor.BOLD
				+ DurationFormatter.getRemaining(remaining, true, false) + CoreConstants.YELLOW + '.');

		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
		return Collections.emptyList();
	}
}
