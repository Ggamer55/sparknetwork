package us.sparknetwork.hcf.command;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import us.sparknetwork.api.command.CommandModule;
import us.sparknetwork.core.CoreConstants;
import us.sparknetwork.hcf.ConfigurationService;
import us.sparknetwork.hcf.HCF;
import us.sparknetwork.hcf.user.FactionUser;
import us.sparknetwork.util.PlayerUtils;

public class SpawnCommand extends CommandModule {

    public SpawnCommand() {
        super("spawn", 0, 1, "Usage: /<command> [player]");
    }

    @Override
    public boolean run(CommandSender sender, String[] args) {
        if (!(sender instanceof Player) && args.length == 0) {
            return false;
        }
        // Staff teleport
        if (sender.hasPermission(this.getPermission() + ".teleport")) {
            // Another player staff teleport
            if (args.length == 1) {
                Player player = PlayerUtils.getPlayer(args[0]);
                player.teleport(player.getWorld().getSpawnLocation());
                broadcastCommandMessage(sender, CoreConstants.YELLOW + String.format("The player %1$s was teleported to the spawn.", player.getName()));
                return true;
            }
            // Self staff teleport
            Player player = (Player) sender;
            player.teleport(player.getWorld().getSpawnLocation());
            broadcastCommandMessage(sender, CoreConstants.YELLOW + String.format("The player %1$s was teleported to the spawn.", player.getName()));
            return true;
        }
        if (args.length == 0) {
            // Normal spawn teleport
            Player player = (Player) sender;
            if (!ConfigurationService.KIT_MAP) {
                FactionUser user = HCF.getPlugin().getUserManager().getUser(player.getUniqueId());
                if (user.getSpawnCredits() < 1) {
                    player.sendMessage(ChatColor.RED + "You must walk to the spawn, that is located at (0,0), you can buy a Spawn Credit to teleport to the spawn.");
                    return true;
                }
                user.setSpawnCredits(user.getSpawnCredits() - 1);
            }
            HCF.getPlugin().getTimerManager().getSpawnTimer().teleport(player, CoreConstants.YELLOW + "You are being teleported to the spawn, please wait.");
            return true;
        }
        return false;
    }
}
