package us.sparknetwork.hcf.command;

import java.util.Collections;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import us.sparknetwork.api.command.CommandModule;
import us.sparknetwork.hcf.HCF;
import us.sparknetwork.hcf.timer.type.LogoutTimer;

public class LogoutCommand extends CommandModule implements TabCompleter {

	private final HCF plugin;

	public LogoutCommand(HCF plugin) {
		super("logout", 0, 0, "Usage /(command)");
		this.plugin = plugin;
	}

	@Override
	public boolean run(CommandSender sender, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(ChatColor.RED + "This command is only executable by players.");
			return true;
		}

		Player player = (Player) sender;
		LogoutTimer logoutTimer = plugin.getTimerManager().getLogoutTimer();

		if (!logoutTimer.setCooldown(player, player.getUniqueId())) {
			sender.sendMessage(ChatColor.RED + "Your " + logoutTimer.getDisplayName() + ChatColor.RED
					+ " timer is already active.");
			return true;
		}

		sender.sendMessage(
				ChatColor.RED + "Your " + logoutTimer.getDisplayName() + ChatColor.RED + " timer has started.");
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
		return Collections.emptyList();
	}
}
