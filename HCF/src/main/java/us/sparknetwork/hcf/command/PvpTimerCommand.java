package us.sparknetwork.hcf.command;

import java.util.Collections;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import com.google.common.collect.ImmutableList;

import us.sparknetwork.api.command.CommandModule;
import us.sparknetwork.core.CoreConstants;
import us.sparknetwork.hcf.DurationFormatter;
import us.sparknetwork.hcf.HCF;
import us.sparknetwork.hcf.timer.type.InvincibilityTimer;
import us.sparknetwork.util.BukkitUtils;

/**
 * Command used to manage the {@link InvincibilityTimer} of {@link Player}s.
 */
public class PvpTimerCommand extends CommandModule implements TabCompleter {

	private final HCF plugin;

	public PvpTimerCommand(HCF plugin) {
		super("pvptimer", 0, 1, "Usage /(command)", "pvp");
		this.plugin = plugin;
	}

	@Override
	public boolean run(CommandSender sender, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(ChatColor.RED + "This command is only executable by players.");
			return true;
		}

		Player player = (Player) sender;
		InvincibilityTimer pvpTimer = plugin.getTimerManager().getInvincibilityTimer();

		if (pvpTimer == null)
			return true;

		if (args.length < 1) {
			printUsage(sender, getLabel(), pvpTimer);
			return true;
		}

		if (args[0].equalsIgnoreCase("enable") || args[0].equalsIgnoreCase("remove")
				|| args[0].equalsIgnoreCase("off")) {
			if (pvpTimer.getRemaining(player) <= 0L) {
				sender.sendMessage(ChatColor.RED + "Your " + pvpTimer.getDisplayName() + ChatColor.RED
						+ " timer is currently not active.");
				return true;
			}

			sender.sendMessage(CoreConstants.YELLOW + "Your " + pvpTimer.getDisplayName() + CoreConstants.YELLOW
					+ " timer is now off.");
			pvpTimer.clearCooldown(player);
			return true;
		}

		if (args[0].equalsIgnoreCase("remaining") || args[0].equalsIgnoreCase("time")
				|| args[0].equalsIgnoreCase("left") || args[0].equalsIgnoreCase("check")) {
			long remaining = pvpTimer.getRemaining(player);
			if (remaining <= 0L) {
				sender.sendMessage(ChatColor.RED + "Your " + pvpTimer.getDisplayName() + ChatColor.RED
						+ " timer is currently not active.");
				return true;
			}

			sender.sendMessage(CoreConstants.YELLOW + "Your " + pvpTimer.getDisplayName() + CoreConstants.YELLOW
					+ " timer is active for another " + ChatColor.BOLD
					+ DurationFormatter.getRemaining(remaining, true, false) + CoreConstants.YELLOW
					+ (pvpTimer.isPaused(player) ? " and is currently paused" : "") + '.');

			return true;
		}

		printUsage(sender, getLabel(), pvpTimer);
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
		return args.length == 1 ? BukkitUtils.getCompletions(args, COMPLETIONS) : Collections.emptyList();
	}

	private static final ImmutableList<String> COMPLETIONS = ImmutableList.of("enable", "time");

	/**
	 * Prints the usage of this command to a sender.
	 *
	 * @param sender
	 *            the sender to print for
	 * @param label
	 *            the label used for command
	 */
	private void printUsage(CommandSender sender, String label, InvincibilityTimer pvpTimer) {
		sender.sendMessage(CoreConstants.GOLD + "*** " + pvpTimer.getName() + " Help ***");
		sender.sendMessage(CoreConstants.GRAY + "/" + label + " enable - Removes your " + pvpTimer.getDisplayName()
				+ CoreConstants.GRAY + " timer.");
		sender.sendMessage(CoreConstants.GRAY + "/" + label + " time - Check remaining " + pvpTimer.getDisplayName()
				+ CoreConstants.GRAY + " time.");
		sender.sendMessage(CoreConstants.GRAY + "/lives - Life and deathban related commands.");
	}
}
