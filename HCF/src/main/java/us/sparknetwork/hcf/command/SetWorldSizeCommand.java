package us.sparknetwork.hcf.command;

import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;
import us.sparknetwork.api.command.CommandModule;
import us.sparknetwork.core.CoreConstants;
import us.sparknetwork.hcf.ConfigurationService;
import us.sparknetwork.hcf.HCF;
import us.sparknetwork.util.Config;

public class SetWorldSizeCommand extends CommandModule {

	public SetWorldSizeCommand() {
		super("setworldsize", 1, 1, "Usage /(command) <size>");
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean run(CommandSender sender, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(ChatColor.RED + "Only players can execute this command");
			return true;
		}
		int size = 0;
		try {
			size = Integer.parseInt(args[0]);
		} catch (NumberFormatException e) {
			sender.sendMessage(
					CoreConstants.GOLD + String.format("Number %1$s not found, provide a valid one.", args[0]));
			return true;
		}
		World.Environment env = ((Player) sender).getWorld().getEnvironment();
		ConfigurationService.BORDER_SIZES.put(env, size);
		Config cfg = new Config(HCF.getPlugin(), "settings");
		cfg.set("border_sizes." + env.toString(), size);
		cfg.save();
		sender.sendMessage(CoreConstants.YELLOW + String.format("Sucessfully set the border size to %1$s.", size));
		return true;
	}

}
