package us.sparknetwork.hcf.command;

import java.util.Collections;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

import us.sparknetwork.api.command.CommandModule;
import us.sparknetwork.hcf.HCF;

public class CoordsCommand extends CommandModule implements TabCompleter {

	private final HCF plugin;

	public CoordsCommand(HCF plugin) {
		super("coords", 0, 0, "Usage /(command)");
		this.plugin = plugin;
	}

	@Override
	public boolean run(CommandSender sender, String[] args) {
		sender.sendMessage(plugin.getExtraConfiguration().getCoordsMessage());
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
		return Collections.emptyList();
	}
}
