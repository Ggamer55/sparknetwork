package us.sparknetwork.hcf.command;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World.Environment;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import us.sparknetwork.api.command.CommandModule;
import us.sparknetwork.core.CoreConstants;
import us.sparknetwork.hcf.ConfigurationService;
import us.sparknetwork.hcf.HCF;
import us.sparknetwork.util.Config;
import us.sparknetwork.util.PersistableLocation;

public class SetEndExitCommand extends CommandModule {
	public SetEndExitCommand() {
		super("setendexit", 1, 1, "Usage /(command)");
	}

	@Override
	public boolean run(CommandSender sender, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(ChatColor.RED + "Only players can use this command");
			return true;
		}
		Location playerlocation = ((Player) sender).getLocation();
		if (playerlocation.getWorld().getEnvironment() == Environment.THE_END) {
			sender.sendMessage(ChatColor.RED + "You can't set the endexit in the end.");
			return true;
		}
		ConfigurationService.END_EXIT = new PersistableLocation(playerlocation);
		Config cfg = new Config(HCF.getPlugin(), "settings");
		cfg.set("end.exit", ConfigurationService.END_EXIT);
		cfg.save();
		sender.sendMessage(CoreConstants.YELLOW + "Sucessfully setted the endexit location.");
		return true;
	}
}
