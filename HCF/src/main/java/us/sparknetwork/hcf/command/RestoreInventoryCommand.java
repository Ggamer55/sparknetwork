package us.sparknetwork.hcf.command;

import com.google.common.collect.ImmutableList;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;
import us.sparknetwork.api.command.CommandModule;
import us.sparknetwork.core.CoreConstants;
import us.sparknetwork.hcf.HCF;
import us.sparknetwork.util.PlayerUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class RestoreInventoryCommand extends CommandModule implements Listener {

    private Map<UUID, ItemStack[]> armorList = new HashMap<>();
    private Map<UUID, ItemStack[]> contentsList = new HashMap<>();

    public RestoreInventoryCommand(HCF plugin) {
        super("restoreinventory", 1, 1, "Usage /(command) <player>",
                ImmutableList.of("restore", "invrestore", "restoreinv", "refund", "refundinv", "refundinventory"));
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void onDeath(PlayerDeathEvent event) {
        armorList.put(event.getEntity().getUniqueId(), event.getEntity().getInventory().getArmorContents());
        contentsList.put(event.getEntity().getUniqueId(), event.getEntity().getInventory().getArmorContents());
    }

    @Override
    public boolean run(CommandSender sender, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "You must be a player to execute this command.");
            return true;
        }

        Player target = PlayerUtils.getPlayer(args[0]);

        if (target == null) {
            sender.sendMessage(String.format(CoreConstants.PLAYER_WITH_NAME_OR_UUID, args[0]));
            return true;
        }

        if (!armorList.containsKey(target.getUniqueId()) && !contentsList.containsKey(target.getUniqueId())) {
            sender.sendMessage(CoreConstants.GOLD + String.format(
                    "No refund inventory found of %1$s maybe the server restarted, they don't died or the inventory was already restored.",
                    target.getName()));
            return true;
        }

        target.getInventory().setArmorContents(armorList.get(target.getUniqueId()));
        target.getInventory().setContents(contentsList.get(target.getUniqueId()));

        target.updateInventory();

        broadcastCommandMessage(sender,
                CoreConstants.YELLOW + String.format("Sucessfully restored the inventory of %1$s.", target.getName()));
        target.sendMessage(CoreConstants.YELLOW + String.format("Your inventory was restored by %1$s.", sender.getName()));
        armorList.remove(target.getUniqueId());
        contentsList.remove(target.getUniqueId());
        return true;
    }
}
