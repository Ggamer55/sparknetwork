package us.sparknetwork.hcf.command;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.ImmutableList;

import us.sparknetwork.api.command.CommandModule;
import us.sparknetwork.core.CoreConstants;
import us.sparknetwork.hcf.HCF;
import us.sparknetwork.hcf.listener.Crowbar;

public class GiveCrowbarCommand extends CommandModule {

	public GiveCrowbarCommand(HCF plugin) {
		super("crowgive", 1, 1, ChatColor.RED + "Usage: /(command) <player>",
				ImmutableList.of("crowbargive", "crowbar", "crowgive"));
	}

	@Override
	public boolean run(CommandSender sender, String[] args) {

		Player target = Bukkit.getServer().getPlayer(args[0]);

		if (target == null) {
			sender.sendMessage(String.format(CoreConstants.PLAYER_WITH_NAME_OR_UUID, args[0]));
			return true;
		}

		ItemStack stack = new Crowbar().getItemIfPresent();

		target.getInventory().addItem(stack);
		target.sendMessage(ChatColor.GREEN + "You were given a CROWBAR from " + sender.getName());

		sender.sendMessage(ChatColor.GREEN + "You have given " + target.getName() + " a CROWBAR");
		return true;
	}

}