package us.sparknetwork.hcf.command;

import java.util.Collections;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import us.sparknetwork.api.command.CommandModule;
import us.sparknetwork.hcf.HCF;
import us.sparknetwork.hcf.user.FactionUser;

/**
 * Command used to toggle messages shown when entering or leaving
 */
public class ToggleCapzoneEntryCommand extends CommandModule implements TabCompleter {

    private final HCF plugin;

    public ToggleCapzoneEntryCommand(HCF plugin) {
    	super("togglecapzoneentry",0,0,"Usage /(command)");
        this.plugin = plugin;
    }

    @Override
    public boolean run(CommandSender sender, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "This command is only executable by players.");
            return true;
        }

        FactionUser factionUser = plugin.getUserManager().getUser(((Player) sender).getUniqueId());
        boolean newStatus = !factionUser.isCapzoneEntryAlerts();
        factionUser.setCapzoneEntryAlerts(newStatus);

        sender.sendMessage(ChatColor.AQUA + "You will now " + (newStatus ? ChatColor.GREEN.toString() : ChatColor.RED + "un") + "able" + ChatColor.AQUA + " to see capture zone entry messages.");

        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        return Collections.emptyList();
    }
}
