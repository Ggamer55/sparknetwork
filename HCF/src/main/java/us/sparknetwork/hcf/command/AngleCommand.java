package us.sparknetwork.hcf.command;

import java.util.Collections;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import us.sparknetwork.api.command.CommandModule;
import us.sparknetwork.core.CoreConstants;
import us.sparknetwork.util.JavaUtils;

/**
 * Command used to check the angle and yaw positions of {@link Player}s.
 */
public class AngleCommand extends CommandModule implements TabCompleter {

	public AngleCommand() {
		super("angle", 0, 0, "Usage /(command)");
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean run(CommandSender sender, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(ChatColor.RED + "This command is only executable by players.");
			return true;
		}

		Location location = ((Player) sender).getLocation();

		sender.sendMessage(CoreConstants.GOLD + JavaUtils.format(location.getYaw()) + " yaw" + ChatColor.WHITE + ", "
				+ CoreConstants.GOLD + JavaUtils.format(location.getPitch()) + " pitch");
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
		return Collections.emptyList();
	}
}
