package us.sparknetwork.hcf.command;

import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.time.FastDateFormat;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

import us.sparknetwork.api.command.CommandModule;
import us.sparknetwork.hcf.ConfigurationService;

/**
 * Command used to check the current time for the server.
 */
public class ServerTimeCommand extends CommandModule implements TabCompleter {

	public ServerTimeCommand() {
		super("servertime", 0, 0,"Usage /(command)");
		// TODO Auto-generated constructor stub
	}

	private static final FastDateFormat FORMAT = FastDateFormat.getInstance("E MMM dd h:mm:ssa z yyyy",
			ConfigurationService.SERVER_TIME_ZONE);

	@Override
	public boolean run(CommandSender sender, String[] args) {
		sender.sendMessage(ChatColor.GREEN + "The server time is " + ChatColor.LIGHT_PURPLE
				+ FORMAT.format(System.currentTimeMillis()) + ChatColor.GREEN + '.');
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
		return Collections.emptyList();
	}
}
