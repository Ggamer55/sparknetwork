package us.sparknetwork.hcf.command;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;
import us.sparknetwork.api.command.CommandModule;
import us.sparknetwork.core.CoreConstants;
import us.sparknetwork.hcf.reclaim.ReclaimManager;

public class ReclaimCommand extends CommandModule {

	public ReclaimCommand() {
		super("reclaim", 0, 0, "Usage /(command)", "dr");
	}

	@Override
	public boolean run(CommandSender sender, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(ChatColor.RED + "Only players can use this command");
			return true;
		}
		Player target = (Player) sender;
		if (ReclaimManager.getInstance().reclaimRank(target)) {
			sender.sendMessage(CoreConstants.YELLOW + "Sucessfully reclaimed your rank.");
			return true;
		}
		sender.sendMessage(CoreConstants.GOLD + "You don't have any ranks to reclaim.");
		return true;
	}

}
