package us.sparknetwork.hcf.command;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World.Environment;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import us.sparknetwork.api.command.CommandModule;
import us.sparknetwork.core.CoreConstants;
import us.sparknetwork.hcf.ConfigurationService;
import us.sparknetwork.hcf.HCF;
import us.sparknetwork.util.Config;
import us.sparknetwork.util.PersistableLocation;

public class SetEndSpawnCommand extends CommandModule {

	public SetEndSpawnCommand() {
		super("setendspawn", 0, 0, "Usage /(command)");
	}

	@Override
	public boolean run(CommandSender sender, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(ChatColor.RED + "Only players can use this command");
			return true;
		}
		Location playerlocation = ((Player) sender).getLocation();
		if (playerlocation.getWorld().getEnvironment() != Environment.THE_END) {
			sender.sendMessage(ChatColor.RED + "You can't set the endspawn in a world that isn't the end.");
			return true;
		}
		ConfigurationService.END_SPAWN = new PersistableLocation(playerlocation);
		Config cfg = new Config(HCF.getPlugin(), "settings");
		cfg.set("end.spawn", ConfigurationService.END_SPAWN);
		cfg.save();
		sender.sendMessage(CoreConstants.YELLOW + "Sucessfully setted the endspawn location.");
		return true;

	}

}
