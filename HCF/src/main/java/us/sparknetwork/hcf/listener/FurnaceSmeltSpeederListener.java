package us.sparknetwork.hcf.listener;

import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.BrewingStand;
import org.bukkit.block.Furnace;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.scheduler.BukkitRunnable;
import us.sparknetwork.hcf.HCF;

public class FurnaceSmeltSpeederListener implements Listener {

	@EventHandler(ignoreCancelled = true, priority = EventPriority.MONITOR)
	public void onPlayerInteract(PlayerInteractEvent event) {
		if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
			Block block = event.getClickedBlock();
			BlockState state = block.getState();
			if (state instanceof Furnace) {
				// TODO: Set cook speed times 30
				((Furnace) state).setCookSpeedMultiplier(30);
			} else if (state instanceof BrewingStand) {
				this.startUpdate((BrewingStand) state, 10);
			}
		}
	}
	

	private void startUpdate(final BrewingStand tile, final int increase) {
		new BukkitRunnable() {
			public void run() {
				if (tile.getBrewingTime() > 10 && tile.getBrewingTime() < 399) {
					tile.setBrewingTime((short) (tile.getBrewingTime() - increase));
					tile.update();
				}
				this.cancel();
			}
		}.runTaskTimer(HCF.getPlugin(), 1L, 1L);
	}
}
