package us.sparknetwork.hcf.listener.fixes;

import org.bukkit.World.Environment;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.weather.WeatherChangeEvent;

public class WeatherFixListener implements Listener {

	@EventHandler
	public void onWeatherChange(WeatherChangeEvent e) {
		if (e.getWorld().getEnvironment() == Environment.NORMAL && e.toWeatherState()) {
			e.setCancelled(true);
		}

	}

}
