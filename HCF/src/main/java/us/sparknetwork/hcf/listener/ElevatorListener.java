package us.sparknetwork.hcf.listener;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Minecart;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.event.vehicle.VehicleEnterEvent;

public class ElevatorListener implements Listener {

	@EventHandler
	public void onMinecart(VehicleEnterEvent event) {
		if (!(event.getVehicle() instanceof Minecart) || !(event.getEntered() instanceof Player))
			return;

		Player p = (Player) event.getEntered();
		Location l = event.getVehicle().getLocation();
		Location loc = new Location(p.getWorld(), l.getBlockX(), l.getBlockY(), l.getBlockZ());
		Material m = loc.getBlock().getType();

		if (!m.equals(Material.FENCE_GATE) && !m.equals(Material.SIGN_POST))
			return;

		event.setCancelled(true);

		if (!p.isSneaking())
			return;

		p.teleport(this.teleportSpot(loc, loc.getBlockY(), 254));
	}

	@EventHandler
	public void onSign(PlayerInteractEvent e) {
		Player player = e.getPlayer();
		Block block = e.getClickedBlock();
		if (e.getAction() == Action.RIGHT_CLICK_BLOCK && block.getState() instanceof Sign) {
			Sign sign = (Sign) block.getState();
			String[] lines = sign.getLines();
			if (lines[1].contains("[Elevator]")) {
				if (lines[2].contains("Up")) {
					player.teleport(this.teleportSpot(block.getLocation(), block.getLocation().getBlockY(), 254),
							TeleportCause.PLUGIN);
				} else if (lines[2].contains("Down")) {
					player.teleport(this.teleportSpot(block.getLocation(), 1, block.getLocation().getBlockY()), TeleportCause.PLUGIN);
				}

			}
		}
	}

	public Location teleportSpot(Location loc, int min, int max) {
		int k = min;

		while (k < max) {
			Material m1 = new Location(loc.getWorld(), loc.getBlockX(), k + 0.5D, loc.getBlockZ()).getBlock().getType();
			Material m2 = new Location(loc.getWorld(), loc.getBlockX(), k + 1 + 0.5D, loc.getBlockZ()).getBlock()
					.getType();

			if (m1.equals(Material.AIR) && m2.equals(Material.AIR)) {
				return new Location(loc.getWorld(), loc.getBlockX(), k + 0.5D, loc.getBlockZ());
			}

			++k;
		}

		return new Location(loc.getWorld(), loc.getBlockX(),
				loc.getWorld().getHighestBlockYAt(loc.getBlockX(), loc.getBlockZ()) + 0.5D, loc.getBlockZ());
	}

	public Location teleportSpotDown(Location loc, int min) {
		int c = loc.getBlockY();
		while (loc.getBlockY() > min) {
			Material m1 = new Location(loc.getWorld(), loc.getBlockX(), c + 1, loc.getBlockZ()).getBlock().getType();
			Material m2 = new Location(loc.getWorld(), loc.getBlockX(), c, loc.getBlockZ()).getBlock().getType();
			if(m1.equals(m2) && m1.equals(Material.AIR)) {
				return new Location(loc.getWorld(),loc.getBlockX(),c+0.5, loc.getBlockZ());
			}
			
			c++;
		}
		
		return loc;
	}

}