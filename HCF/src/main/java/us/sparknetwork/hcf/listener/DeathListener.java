package us.sparknetwork.hcf.listener;

import java.util.concurrent.TimeUnit;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

import us.sparknetwork.core.CoreConstants;
import us.sparknetwork.core.economy.EconomyManager;
import us.sparknetwork.hcf.ConfigurationService;
import us.sparknetwork.hcf.HCF;
import us.sparknetwork.hcf.faction.struct.Role;
import us.sparknetwork.hcf.faction.type.Faction;
import us.sparknetwork.hcf.faction.type.PlayerFaction;
import us.sparknetwork.hcf.user.FactionUser;
import us.sparknetwork.util.JavaUtils;

public class DeathListener implements Listener {

	private final HCF plugin;

	public DeathListener(HCF plugin) {
		this.plugin = plugin;
	}

	@EventHandler(ignoreCancelled = true, priority = EventPriority.LOWEST)
	public void onPlayerDeathKillIncrement(PlayerDeathEvent event) {
		Player killer = event.getEntity().getKiller();
		if (killer != null) {
			FactionUser user = plugin.getUserManager().getUser(killer.getUniqueId());
			user.setKills(user.getKills() + 1);
		}
	}

	private static final long BASE_REGEN_DELAY = ConfigurationService.BASE_REGEN_DELAY;

	@SuppressWarnings("deprecation")
	@EventHandler(ignoreCancelled = true, priority = EventPriority.MONITOR)
	public void onPlayerDeath(PlayerDeathEvent event) {
		Player player = event.getEntity();
		PlayerFaction playerFaction = plugin.getFactionManager().getPlayerFaction(player);
		if (playerFaction != null) {
			Faction factionAt = plugin.getFactionManager().getFactionAt(player.getLocation());
			double dtrLoss = (1.0D * factionAt.getDtrLossMultiplier());
			double newDtr = playerFaction.setDeathsUntilRaidable(playerFaction.getDeathsUntilRaidable() - dtrLoss);

			Role role = playerFaction.getMember(player.getUniqueId()).getRole();
			playerFaction.setRemainingRegenerationTime(
					BASE_REGEN_DELAY + (playerFaction.getOnlinePlayers().size() * TimeUnit.MINUTES.toMillis(2L)));
			playerFaction.broadcast(CoreConstants.GOLD + "Member Death: " + ConfigurationService.TEAMMATE_COLOUR
					+ role.getAstrix() + player.getName() + CoreConstants.GOLD + ". " + "DTR: (" + ChatColor.WHITE
					+ JavaUtils.format(newDtr, 2) + '/'
					+ JavaUtils.format(playerFaction.getMaximumDeathsUntilRaidable(), 2) + CoreConstants.GOLD + ").");
		}
		if (Bukkit.spigot().getTPS()[0] > 15) { // Prevent unnecessary lag during prime times.
			for (Player target : Bukkit.getOnlinePlayers()) {
				if (plugin.getUserManager().getUser(target.getUniqueId()).isShowLightning()) {
					player.getWorld().strikeLightningEffect(player.getLocation());
					target.playSound(target.getLocation(), Sound.AMBIENCE_THUNDER, 1.0F, 1.0F);
				}
			}
		}
		if (ConfigurationService.KIT_MAP) {
			Player killer = event.getEntity().getKiller();
			killer.sendMessage(CoreConstants.YELLOW + String.format("You killed the player %1$s and you received %2$s.",
					player.getName(), ConfigurationService.BALANCE_PER_KILL));
			EconomyManager.getInstance().addBalance(killer.getUniqueId(), ConfigurationService.BALANCE_PER_KILL);
		}

	}

}
