package us.sparknetwork.hcf.listener;

import java.util.Collection;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import com.google.common.collect.ImmutableSet;

import us.sparknetwork.core.CoreConstants;
import us.sparknetwork.hcf.HCF;
import us.sparknetwork.hcf.faction.event.FactionChatEvent;
import us.sparknetwork.hcf.faction.struct.ChatChannel;
import us.sparknetwork.hcf.faction.type.Faction;
import us.sparknetwork.hcf.faction.type.PlayerFaction;

public class ChatListener implements Listener {

	private static final String EOTW_CAPPER_PREFIX = CoreConstants.YELLOW + "★ ";
	private static final ImmutableSet<UUID> EOTW_CAPPERS;

	static {
		ImmutableSet.Builder<UUID> builder = ImmutableSet.<UUID>builder();

		EOTW_CAPPERS = builder.build();
	}

	private final HCF plugin;

	public ChatListener(HCF plugin) {
		this.plugin = plugin;

	}

	@EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
	public void onPlayerChat(AsyncPlayerChatEvent event) {
		String message = event.getMessage();
		Player player = event.getPlayer();

		PlayerFaction playerFaction = plugin.getFactionManager().getPlayerFaction(player);
		ChatChannel chatChannel = playerFaction == null ? ChatChannel.PUBLIC
				: playerFaction.getMember(player).getChatChannel();

		// Handle faction or alliance chat modes.
		Set<Player> recipients = event.getRecipients();
		if (chatChannel == ChatChannel.FACTION || chatChannel == ChatChannel.ALLIANCE) {
			if (isGlobalChannel(message)) { // allow players to use '!' to bypass friendly chat.
				message = message.substring(1, message.length()).trim();
				event.setMessage(message);
			} else {
				Collection<Player> online = playerFaction.getOnlinePlayers();
				if (chatChannel == ChatChannel.ALLIANCE) {
					Collection<PlayerFaction> allies = playerFaction.getAlliedFactions();
					for (PlayerFaction ally : allies) {
						online.addAll(ally.getOnlinePlayers());
					}
				}

				recipients.retainAll(online);
				event.setFormat(chatChannel.getRawFormat(player));

				Bukkit.getPluginManager().callEvent(
						new FactionChatEvent(true, playerFaction, player, chatChannel, recipients, event.getMessage()));
				return;
			}
		}

		event.setCancelled(true);

		ConsoleCommandSender console = Bukkit.getConsoleSender();
		console.sendMessage(this.getFormattedMessage(player, playerFaction, player.getDisplayName(), event.getFormat(),
				message, console));
		for (Player recipient : event.getRecipients()) {
			recipient.sendMessage(this.getFormattedMessage(player, playerFaction, player.getDisplayName(),
					event.getFormat(), message, recipient));
		}
	}

	private String getFormattedMessage(Player player, PlayerFaction playerFaction, String playerDisplayName,
			String format, String message, CommandSender viewer) {
		String tag = playerFaction == null ? ChatColor.RED + Faction.FACTIONLESS_PREFIX
				: playerFaction.getDisplayName(viewer);
		return ChatColor.DARK_GRAY + "[" + tag + ChatColor.DARK_GRAY + "] "
				+ (EOTW_CAPPERS.contains(player.getUniqueId()) ? EOTW_CAPPER_PREFIX : "")
				+ String.format(format, playerDisplayName, message);
	}

	/**
	 * Checks if a message should be posted in {@link ChatChannel#PUBLIC}.
	 *
	 * @param input
	 *            the message to check
	 * @return true if the message should be posted in {@link ChatChannel#PUBLIC}
	 */
	private boolean isGlobalChannel(String input) {
		int length = input.length();
		if (length <= 1 || !input.startsWith("!")) {
			return false;
		}

		for (int i = 1; i < length; i++) {
			char character = input.charAt(i);
			if (character == ' ')
				continue;
			if (character == '/') {
				return false;
			} else {
				break;
			}
		}

		return true;
	}
}
