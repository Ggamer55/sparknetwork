package us.sparknetwork.hcf.listener;

import com.google.common.collect.Lists;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.event.player.PlayerDropItemEvent;

import org.bukkit.event.player.PlayerPortalEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import us.sparknetwork.core.CoreConstants;
import us.sparknetwork.hcf.ConfigurationService;
import us.sparknetwork.hcf.DateTimeFormats;
import us.sparknetwork.hcf.HCF;

import java.util.Arrays;
import java.util.List;

public class KitMapListener implements Listener {

	final HCF plugin;

	public KitMapListener(HCF plugin) {
		this.plugin = plugin;
	}

	@EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
	public void onCreatureSpawn(CreatureSpawnEvent event) {
		if (ConfigurationService.KIT_MAP) {
			event.setCancelled(true);
		}
	}

	@EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
	public void onPlayerDropItem(PlayerDropItemEvent event) {
		if (ConfigurationService.KIT_MAP
				&& plugin.getFactionManager().getFactionAt(event.getItemDrop().getLocation()).isSafezone()) {
			event.getItemDrop().remove();
		}
	}

	@EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
	public void onItemSpawn(ItemSpawnEvent event) {
		if (ConfigurationService.KIT_MAP && plugin.getFactionManager().getFactionAt(event.getLocation()).isSafezone()) {
			event.getEntity().remove();
		}
	}

    public static class EventSignListener implements Listener {

        private final static String EVENT_SIGN_ITEM_NAME = CoreConstants.GOLD + "Event Sign";

        public static ItemStack getEventSign(String playerName, String kothName) {
            ItemStack stack = new ItemStack(Material.SIGN, 1);
            ItemMeta meta = stack.getItemMeta();
            meta.setDisplayName(EVENT_SIGN_ITEM_NAME);
            meta.setLore(Lists.newArrayList(ChatColor.GREEN + playerName, ChatColor.WHITE + "captured by", ChatColor.GREEN + kothName,
                    DateTimeFormats.DAY_MTH_HR_MIN_SECS.format(System.currentTimeMillis())));
            stack.setItemMeta(meta);
            return stack;
        }

        @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGH)
        public void onSignChange(SignChangeEvent event) {
            if (isEventSign(event.getBlock())) {
                event.setCancelled(true);
            }
        }

        @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
        public void onBlockBreak(BlockBreakEvent event) {
            Block block = event.getBlock();
            if (isEventSign(block)) {
                BlockState state = block.getState();
                Sign sign = (Sign) state;
                ItemStack stack = new ItemStack(Material.SIGN, 1);
                ItemMeta meta = stack.getItemMeta();
                meta.setDisplayName(EVENT_SIGN_ITEM_NAME);
                meta.setLore(Arrays.asList(sign.getLines()));
                stack.setItemMeta(meta);

                Player player = event.getPlayer();
                World world = player.getWorld();
                Location blockLocation = block.getLocation();
                if (player.getGameMode() != GameMode.CREATIVE && world.isGameRule("doTileDrops")) {
                    world.dropItemNaturally(blockLocation, stack);
                }

                event.setCancelled(true);
                block.setType(Material.AIR);
                state.update();
            }
        }

        @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
        public void onBlockPlace(BlockPlaceEvent event) {
            ItemStack stack = event.getItemInHand();
            BlockState state = event.getBlock().getState();
            if (state instanceof Sign && stack.hasItemMeta()) {
                ItemMeta meta = stack.getItemMeta();
                if (meta.hasDisplayName() && meta.getDisplayName().equals(EVENT_SIGN_ITEM_NAME)) {
                    Sign sign = (Sign) state;
                    List<String> lore = meta.getLore();
                    int count = 0;
                    for (String loreLine : lore) {
                        sign.setLine(count++, loreLine);
                        if (count == 4)
                            break;
                    }

                    sign.update();

                    //TODO: Set sign not editable
                    //sign.setEditible(false);
                }
            }
        }

        private boolean isEventSign(Block block) {
            BlockState state = block.getState();
            if (state instanceof Sign) {
                String[] lines = ((Sign) state).getLines();
                return lines.length > 0 && lines[1] != null && lines[1].equals(ChatColor.WHITE + "captured by");
            }

            return false;
        }
    }

    public static class EndListener implements Listener {

        private final Location endExitLocation = ConfigurationService.END_EXIT.toLocation(); // TODO: Configurable
        private final Location endSpawnLocation = ConfigurationService.END_SPAWN.toLocation();

        @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
        public void onPlayerPortal(PlayerPortalEvent event) {
            if (event.getCause() == PlayerTeleportEvent.TeleportCause.END_PORTAL) {
                if (event.getTo().getWorld().getEnvironment() == World.Environment.THE_END) {
                    event.setTo(endSpawnLocation);
                } else if (event.getFrom().getWorld().getEnvironment() == World.Environment.THE_END) {
                    event.setTo(this.endExitLocation);
                }
            }
        }
    }
}
