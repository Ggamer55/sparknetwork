package us.sparknetwork.hcf.listener;

import org.bukkit.enchantments.Enchantment;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import net.md_5.bungee.api.ChatColor;
import us.sparknetwork.core.CoreConstants;
import us.sparknetwork.hcf.ConfigurationService;
import us.sparknetwork.util.BukkitUtils;

public class JoinMessageListener implements Listener {

	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		e.getPlayer().sendMessage(ChatColor.DARK_GRAY + BukkitUtils.STRAIGHT_LINE_DEFAULT);
		e.getPlayer().sendMessage(CoreConstants.YELLOW + String.format(
				"Welcome to " + CoreConstants.GOLD + ChatColor.BOLD.toString() + "%1$s" + ChatColor.RED + " [Map %2$s]",
				ConfigurationService.TITLE, ConfigurationService.MAP_NUMBER));
		String allies = ConfigurationService.MAX_ALLIES_PER_FACTION == 0 ? "No"
				: ConfigurationService.MAX_ALLIES_PER_FACTION + "";
		e.getPlayer().sendMessage(
				String.format(CoreConstants.GOLD + "* " + CoreConstants.YELLOW + "Faction size:" + CoreConstants.GRAY
						+ " %1$s Man / %2$s Allies", ConfigurationService.MAX_MEMBERS_PER_FACTION, allies));
		String Sharpnesslimit = ConfigurationService.ENCHANTMENT_LIMITS.get(Enchantment.DAMAGE_ALL) + "";
		String Protectionlimit = ConfigurationService.ENCHANTMENT_LIMITS.get(Enchantment.PROTECTION_ENVIRONMENTAL) + "";
		e.getPlayer().sendMessage(String.format(CoreConstants.GOLD + "* " + CoreConstants.YELLOW + "Map Kit: "
				+ CoreConstants.GRAY + "Protection %1$s / Sharpness %1$s", Protectionlimit, Sharpnesslimit));
		e.getPlayer()
				.sendMessage(String.format(
						CoreConstants.GOLD + "* " + CoreConstants.YELLOW + "Website: " + CoreConstants.GRAY + "%1$s",
						CoreConstants.SITE));
		e.getPlayer()
				.sendMessage(String.format(
						CoreConstants.GOLD + "* " + CoreConstants.YELLOW + "Teamspeak: " + CoreConstants.GRAY + "%1$s",
						CoreConstants.TEAMSPEAK));
		e.getPlayer().sendMessage(ChatColor.DARK_GRAY + BukkitUtils.STRAIGHT_LINE_DEFAULT);
	}

}
