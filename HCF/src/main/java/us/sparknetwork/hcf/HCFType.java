package us.sparknetwork.hcf;

import lombok.Getter;

import java.util.concurrent.TimeUnit;

public enum HCFType {

    KITMAP(TimeUnit.MINUTES.toMillis(30), 0, 0),
    EASY(TimeUnit.HOURS.toMillis(3), TimeUnit.HOURS.toMillis(1), TimeUnit.HOURS.toMillis(1)),
    NORMAL(TimeUnit.HOURS.toMillis(2), TimeUnit.MINUTES.toMillis(30), TimeUnit.HOURS.toMillis(1) + TimeUnit.MINUTES.toMillis(30)),
    HARD(TimeUnit.HOURS.toMillis(1), TimeUnit.MINUTES.toMillis(20), TimeUnit.HOURS.toMillis(2)),
    INSANE(TimeUnit.MINUTES.toMillis(30), TimeUnit.MINUTES.toMillis(20), TimeUnit.HOURS.toMillis(3)),
    EXTREME(TimeUnit.MINUTES.toMillis(30), TimeUnit.MINUTES.toMillis(10), TimeUnit.HOURS.toMillis(4));

    @Getter
    private long SOTWTime;
    @Getter
    private long PVPProtTime;
    @Getter
    private long DeathbanTime;

    HCFType(long SOTW, long PvpProt, long Deathban) {
        SOTWTime = SOTW;
        PVPProtTime = PvpProt;
        DeathbanTime = Deathban;
    }
}
