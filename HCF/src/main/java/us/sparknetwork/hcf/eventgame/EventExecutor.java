package us.sparknetwork.hcf.eventgame;

import java.util.Arrays;

import us.sparknetwork.hcf.HCF;
import us.sparknetwork.hcf.eventgame.argument.EventCancelArgument;
import us.sparknetwork.hcf.eventgame.argument.EventCreateArgument;
import us.sparknetwork.hcf.eventgame.argument.EventDeleteArgument;
import us.sparknetwork.hcf.eventgame.argument.EventRenameArgument;
import us.sparknetwork.hcf.eventgame.argument.EventSetAreaArgument;
import us.sparknetwork.hcf.eventgame.argument.EventSetCapzoneArgument;
import us.sparknetwork.hcf.eventgame.argument.EventStartArgument;
import us.sparknetwork.hcf.eventgame.argument.EventUptimeArgument;
import us.sparknetwork.core.commands.ArgumentExecutor;

/**
 * Handles the execution and tab completion of the event command.
 */
public class EventExecutor extends ArgumentExecutor {

	public EventExecutor(HCF plugin) {
		super("event");
		this.setAliases(Arrays.asList("game"));
		addArgument(new EventCancelArgument(plugin));
		addArgument(new EventCreateArgument(plugin));
		addArgument(new EventDeleteArgument(plugin));
		addArgument(new EventRenameArgument(plugin));
		addArgument(new EventSetAreaArgument(plugin));
		addArgument(new EventSetCapzoneArgument(plugin));
		//addArgument(new EventSetLootArgument(plugin));
		addArgument(new EventStartArgument(plugin));
		addArgument(new EventUptimeArgument(plugin));
	}
}