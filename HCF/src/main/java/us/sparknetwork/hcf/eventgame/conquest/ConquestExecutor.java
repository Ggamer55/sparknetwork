package us.sparknetwork.hcf.eventgame.conquest;

import us.sparknetwork.hcf.HCF;
import us.sparknetwork.core.commands.ArgumentExecutor;

public class ConquestExecutor extends ArgumentExecutor {

    public ConquestExecutor(HCF plugin) {
        super("conquest");
        addArgument(new ConquestSetpointsArgument(plugin));
    }
}
