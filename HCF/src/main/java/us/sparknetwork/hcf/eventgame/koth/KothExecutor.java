package us.sparknetwork.hcf.eventgame.koth;

import org.bukkit.command.CommandSender;

import us.sparknetwork.hcf.HCF;
import us.sparknetwork.hcf.eventgame.koth.argument.KothHelpArgument;
import us.sparknetwork.hcf.eventgame.koth.argument.KothNextArgument;
import us.sparknetwork.hcf.eventgame.koth.argument.KothScheduleArgument;
import us.sparknetwork.hcf.eventgame.koth.argument.KothSetCapDelayArgument;
import us.sparknetwork.core.commands.ArgumentExecutor;

/**
 * Command used to handle KingOfTheHills.
 */
public class KothExecutor extends ArgumentExecutor {

	private final KothScheduleArgument kothScheduleArgument;

	public KothExecutor(HCF plugin) {
		super("koth");

		addArgument(new KothHelpArgument(this));
		addArgument(new KothNextArgument(plugin));
		addArgument(this.kothScheduleArgument = new KothScheduleArgument(plugin));
		addArgument(new KothSetCapDelayArgument(plugin));
	}

	@Override
	public boolean run(CommandSender sender, String[] args) {
		if (args.length < 1) {
			this.kothScheduleArgument.onCommand(sender, this, label, args);
			return true;
		}

		return super.run(sender, args);
	}
}
