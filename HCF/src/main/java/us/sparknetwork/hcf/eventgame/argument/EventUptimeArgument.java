package us.sparknetwork.hcf.eventgame.argument;

import org.apache.commons.lang.time.DurationFormatUtils;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import us.sparknetwork.core.CoreConstants;
import us.sparknetwork.hcf.DateTimeFormats;
import us.sparknetwork.hcf.HCF;
import us.sparknetwork.hcf.eventgame.EventTimer;
import us.sparknetwork.hcf.eventgame.faction.EventFaction;
import us.sparknetwork.core.commands.CommandArgument;

/**
 * A {@link CommandArgument} argument used for checking the uptime of current
 * event.
 */
public class EventUptimeArgument extends CommandArgument {

	private final HCF plugin;

	public EventUptimeArgument(HCF plugin) {
		super("uptime", "Check the uptime of an event");
		this.plugin = plugin;
		this.permission = "hcf.command.event.argument." + getName();
	}

	@Override
	public String getUsage(String label) {
		return '/' + label + ' ' + getName();
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		EventTimer eventTimer = plugin.getTimerManager().getEventTimer();

		if (eventTimer.getRemaining() <= 0L) {
			sender.sendMessage(ChatColor.RED + "There is not a running event.");
			return true;
		}

		EventFaction eventFaction = eventTimer.getEventFaction();
		sender.sendMessage(CoreConstants.YELLOW + "Up-time of " + eventTimer.getName() + " timer"
				+ (eventFaction == null ? ""
						: ": " + ChatColor.BLUE + '(' + eventFaction.getDisplayName(sender) + ChatColor.BLUE + ')')
				+ CoreConstants.YELLOW + " is " + CoreConstants.GRAY
				+ DurationFormatUtils.formatDurationWords(eventTimer.getUptime(), true, true) + CoreConstants.YELLOW
				+ ", started at " + CoreConstants.GOLD
				+ DateTimeFormats.HR_MIN_AMPM_TIMEZONE.format(eventTimer.getStartStamp()) + CoreConstants.YELLOW + '.');

		return true;
	}
}
