package us.sparknetwork.hcf.timer;

import us.sparknetwork.hcf.HCF;
import us.sparknetwork.hcf.timer.argument.TimerCheckArgument;
import us.sparknetwork.hcf.timer.argument.TimerSetArgument;
import us.sparknetwork.core.commands.ArgumentExecutor;

/**
 * Handles the execution and tab completion of the timer command.
 */
public class TimerExecutor extends ArgumentExecutor {

    public TimerExecutor(HCF plugin) {
        super("timer");

        addArgument(new TimerCheckArgument(plugin));
        addArgument(new TimerSetArgument(plugin));
    }
}