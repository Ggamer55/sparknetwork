package us.sparknetwork.hcf.scoreboard.provider;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.google.common.collect.Ordering;

import us.sparknetwork.core.CoreConstants;
import us.sparknetwork.core.economy.EconomyManager;
import us.sparknetwork.core.user.User;
import us.sparknetwork.core.user.UserManager;
import us.sparknetwork.hcf.ConfigurationService;
import us.sparknetwork.hcf.DateTimeFormats;
import us.sparknetwork.hcf.DurationFormatter;
import us.sparknetwork.hcf.HCF;
import us.sparknetwork.hcf.eventgame.EventTimer;
import us.sparknetwork.hcf.eventgame.eotw.EotwHandler;
import us.sparknetwork.hcf.eventgame.faction.ConquestFaction;
import us.sparknetwork.hcf.eventgame.faction.EventFaction;
import us.sparknetwork.hcf.eventgame.faction.KothFaction;
import us.sparknetwork.hcf.eventgame.tracker.ConquestTracker;
import us.sparknetwork.hcf.faction.type.PlayerFaction;
import us.sparknetwork.hcf.pvpclass.PvpClass;
import us.sparknetwork.hcf.pvpclass.archer.ArcherClass;
import us.sparknetwork.hcf.pvpclass.archer.ArcherMark;
import us.sparknetwork.hcf.pvpclass.bard.BardClass;
import us.sparknetwork.hcf.scoreboard.SidebarEntry;
import us.sparknetwork.hcf.scoreboard.SidebarProvider;
import us.sparknetwork.hcf.sotw.SotwTimer;
import us.sparknetwork.hcf.timer.PlayerTimer;
import us.sparknetwork.hcf.timer.Timer;
import us.sparknetwork.util.BukkitUtils;

public class TimerSidebarProvider implements SidebarProvider {

	public static final ThreadLocal<DecimalFormat> CONQUEST_FORMATTER = new ThreadLocal<DecimalFormat>() {
		@Override
		protected DecimalFormat initialValue() {
			return new DecimalFormat("00.0");
		}
	};
	protected static String STRAIGHT_LINE = BukkitUtils.STRAIGHT_LINE_DEFAULT.substring(0, 14);

	private static final Comparator<Map.Entry<UUID, ArcherMark>> ARCHER_MARK_COMPARATOR = (o1, o2) -> o1.getValue()
			.compareTo(o2.getValue());

	private final HCF plugin;

	public TimerSidebarProvider(HCF plugin) {
		this.plugin = plugin;
	}

	public SidebarEntry add(String s) {

		if (s.length() < 10) {
			return new SidebarEntry(s);
		}

		if (s.length() > 10 && s.length() < 20) {
			return new SidebarEntry(s.substring(0, 10), s.substring(10, s.length()), "");
		}

		if (s.length() > 20) {
			return new SidebarEntry(s.substring(0, 10), s.substring(10, 20), s.substring(20, s.length()));
		}

		return null;
	}

	@Override
	public String getTitle() {
		return ConfigurationService.SCOREBOARD_TITLE;
	}

	@SuppressWarnings("deprecation")
	@Override
	public List<SidebarEntry> getLines(Player player) {
		List<SidebarEntry> lines = new ArrayList<>();
		if (!ConfigurationService.SCOREBOARD_ENABLED) {
			return lines;
		}
		User user = (User) UserManager.getInstance().getUser(player.getUniqueId());
		EotwHandler.EotwRunnable eotwRunnable = plugin.getEotwHandler().getRunnable();
		if (eotwRunnable != null) {
			long remaining = eotwRunnable.getMillisUntilStarting();
			if (remaining > 0L) {
				lines.add(
						new SidebarEntry(ChatColor.RED.toString() + ChatColor.BOLD, "EOTW" + ChatColor.RED + " starts",
								" in " + ChatColor.BOLD + DurationFormatter.getRemaining(remaining, true)));
			} else if ((remaining = eotwRunnable.getMillisUntilCappable()) > 0L) {
				lines.add(new SidebarEntry(ChatColor.RED.toString() + ChatColor.BOLD,
						"EOTW" + ChatColor.RED + " cappable",
						" in " + ChatColor.BOLD + DurationFormatter.getRemaining(remaining, true)));
			}
		}

		SotwTimer.SotwRunnable sotwRunnable = plugin.getSotwTimer().getSotwRunnable();
		if (sotwRunnable != null) {
			lines.add(new SidebarEntry(ChatColor.GREEN.toString() + ChatColor.BOLD, "SOTW", CoreConstants.GRAY + ": "
					+ ChatColor.WHITE + DurationFormatter.getRemaining(sotwRunnable.getRemaining(), true)));
		}

		EventTimer eventTimer = plugin.getTimerManager().getEventTimer();
		List<SidebarEntry> conquestLines = null;

		EventFaction eventFaction = eventTimer.getEventFaction();
		if (eventFaction instanceof KothFaction) {
			lines.add(new SidebarEntry(eventTimer.getScoreboardPrefix(),
					eventFaction.getScoreboardName() + CoreConstants.GRAY,
					": " + ChatColor.WHITE + DurationFormatter.getRemaining(eventTimer.getRemaining(), true)));
		} else if (eventFaction instanceof ConquestFaction) {
			ConquestFaction conquestFaction = (ConquestFaction) eventFaction;
			CONQUEST_FORMATTER.get();

			conquestLines = new ArrayList<>();
			conquestLines.add(new SidebarEntry(ChatColor.BLUE.toString(),
					ChatColor.BOLD + conquestFaction.getName() + CoreConstants.GRAY, ":"));

			conquestLines.add(new SidebarEntry(
					"  " + ChatColor.RED.toString() + conquestFaction.getRed().getScoreboardRemaining(),
					ChatColor.RESET + " ",
					CoreConstants.YELLOW.toString() + conquestFaction.getYellow().getScoreboardRemaining()));

			conquestLines.add(new SidebarEntry(
					"  " + ChatColor.GREEN.toString() + conquestFaction.getGreen().getScoreboardRemaining(),
					ChatColor.RESET + " " + ChatColor.RESET,
					ChatColor.AQUA.toString() + conquestFaction.getBlue().getScoreboardRemaining()));

			// Show the top 3 factions next.
			ConquestTracker conquestTracker = (ConquestTracker) conquestFaction.getEventType().getEventTracker();
			int count = 0;
			for (Map.Entry<PlayerFaction, Integer> entry : conquestTracker.getFactionPointsMap().entrySet()) {
				String factionName = entry.getKey().getName();
				if (factionName.length() > 14)
					factionName = factionName.substring(0, 14);
				conquestLines.add(new SidebarEntry(ChatColor.RED, ChatColor.BOLD + factionName,
						CoreConstants.GRAY + ": " + ChatColor.WHITE + entry.getValue()));
				if (++count == 3)
					break;
			}
		}

		// Show the current PVP Class statistics of the player.
		PvpClass pvpClass = plugin.getPvpClassManager().getEquippedClass(player);
		if (pvpClass != null) {
			lines.add(new SidebarEntry(CoreConstants.YELLOW, "Current Class" + CoreConstants.GRAY + ": ",
					ChatColor.GREEN + pvpClass.getName()));
			if (pvpClass instanceof BardClass) {
				BardClass bardClass = (BardClass) pvpClass;
				lines.add(
						new SidebarEntry(CoreConstants.GOLD + " \u00bb ", ChatColor.AQUA + "Energy", CoreConstants.GRAY
								+ ": " + ChatColor.WHITE + handleBardFormat(bardClass.getEnergyMillis(player), true)));

				long remaining = bardClass.getRemainingBuffDelay(player);
				if (remaining > 0) {
					lines.add(new SidebarEntry(CoreConstants.GOLD + " \u00bb ", ChatColor.AQUA + "Buff Delay",
							CoreConstants.GRAY + ": " + ChatColor.WHITE
									+ DurationFormatter.getRemaining(remaining, true)));
				}
			} else if (pvpClass instanceof ArcherClass) {
				ArcherClass archerClass = (ArcherClass) pvpClass;

				List<Map.Entry<UUID, ArcherMark>> entryList = Ordering.from(ARCHER_MARK_COMPARATOR)
						.sortedCopy(archerClass.getSentMarks(player).entrySet());
				entryList = entryList.subList(0, Math.min(entryList.size(), 3));
				for (Map.Entry<UUID, ArcherMark> entry : entryList) {
					ArcherMark archerMark = entry.getValue();
					Player target = Bukkit.getPlayer(entry.getKey());
					if (target != null) {
						ChatColor levelColour;
						switch (archerMark.currentLevel) {
						case 1:
							levelColour = ChatColor.GREEN;
							break;
						case 2:
							levelColour = ChatColor.RED;
							break;
						case 3:
							levelColour = ChatColor.DARK_RED;
							break;

						default:
							levelColour = CoreConstants.YELLOW;
							break;
						}

						// Add the current mark level to scoreboard.
						// Needs to be re-written. May be &6&lArcher Mark&7:
						// >>(/u00bb) Interlagos [Mark 1]
						// *NOTE THIS IS EXPERIMENTAL MAY CAUSE ERRORS THEREFORE NOT COMMITING TO MAIN!
						String targetName = target.getName();
						targetName = targetName.substring(0, Math.min(targetName.length(), 15));
						lines.add(new SidebarEntry(CoreConstants.GOLD + "" + ChatColor.BOLD,
								"Archer Mark" + CoreConstants.GRAY + ": ", ""));
						lines.add(new SidebarEntry(CoreConstants.GOLD + " \u00bb" + ChatColor.RED, ' ' + targetName,
								CoreConstants.YELLOW.toString() + levelColour + " [Mark " + archerMark.currentLevel
										+ ']'));
					}
				}
			}
		}

		Collection<Timer> timers = plugin.getTimerManager().getTimers();
		for (Timer timer : timers) {
			if (timer instanceof PlayerTimer) {
				PlayerTimer playerTimer = (PlayerTimer) timer;
				long remaining = playerTimer.getRemaining(player);
				if (remaining <= 0)
					continue;

				String timerName = playerTimer.getName();
				if (timerName.length() > 14)
					timerName = timerName.substring(0, timerName.length());
				lines.add(new SidebarEntry(playerTimer.getScoreboardPrefix(), timerName + CoreConstants.GRAY,
						": " + ChatColor.WHITE + DurationFormatter.getRemaining(remaining, true)));
			}
		}

		if (conquestLines != null && !conquestLines.isEmpty()) {
			if (!lines.isEmpty()) {
				conquestLines.add(new SidebarEntry(" ", "  ", " "));
			}

			conquestLines.addAll(lines);
			lines = conquestLines;
		}
		if (ConfigurationService.KIT_MAP) {
			Integer Playerbalance = (int) EconomyManager.getInstance().getBalance(player.getUniqueId());
			lines.add(add(CoreConstants.GOLD + "» " + CoreConstants.YELLOW + "§l Statistics"));
			lines.add(add(CoreConstants.GOLD + String.format("Kills:" + CoreConstants.YELLOW + " %1$s",
					HCF.getPlugin().getUserManager().getUser(player.getUniqueId()).getKills())));
			lines.add(add(CoreConstants.GOLD + String.format("Deaths:" + CoreConstants.YELLOW + " %1$s",
					HCF.getPlugin().getUserManager().getUser(player.getUniqueId()).getDeaths())));
			lines.add(add(CoreConstants.GOLD + "Balance:"
					+ String.format("" + CoreConstants.YELLOW + " %1$s", Playerbalance)));
		}
		if (player.hasPermission("network.staff") && user.isStaffMode()) {
			String vanished = user.isVanished() ? "§atrue" : "§cfalse";
			String chatchannel = !user.isStaffChat() ? "§aGlobal Chat" : "§cStaff Chat";
			lines.add(add(CoreConstants.YELLOW + ChatColor.BOLD.toString() + "StaffMode" + CoreConstants.GRAY + ":"));
			lines.add(
					add(CoreConstants.GOLD + String.format("» " + CoreConstants.YELLOW + "Vanished: %1$s", vanished)));
			lines.add(add(CoreConstants.GOLD + String.format("» " + CoreConstants.YELLOW + "Gamemode: %1$s",
					"§a" + player.getGameMode().toString())));
			lines.add(add(CoreConstants.GOLD
					+ String.format("» " + CoreConstants.YELLOW + "Chat Channel: %1$s", chatchannel)));
		}
		if (!lines.isEmpty()) {
			lines.add(0, new SidebarEntry(CoreConstants.GRAY, STRAIGHT_LINE, STRAIGHT_LINE));
			lines.add(lines.size(),
					new SidebarEntry(CoreConstants.GRAY, ChatColor.STRIKETHROUGH + STRAIGHT_LINE, STRAIGHT_LINE));
		}

		return lines;
	}

	private static String handleBardFormat(long millis, boolean trailingZero) {
		return (trailingZero ? DateTimeFormats.REMAINING_SECONDS_TRAILING : DateTimeFormats.REMAINING_SECONDS).get()
				.format(millis * 0.001);
	}
}
