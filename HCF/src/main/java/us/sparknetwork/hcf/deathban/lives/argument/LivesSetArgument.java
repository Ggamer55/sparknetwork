package us.sparknetwork.hcf.deathban.lives.argument;

import java.util.Collections;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import us.sparknetwork.core.CoreConstants;
import us.sparknetwork.hcf.HCF;
import us.sparknetwork.util.BukkitUtils;
import us.sparknetwork.util.JavaUtils;
import us.sparknetwork.core.commands.CommandArgument;

/**
 * An {@link CommandArgument} used to set the lives of {@link Player}s.
 */
public class LivesSetArgument extends CommandArgument {

	private final HCF plugin;

	public LivesSetArgument(HCF plugin) {
		super("set", "Set how much lives a player has");
		this.plugin = plugin;
		this.permission = "hcf.command.lives.argument." + getName();
	}

	@Override
	public String getUsage(String label) {
		return '/' + label + ' ' + getName() + " <playerName> <amount>";
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (args.length < 3) {
			sender.sendMessage(ChatColor.RED + "Usage: " + getUsage(label));
			return true;
		}

		Integer amount = JavaUtils.tryParseInt(args[2]);

		if (amount == null) {
			sender.sendMessage(ChatColor.RED + "'" + args[2] + "' is not a number.");
			return true;
		}

		OfflinePlayer target = BukkitUtils.offlinePlayerWithNameOrUUID(args[1]);

		if (!target.hasPlayedBefore() && !target.isOnline()) {
			sender.sendMessage(String.format(CoreConstants.PLAYER_WITH_NAME_OR_UUID, args[1]));
			return true;
		}

		plugin.getUserManager().getUser(target.getUniqueId()).setLives(amount);
		sender.sendMessage(CoreConstants.YELLOW + target.getName() + " now has " + CoreConstants.GOLD + amount
				+ CoreConstants.YELLOW + " lives.");
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
		return args.length == 2 ? null : Collections.<String>emptyList();
	}
}
