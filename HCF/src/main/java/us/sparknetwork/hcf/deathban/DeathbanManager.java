package us.sparknetwork.hcf.deathban;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.bukkit.entity.Player;

public interface DeathbanManager {

	long MAX_DEATHBAN_TIME = TimeUnit.HOURS.toMillis(8);

	/**
	 * Gets the deathban multiplier for a {@link Player}.
	 *
	 * @param player
	 *            the {@link Player} to get for
	 * @return the deathban multiplier
	 */
	double getDeathBanMultiplier(Player player);

	/**
	 * Applies a {@link Deathban} to a {@link Player}.
	 *
	 * @param player
	 *            the {@link Player} to apply to
	 * @param reason
	 *            the reason for {@link Deathban}
	 * @return the {@link Deathban} that has been applied
	 */
	Deathban applyDeathBan(Player player, String reason);

	/**
	 * Applies a {@link Deathban} to a {@link Player}.
	 *
	 * @param uuid
	 *            the uuid of player to apply to
	 * @param deathban
	 *            the {@link Deathban} to be applied
	 * @return the {@link Deathban} that has been applied
	 */
	Deathban applyDeathBan(UUID uuid, Deathban deathban);

}
