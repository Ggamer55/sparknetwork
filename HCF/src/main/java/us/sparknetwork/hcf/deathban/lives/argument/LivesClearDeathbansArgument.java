package us.sparknetwork.hcf.deathban.lives.argument;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import us.sparknetwork.core.CoreConstants;
import us.sparknetwork.hcf.HCF;
import us.sparknetwork.hcf.deathban.Deathban;
import us.sparknetwork.hcf.user.FactionUser;
import us.sparknetwork.core.commands.CommandArgument;

/**
 * An {@link CommandArgument} used to clear all {@link Deathban}s.
 */
public class LivesClearDeathbansArgument extends CommandArgument {

	private final HCF plugin;

	public LivesClearDeathbansArgument(HCF plugin) {
		super("cleardeathbans", "Clears the global deathbans");
		this.plugin = plugin;
		this.aliases = new String[] { "resetdeathbans" };
		this.permission = "hcf.command.lives.argument." + getName();
	}

	@Override
	public String getUsage(String label) {
		return '/' + label + ' ' + getName();
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		for (FactionUser user : plugin.getUserManager().getUsers().values()) {
			user.setDeathban(null);
		}

		Command.broadcastCommandMessage(sender, CoreConstants.YELLOW + "All death-bans have been cleared.");
		return true;
	}
}
