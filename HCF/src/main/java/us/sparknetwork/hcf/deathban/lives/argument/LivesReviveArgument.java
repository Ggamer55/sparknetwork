package us.sparknetwork.hcf.deathban.lives.argument;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import us.sparknetwork.core.CoreConstants;
import us.sparknetwork.core.CorePlugin;
import us.sparknetwork.core.commands.CommandArgument;
import us.sparknetwork.core.server.ServerSettings;
import us.sparknetwork.hcf.ConfigurationService;
import us.sparknetwork.hcf.HCF;
import us.sparknetwork.hcf.deathban.Deathban;
import us.sparknetwork.hcf.faction.struct.Relation;
import us.sparknetwork.hcf.faction.type.PlayerFaction;
import us.sparknetwork.hcf.user.FactionUser;

import java.util.*;

/**
 * An {@link CommandArgument} used to revive {@link Deathban}ned
 * {@link Player}s.
 */
public class LivesReviveArgument extends CommandArgument {

    private static final String REVIVE_BYPASS_PERMISSION = "hcf.revive.bypass";

    private final HCF plugin;

    public LivesReviveArgument(HCF plugin) {
        super("revive", "Revive a death-banned player");
        this.plugin = plugin;
        this.permission = "hcf.command.lives.argument." + getName();
    }

    @Override
    public String getUsage(String label) {
        return '/' + label + ' ' + getName() + " <playerName>";
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length < 2) {
            sender.sendMessage(ChatColor.RED + "Usage: " + getUsage(label));
            return true;
        }

        OfflinePlayer target = Bukkit.getOfflinePlayer(args[1]); // TODO: breaking

        if (!target.hasPlayedBefore() && !target.isOnline()) {
            sender.sendMessage(
                    CoreConstants.GOLD + "Player '" + ChatColor.WHITE + args[1] + CoreConstants.GOLD + "' not found.");
            return true;
        }

        UUID targetUUID = target.getUniqueId();
        FactionUser factionTarget = plugin.getUserManager().getUser(targetUUID);
        Deathban deathban = factionTarget.getDeathban();

        if (deathban == null || !deathban.isActive()) {
            sender.sendMessage(ChatColor.RED + target.getName() + " is not death-banned.");
            return true;
        }

        Relation relation = Relation.ENEMY;
        if (sender instanceof Player) {
            if (!sender.hasPermission(REVIVE_BYPASS_PERMISSION)) {
                if (ConfigurationService.KIT_MAP) {
                    sender.sendMessage(ChatColor.RED + "You cannot revive players during a kit map.");
                    return true;
                }

                if (plugin.getEotwHandler().isEndOfTheWorld()) {
                    sender.sendMessage(ChatColor.RED + "You cannot revive players during EOTW.");
                    return true;
                }
            }

            Player player = (Player) sender;
            UUID playerUUID = player.getUniqueId();
            int selfLives = plugin.getUserManager().getUser(playerUUID).getLives();

            if (selfLives <= 0) {
                sender.sendMessage(ChatColor.RED + "You do not have any lives.");
                return true;
            }

            plugin.getUserManager().getUser(playerUUID).setLives(selfLives - 1);
            PlayerFaction playerFaction = plugin.getFactionManager().getPlayerFaction(player);
            relation = playerFaction == null ? Relation.ENEMY
                    : playerFaction.getFactionRelation(plugin.getFactionManager().getPlayerFaction(targetUUID));
            sender.sendMessage(CoreConstants.YELLOW + "You have used a life to revive " + relation.toChatColour()
                    + target.getName() + CoreConstants.YELLOW + '.');
        } else {
            sender.sendMessage(CoreConstants.YELLOW + "You have revived " + ConfigurationService.ENEMY_COLOUR
                    + target.getName() + CoreConstants.YELLOW + '.');
        }

        CorePlugin.getPlugin().getMessager().sendMessageToPlayer(args[1], relation.toChatColour() + sender.getName() + CoreConstants.GOLD + " has just revived you from "
                + ServerSettings.NAME + CoreConstants.GOLD + '.');

        factionTarget.setDeathban(null);
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        if (args.length != 2) {
            return Collections.emptyList();
        }

        List<String> results = new ArrayList<>();
        Collection<FactionUser> factionUsers = plugin.getUserManager().getUsers().values();
        for (FactionUser factionUser : factionUsers) {
            Deathban deathban = factionUser.getDeathban();
            if (deathban == null || !deathban.isActive())
                continue;

            OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(factionUser.getUserUUID());
            String offlineName = offlinePlayer.getName();
            if (offlineName != null) {
                results.add(offlinePlayer.getName());
            }
        }

        return results;
    }
}
