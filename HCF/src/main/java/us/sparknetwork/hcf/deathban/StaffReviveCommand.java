package us.sparknetwork.hcf.deathban;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import us.sparknetwork.api.command.CommandModule;
import us.sparknetwork.core.CoreConstants;
import us.sparknetwork.core.CorePlugin;
import us.sparknetwork.core.server.ServerSettings;
import us.sparknetwork.hcf.HCF;
import us.sparknetwork.hcf.user.FactionUser;
import us.sparknetwork.util.BukkitUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class StaffReviveCommand extends CommandModule implements TabCompleter {

    private final HCF plugin;

    public StaffReviveCommand(HCF plugin) {
        super("staffrevive", 1, 1, "Usage /(command) <player>", "revive");
        this.plugin = plugin;
    }

    @Override
    public boolean run(CommandSender sender, String[] args) {
        if (args.length < 1) {
            sender.sendMessage(ChatColor.RED + "Usage: /" + getLabel() + " <playerName>");
            return true;
        }

        OfflinePlayer target = Bukkit.getOfflinePlayer(args[0]); // TODO: breaking

        if (!target.hasPlayedBefore() && !target.isOnline()) {
            sender.sendMessage(
                    CoreConstants.GOLD + "Player '" + ChatColor.WHITE + args[0] + CoreConstants.GOLD + "' not found.");
            return true;
        }

        UUID targetUUID = target.getUniqueId();
        FactionUser factionTarget = HCF.getPlugin().getUserManager().getUser(targetUUID);
        Deathban deathban = factionTarget.getDeathban();

        if (deathban == null || !deathban.isActive()) {
            sender.sendMessage(ChatColor.RED + target.getName() + " is not death-banned.");
            return true;
        }

        factionTarget.setDeathban(null);
        Command.broadcastCommandMessage(sender, CoreConstants.YELLOW + "Staff revived " + target.getName() + ".");
        CorePlugin.getPlugin().getMessager().sendMessageToPlayer(target.getName(), String.format("A staff revived you in the server %1$s.", ServerSettings.NAME));

        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        if (args.length != 1) {
            return Collections.emptyList();
        }

        List<String> results = new ArrayList<>();
        for (FactionUser factionUser : plugin.getUserManager().getUsers().values()) {
            Deathban deathban = factionUser.getDeathban();
            if (deathban != null && deathban.isActive()) {
                OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(factionUser.getUserUUID());
                String name = offlinePlayer.getName();
                if (name != null) {
                    results.add(name);
                }
            }
        }

        return BukkitUtils.getCompletions(args, results);
    }
}
