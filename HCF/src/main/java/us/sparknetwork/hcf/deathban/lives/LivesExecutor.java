package us.sparknetwork.hcf.deathban.lives;

import us.sparknetwork.hcf.HCF;
import us.sparknetwork.hcf.deathban.lives.argument.LivesCheckArgument;
import us.sparknetwork.hcf.deathban.lives.argument.LivesCheckDeathbanArgument;
import us.sparknetwork.hcf.deathban.lives.argument.LivesClearDeathbansArgument;
import us.sparknetwork.hcf.deathban.lives.argument.LivesGiveArgument;
import us.sparknetwork.hcf.deathban.lives.argument.LivesReviveArgument;
import us.sparknetwork.hcf.deathban.lives.argument.LivesSetArgument;
import us.sparknetwork.hcf.deathban.lives.argument.LivesSetDeathbanTimeArgument;
import us.sparknetwork.core.commands.ArgumentExecutor;

/**
 * Handles the execution and tab completion of the lives command.
 */
public class LivesExecutor extends ArgumentExecutor {

	public LivesExecutor(HCF plugin) {
		super("lives");

		addArgument(new LivesCheckArgument(plugin));
		addArgument(new LivesCheckDeathbanArgument(plugin));
		addArgument(new LivesClearDeathbansArgument(plugin));
		addArgument(new LivesGiveArgument(plugin));
		addArgument(new LivesReviveArgument(plugin));
		addArgument(new LivesSetArgument(plugin));
		addArgument(new LivesSetDeathbanTimeArgument());
	}
}