package us.sparknetwork.hcf.deathban;

import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import us.sparknetwork.hcf.ConfigurationService;
import us.sparknetwork.hcf.HCF;
import us.sparknetwork.hcf.faction.type.Faction;
import us.sparknetwork.util.Config;
import us.sparknetwork.util.PersistableLocation;

public class FlatFileDeathbanManager implements DeathbanManager {

	private static final int MAX_DEATHBAN_MULTIPLIER = 300;

	private final HCF plugin;

	public FlatFileDeathbanManager(HCF plugin) {
		this.plugin = plugin;
	}


	@Override
	public double getDeathBanMultiplier(Player player) {
		for (int i = 5; i < MAX_DEATHBAN_MULTIPLIER; i++) {
			if (player.hasPermission("hcf.deathban.multiplier." + i)) {
				return (i) / 100.0;
			}
		}

		return 1.0D;
	}

	@Override
	public Deathban applyDeathBan(Player player, String reason) {
		Location location = player.getLocation();
		Faction factionAt = plugin.getFactionManager().getFactionAt(location);
		long duration = ConfigurationService.HCF_DIFFICULTY.getDeathbanTime();

		Config cfg =  new Config(plugin,"settings");
		for(String key : cfg.getConfigurationSection("deathban.times").getKeys(false)) {
			if(player.hasPermission(cfg.getString("deathban.times."+key+".permission"))) {
				duration = cfg.getLong("deathban.times."+key+".millis");
			}
		}

		if (!factionAt.isDeathban()) {
			duration /= 2; // non-deathban factions should be 50% quicker
		}

		duration *= getDeathBanMultiplier(player);
		duration *= factionAt.getDeathbanMultiplier();
		return applyDeathBan(player.getUniqueId(),
				new Deathban(reason, Math.min(MAX_DEATHBAN_TIME, duration), new PersistableLocation(location)));
	}

	@Override
	public Deathban applyDeathBan(UUID uuid, Deathban deathban) {
		plugin.getUserManager().getUser(uuid).setDeathban(deathban);
		return deathban;
	}
}
