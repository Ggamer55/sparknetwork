package us.sparknetwork.hcf;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.bukkit.ChatColor;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import com.doctordark.internal.com.doctordark.base.BasePlugin;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;

import lombok.Getter;
import us.sparknetwork.api.command.CommandModule;
import us.sparknetwork.core.CoreConstants;
import us.sparknetwork.core.CorePlugin;
import us.sparknetwork.core.commands.ReflectionCommandHandler;
import us.sparknetwork.core.economy.EconomyManager;
import us.sparknetwork.hcf.combatlog.CombatLogListener;
import us.sparknetwork.hcf.combatlog.CustomEntityRegistration;
import us.sparknetwork.hcf.command.*;
import us.sparknetwork.hcf.deathban.Deathban;
import us.sparknetwork.hcf.deathban.DeathbanListener;
import us.sparknetwork.hcf.deathban.DeathbanManager;
import us.sparknetwork.hcf.deathban.FlatFileDeathbanManager;
import us.sparknetwork.hcf.deathban.StaffReviveCommand;
import us.sparknetwork.hcf.deathban.lives.LivesExecutor;
import us.sparknetwork.hcf.economy.EconomyCommand;
import us.sparknetwork.hcf.economy.PayCommand;
import us.sparknetwork.hcf.economy.ShopSignListener;
import us.sparknetwork.hcf.economy.SpawnerShopListener;
import us.sparknetwork.hcf.eventgame.CaptureZone;
import us.sparknetwork.hcf.eventgame.EventExecutor;
import us.sparknetwork.hcf.eventgame.EventScheduler;
import us.sparknetwork.hcf.eventgame.conquest.ConquestExecutor;
import us.sparknetwork.hcf.eventgame.crate.KeyManager;
import us.sparknetwork.hcf.eventgame.eotw.EotwCommand;
import us.sparknetwork.hcf.eventgame.eotw.EotwHandler;
import us.sparknetwork.hcf.eventgame.eotw.EotwListener;
import us.sparknetwork.hcf.eventgame.faction.CapturableFaction;
import us.sparknetwork.hcf.eventgame.faction.ConquestFaction;
import us.sparknetwork.hcf.eventgame.faction.KothFaction;
import us.sparknetwork.hcf.eventgame.koth.KothExecutor;
import us.sparknetwork.hcf.faction.FactionExecutor;
import us.sparknetwork.hcf.faction.FactionManager;
import us.sparknetwork.hcf.faction.FactionMember;
import us.sparknetwork.hcf.faction.FlatFileFactionManager;
import us.sparknetwork.hcf.faction.claim.Claim;
import us.sparknetwork.hcf.faction.claim.ClaimHandler;
import us.sparknetwork.hcf.faction.claim.ClaimWandListener;
import us.sparknetwork.hcf.faction.claim.Subclaim;
import us.sparknetwork.hcf.faction.claim.SubclaimWandListener;
import us.sparknetwork.hcf.faction.type.ClaimableFaction;
import us.sparknetwork.hcf.faction.type.EndPortalFaction;
import us.sparknetwork.hcf.faction.type.Faction;
import us.sparknetwork.hcf.faction.type.PlayerFaction;
import us.sparknetwork.hcf.faction.type.RoadFaction;
import us.sparknetwork.hcf.faction.type.SpawnFaction;
import us.sparknetwork.hcf.listener.AutoSmeltOreListener;
import us.sparknetwork.hcf.listener.BookDeenchantListener;
import us.sparknetwork.hcf.listener.BorderListener;
import us.sparknetwork.hcf.listener.BottledExpListener;
import us.sparknetwork.hcf.listener.ChatListener;
import us.sparknetwork.hcf.listener.CoreListener;
import us.sparknetwork.hcf.listener.CrowbarListener;
import us.sparknetwork.hcf.listener.DeathListener;
import us.sparknetwork.hcf.listener.DeathMessageListener;
import us.sparknetwork.hcf.listener.DeathSignListener;
import us.sparknetwork.hcf.listener.ElevatorListener;
import us.sparknetwork.hcf.listener.EntityLimitListener;
import us.sparknetwork.hcf.listener.ExpMultiplierListener;
import us.sparknetwork.hcf.listener.FactionListener;
import us.sparknetwork.hcf.listener.FoundDiamondsListener;
import us.sparknetwork.hcf.listener.FurnaceSmeltSpeederListener;
import us.sparknetwork.hcf.listener.JoinMessageListener;
import us.sparknetwork.hcf.listener.KitMapListener;
import us.sparknetwork.hcf.listener.PortalListener;
import us.sparknetwork.hcf.listener.ProtectionListener;
import us.sparknetwork.hcf.listener.SignSubclaimListener;
import us.sparknetwork.hcf.listener.SkullListener;
import us.sparknetwork.hcf.listener.WorldListener;
import us.sparknetwork.hcf.listener.fixes.BeaconStrengthFixListener;
import us.sparknetwork.hcf.listener.fixes.BlockHitFixListener;
import us.sparknetwork.hcf.listener.fixes.BlockJumpGlitchFixListener;
import us.sparknetwork.hcf.listener.fixes.BoatGlitchFixListener;
import us.sparknetwork.hcf.listener.fixes.CreativeClickListener;
import us.sparknetwork.hcf.listener.fixes.EnchantLimitListener;
import us.sparknetwork.hcf.listener.fixes.EnderChestRemovalListener;
import us.sparknetwork.hcf.listener.fixes.InfinityArrowFixListener;
import us.sparknetwork.hcf.listener.fixes.PearlGlitchListener;
import us.sparknetwork.hcf.listener.fixes.PotionLimitListener;
import us.sparknetwork.hcf.listener.fixes.VoidGlitchFixListener;
import us.sparknetwork.hcf.listener.fixes.WeatherFixListener;
import us.sparknetwork.hcf.pvpclass.PvpClassManager;
import us.sparknetwork.hcf.pvpclass.bard.EffectRestorer;
import us.sparknetwork.hcf.reclaim.ReclaimManager;
import us.sparknetwork.hcf.scoreboard.ScoreboardHandler;
import us.sparknetwork.hcf.sotw.SotwCommand;
import us.sparknetwork.hcf.sotw.SotwListener;
import us.sparknetwork.hcf.sotw.SotwTimer;
import us.sparknetwork.hcf.timer.TimerExecutor;
import us.sparknetwork.hcf.timer.TimerManager;
import us.sparknetwork.hcf.user.FactionUser;
import us.sparknetwork.hcf.user.UserManager;
import us.sparknetwork.hcf.visualise.ProtocolLibHook;
import us.sparknetwork.hcf.visualise.VisualiseHandler;
import us.sparknetwork.hcf.visualise.WallBorderListener;
import us.sparknetwork.util.BukkitAndNMSUtils;

public class HCF extends JavaPlugin {

	@Getter
	private static HCF plugin;

	@Getter
	private Random random = new Random();

	@Getter
	private ClaimHandler claimHandler;

	@Getter
	private CombatLogListener combatLogListener;

	@Getter
	private DeathbanManager deathbanManager;

	@Getter
	private EconomyManager economyManager;

	@Getter
	private EffectRestorer effectRestorer;

	@Getter
	private EotwHandler eotwHandler;

	@Getter
	private ReclaimManager reclaimManager;

	@Getter
	private EventScheduler eventScheduler;

	@Getter
	private FactionManager factionManager;

	@Getter
	private FoundDiamondsListener foundDiamondsListener;

	@Getter
	private KeyManager keyManager;

	@Getter
	private PvpClassManager pvpClassManager;

	@Getter
	private ScoreboardHandler scoreboardHandler;

	@Getter
	private SotwTimer sotwTimer;

	@Getter
	private TimerManager timerManager;

	@Getter
	private UserManager userManager;

	@Getter
	private VisualiseHandler visualiseHandler;

	@Getter
	private WorldEditPlugin worldEdit;

	@Getter
	private ExtraConfiguration extraConfiguration;

	@Override
	public void onEnable() {
		BasePlugin.getPlugin().init(this);

		HCF.plugin = this;
		ProtocolLibHook.hook(this);

		Plugin wep = getServer().getPluginManager().getPlugin("WorldEdit");
		this.worldEdit = wep instanceof WorldEditPlugin && wep.isEnabled() ? (WorldEditPlugin) wep : null;

		if(!this.getDataFolder().exists()) {
			this.getDataFolder().mkdirs();
		}
		
		
				
		(this.extraConfiguration = new ExtraConfiguration(this)).reload();

		ConfigurationService.init(this);
		CustomEntityRegistration.registerCustomEntities();

		try {
			BukkitAndNMSUtils.getNMSClass("BlockEnderPortal").getField("a")
					.setBoolean(BukkitAndNMSUtils.getNMSClass("BlockEnderPortal"), true);
		} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException
				| ClassNotFoundException e) {
			e.printStackTrace();
		}

		this.effectRestorer = new EffectRestorer(this);
		this.registerConfiguration();
		this.registerCommands();
		this.registerManagers();
		this.registerListeners();

		new BukkitRunnable() {
			@Override
			public void run() {
				saveData();
			}
		}.runTaskTimerAsynchronously(plugin, TimeUnit.MINUTES.toMillis(20L), TimeUnit.MINUTES.toMillis(20L));
	}

	private void saveData() {
		this.economyManager.saveEconomyData();
		this.factionManager.saveFactionData();
		//this.keyManager.saveKeyData();
		this.timerManager.saveTimerData();
		this.userManager.saveUserData();
	}

	@Override
	public void onDisable() {
		CombatLogListener.removeCombatLoggers();
		this.pvpClassManager.onDisable();
		this.scoreboardHandler.clearBoards();
		this.foundDiamondsListener.saveConfig(); // temporary
		this.saveData();

		HCF.plugin = null; // always initialise last
	}

	private void registerConfiguration() {
		ConfigurationSerialization.registerClass(CaptureZone.class);
		ConfigurationSerialization.registerClass(Deathban.class);
		ConfigurationSerialization.registerClass(Claim.class);
		ConfigurationSerialization.registerClass(Subclaim.class);
		ConfigurationSerialization.registerClass(Deathban.class);
		ConfigurationSerialization.registerClass(FactionUser.class);
		ConfigurationSerialization.registerClass(ClaimableFaction.class);
		ConfigurationSerialization.registerClass(ConquestFaction.class);
		ConfigurationSerialization.registerClass(CapturableFaction.class);
		ConfigurationSerialization.registerClass(KothFaction.class);
		ConfigurationSerialization.registerClass(EndPortalFaction.class);
		ConfigurationSerialization.registerClass(Faction.class);
		ConfigurationSerialization.registerClass(FactionMember.class);
		ConfigurationSerialization.registerClass(PlayerFaction.class);
		ConfigurationSerialization.registerClass(RoadFaction.class);
		ConfigurationSerialization.registerClass(SpawnFaction.class);
		ConfigurationSerialization.registerClass(RoadFaction.NorthRoadFaction.class);
		ConfigurationSerialization.registerClass(RoadFaction.EastRoadFaction.class);
		ConfigurationSerialization.registerClass(RoadFaction.SouthRoadFaction.class);
		ConfigurationSerialization.registerClass(RoadFaction.WestRoadFaction.class);
	}

	private void registerListeners() {
		PluginManager manager = this.getServer().getPluginManager();
		manager.registerEvents(new AutoSmeltOreListener(), this);
		manager.registerEvents(new BlockHitFixListener(), this);
		manager.registerEvents(new BlockJumpGlitchFixListener(), this);
		manager.registerEvents(new BoatGlitchFixListener(), this);
		manager.registerEvents(new BookDeenchantListener(), this);
		manager.registerEvents(new BorderListener(), this);
		manager.registerEvents(new BottledExpListener(), this);
		manager.registerEvents(new ChatListener(this), this);
		manager.registerEvents(new ClaimWandListener(this), this);
		manager.registerEvents(this.combatLogListener = new CombatLogListener(this), this);
		manager.registerEvents(new CoreListener(this), this);
		manager.registerEvents(new CreativeClickListener(), this);
		manager.registerEvents(new CrowbarListener(this), this);
		manager.registerEvents(new DeathListener(this), this);
		manager.registerEvents(new DeathMessageListener(this), this);
		manager.registerEvents(new DeathSignListener(this), this);
		manager.registerEvents(new DeathbanListener(this), this);
		manager.registerEvents(new EnchantLimitListener(), this);
		manager.registerEvents(new EnderChestRemovalListener(), this);
		manager.registerEvents(new EntityLimitListener(), this);
		manager.registerEvents(new KitMapListener.EndListener(), this);
		manager.registerEvents(new EotwListener(this), this);
		manager.registerEvents(new KitMapListener.EventSignListener(), this);
		manager.registerEvents(new ExpMultiplierListener(), this);
		manager.registerEvents(new FactionListener(this), this);
		manager.registerEvents(this.foundDiamondsListener = new FoundDiamondsListener(this), this);
		manager.registerEvents(new FurnaceSmeltSpeederListener(), this);
		manager.registerEvents(new InfinityArrowFixListener(), this);
		// manager.registerEvents(new KeyListener(this), this); Disabled for make better key system
		manager.registerEvents(new KitMapListener(this), this);
		manager.registerEvents(new PearlGlitchListener(this), this);
		manager.registerEvents(new PortalListener(this), this);
		manager.registerEvents(new PotionLimitListener(), this);
		manager.registerEvents(new ProtectionListener(this), this);
		manager.registerEvents(new SubclaimWandListener(this), this);
		manager.registerEvents(new SignSubclaimListener(this), this);
		manager.registerEvents(new ShopSignListener(this), this);
		manager.registerEvents(new SkullListener(), this);
		manager.registerEvents(new SotwListener(this), this);
		manager.registerEvents(new BeaconStrengthFixListener(), this);
		manager.registerEvents(new VoidGlitchFixListener(), this);
		manager.registerEvents(new WallBorderListener(this), this);
		manager.registerEvents(new WorldListener(this), this);
		manager.registerEvents(new JoinMessageListener(), this);
		manager.registerEvents(new WeatherFixListener(), this);
		manager.registerEvents(new ElevatorListener(), this);
		if (ConfigurationService.SPAWNERSHOP_ENABLED) {
			manager.registerEvents(new SpawnerShopListener(), this);
		}
	}

	private void registerCommands() {
		List<CommandModule> cmds = new ArrayList<>();
		ReflectionCommandHandler cmdhandler = new ReflectionCommandHandler(this);
		cmds.add(new AngleCommand());
		cmds.add(new GoppleCommand(this));
		cmds.add(new LocationCommand(this));
		cmds.add(new LogoutCommand(this));
		cmds.add(new PvpTimerCommand(this));
		cmds.add(new RegenCommand(this));
		cmds.add(new ServerTimeCommand());
		cmds.add(new SetWorldSizeCommand());
		cmds.add(new SpawnCannonCommand(this));
		cmds.add(new ToggleCapzoneEntryCommand(this));
		cmds.add(new ToggleLightningCommand(this));
		cmds.add(new ToggleSidebarCommand(this));
		cmds.add(new StaffReviveCommand(this));
		cmds.add(new LivesExecutor(this));
		cmds.add(new EconomyCommand(this));
		cmds.add(new PayCommand(this));
		cmds.add(new EventExecutor(this));
		cmds.add(new ConquestExecutor(this));
		cmds.add(new EotwCommand(this));
		cmds.add(new FactionExecutor(this));
		cmds.add(new KothExecutor(this));
		cmds.add(new SotwCommand(this));
		cmds.add(new TimerExecutor(this));
		cmds.add(new CoordsCommand(this));
		cmds.add(new HelpCommand(this));
		cmds.add(new ToggleLightningCommand.MapKitCommand(this));
		cmds.add(new GiveCrowbarCommand(this));
		cmds.add(new ReclaimCommand());
		cmds.add(new SetReclaimedCommand(this));
		cmds.add(new SetEndExitCommand());
		cmds.add(new SetEndSpawnCommand());
		cmds.add(new RestoreInventoryCommand(this));
		cmds.add(new CobbleCommand(this));
		cmds.add(new SpawnCommand());
		cmds.forEach(cmd -> cmd
				.setPermissionMessage(
						CoreConstants.GOLD + ChatColor.BOLD.toString() + CoreConstants.NAME + ChatColor.DARK_GRAY + "»"
								+ CoreConstants.GRAY + "You don't have permission to execute this command")
				.setPermission("hcf.command." + cmd.getName()));
		cmdhandler.registerAll(cmds);
	}

	private void registerManagers() {
		this.claimHandler = new ClaimHandler(this);
		this.deathbanManager = new FlatFileDeathbanManager(this);
		this.economyManager = CorePlugin.getInstance().getEconomyManager();
		this.eotwHandler = new EotwHandler(this);
		this.eventScheduler = new EventScheduler(this);
		this.factionManager = new FlatFileFactionManager(this);
		//this.keyManager = new KeyManager(this);
		this.pvpClassManager = new PvpClassManager(this);
		this.sotwTimer = new SotwTimer();
		this.timerManager = new TimerManager(this); // needs to be registered before ScoreboardHandler
		this.scoreboardHandler = new ScoreboardHandler(this);
		this.reclaimManager = new ReclaimManager(this);
		this.userManager = new UserManager(this);
		this.visualiseHandler = new VisualiseHandler();
	}
}
