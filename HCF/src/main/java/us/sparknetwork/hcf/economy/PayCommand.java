package us.sparknetwork.hcf.economy;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import us.sparknetwork.api.command.CommandModule;
import us.sparknetwork.core.CoreConstants;
import us.sparknetwork.core.economy.EconomyManager;
import us.sparknetwork.hcf.HCF;
import us.sparknetwork.util.JavaUtils;

/**
 * Command used to pay other {@link Player}s some money.
 */
public class PayCommand extends CommandModule implements TabCompleter {

	private final HCF plugin;

	public PayCommand(HCF plugin) {
		super("pay", 2, 2, "Usage /(command) <player> <amount>", Arrays.asList("p2p"));
		this.plugin = plugin;
	}

	@Override
	public boolean run(CommandSender sender, String[] args) {
		if (args.length < 2) {
			return false;
		}

		Integer amount = JavaUtils.tryParseInt(args[1]);

		if (amount == null) {
			sender.sendMessage(ChatColor.RED + "'" + args[1] + "' is not a valid number.");
			return true;
		}

		if (amount <= 0) {
			sender.sendMessage(ChatColor.RED + "You must send money in positive quantities.");
			return true;
		}

		// Calculate the senders balance here.
		Player senderPlayer = sender instanceof Player ? (Player) sender : null;
		double senderBalance = senderPlayer != null ? plugin.getEconomyManager().getBalance(senderPlayer.getUniqueId())
				: 1024;

		if (senderBalance < amount) {
			sender.sendMessage(ChatColor.RED + "You tried to pay " + EconomyManager.ECONOMY_SYMBOL + amount
					+ ", but you only have " + EconomyManager.ECONOMY_SYMBOL + senderBalance
					+ " in your bank account.");

			return true;
		}

		OfflinePlayer target = Bukkit.getOfflinePlayer(args[0]); // TODO: breaking

		if (sender.equals(target)) {
			sender.sendMessage(ChatColor.RED + "You cannot send money to yourself.");
			return true;
		}

		Player targetPlayer = target.getPlayer();

		if (!target.hasPlayedBefore() && targetPlayer == null) {
			sender.sendMessage(
					CoreConstants.GOLD + "Player '" + ChatColor.WHITE + args[0] + CoreConstants.GOLD + "' not found.");
			return true;
		}

		if (targetPlayer == null)
			return false; // won't happen, IntelliJ compiler won't ignore

		// Make the money transactions.
		if (senderPlayer != null)
			plugin.getEconomyManager().withdrawBalance(senderPlayer.getUniqueId(), amount);
		plugin.getEconomyManager().addBalance(targetPlayer.getUniqueId(), amount);

		targetPlayer.sendMessage(CoreConstants.YELLOW + sender.getName() + " has sent you " + CoreConstants.GOLD
				+ EconomyManager.ECONOMY_SYMBOL + amount + CoreConstants.YELLOW + '.');
		sender.sendMessage(CoreConstants.YELLOW + "You have sent " + CoreConstants.GOLD + EconomyManager.ECONOMY_SYMBOL
				+ amount + CoreConstants.YELLOW + " to " + target.getName() + '.');
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
		return args.length == 1 ? null : Collections.<String>emptyList();
	}
}
