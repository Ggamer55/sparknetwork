package us.sparknetwork.hcf.economy;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import us.sparknetwork.api.command.CommandModule;
import us.sparknetwork.core.CoreConstants;
import us.sparknetwork.core.economy.EconomyManager;
import us.sparknetwork.hcf.HCF;
import us.sparknetwork.util.BukkitUtils;
import us.sparknetwork.util.JavaUtils;

/**
 * Command used to check a players' balance.
 */
public class EconomyCommand extends CommandModule implements TabCompleter {

	// The max amount of players shown in '/bal top'.
	private static final int MAX_ENTRIES = 10;

	private final HCF plugin;

	public EconomyCommand(HCF plugin) {
		super("economy", 0, 3, "Usage /(command)", Arrays.asList("eco", "bal", "balance", "$"));
		this.plugin = plugin;
	}

	@Override
	public boolean run(CommandSender sender, String[] args) {
		OfflinePlayer target;
		if (args.length > 0 && sender.hasPermission(getPermission() + ".staff")) {
			target = BukkitUtils.offlinePlayerWithNameOrUUID(args[0]);
		} else if (sender instanceof Player) {
			target = (Player) sender;
		} else {
			sender.sendMessage(ChatColor.RED + "Usage: /" + getLabel() + " <playerName>");
			return true;
		}

		if (!target.hasPlayedBefore() && !target.isOnline()) {
			sender.sendMessage(String.format(CoreConstants.PLAYER_WITH_NAME_OR_UUID, args[0]));
			return true;
		}

		UUID uuid = target.getUniqueId();
		double balance = plugin.getEconomyManager().getBalance(uuid);

		if (args.length < 2) {
			sender.sendMessage(CoreConstants.GOLD
					+ (sender.equals(target) ? "Your balance" : "Balance of " + target.getName()) + " is "
					+ ChatColor.WHITE + EconomyManager.ECONOMY_SYMBOL + balance + CoreConstants.GOLD + '.');

			return true;
		}

		if (args[1].equalsIgnoreCase("give") || args[1].equalsIgnoreCase("add")) {
			if (args.length < 3) {
				sender.sendMessage(
						ChatColor.RED + "Usage: /" + getLabel() + ' ' + target.getName() + ' ' + args[1] + " <amount>");
				return true;
			}

			Integer amount = JavaUtils.tryParseInt(args[2]);

			if (amount == null) {
				sender.sendMessage(ChatColor.RED + "'" + args[2] + "' is not a valid number.");
				return true;
			}

			double newBalance = plugin.getEconomyManager().addBalance(uuid, amount).getNewBalance();
			sender.sendMessage(new String[] {
					CoreConstants.YELLOW + "Added " + EconomyManager.ECONOMY_SYMBOL + JavaUtils.format(amount)
							+ " to balance of " + target.getName() + '.',
					CoreConstants.YELLOW + "Balance of " + target.getName() + " is now " + EconomyManager.ECONOMY_SYMBOL
							+ newBalance + '.' });

			return true;
		}

		if (args[1].equalsIgnoreCase("take") || args[1].equalsIgnoreCase("negate") || args[1].equalsIgnoreCase("minus")
				|| args[1].equalsIgnoreCase("subtract")) {
			if (args.length < 3) {
				sender.sendMessage(
						ChatColor.RED + "Usage: /" + getLabel() + ' ' + target.getName() + ' ' + args[1] + " <amount>");
				return true;
			}

			Integer amount = JavaUtils.tryParseInt(args[2]);

			if (amount == null) {
				sender.sendMessage(ChatColor.RED + "'" + args[2] + "' is not a valid number.");
				return true;
			}

			double newBalance = plugin.getEconomyManager().withdrawBalance(uuid, amount).getNewBalance();

			sender.sendMessage(new String[] {
					CoreConstants.YELLOW + "Taken " + EconomyManager.ECONOMY_SYMBOL + JavaUtils.format(amount)
							+ " from balance of " + target.getName() + '.',
					CoreConstants.YELLOW + "Balance of " + target.getName() + " is now " + EconomyManager.ECONOMY_SYMBOL
							+ newBalance + '.' });

			return true;
		}

		if (args[1].equalsIgnoreCase("set")) {
			if (args.length < 3) {
				sender.sendMessage(
						ChatColor.RED + "Usage: /" + getLabel() + ' ' + target.getName() + ' ' + args[1] + " <amount>");
				return true;
			}

			Integer amount = JavaUtils.tryParseInt(args[2]);

			if (amount == null) {
				sender.sendMessage(ChatColor.RED + "'" + args[2] + "' is not a valid number.");
				return true;
			}

			double newBalance = plugin.getEconomyManager().setBalance(uuid, amount).getNewBalance();
			sender.sendMessage(CoreConstants.YELLOW + "Set balance of " + target.getName() + " to "
					+ EconomyManager.ECONOMY_SYMBOL + JavaUtils.format(newBalance) + '.');
			return true;
		}

		sender.sendMessage(CoreConstants.GOLD + (sender.equals(target) ? "Your balance" : "Balance of " + target.getName())
				+ " is " + ChatColor.WHITE + EconomyManager.ECONOMY_SYMBOL + balance + CoreConstants.GOLD + '.');

		return true;
	}

	private static final ImmutableList<String> COMPLETIONS_SECOND = ImmutableList.of("add", "set", "take");

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
		switch (args.length) {
		case 1:
			List<String> results = Lists.newArrayList("top");
			if (sender.hasPermission(command.getPermission() + ".staff")) {
				Player senderPlayer = sender instanceof Player ? (Player) sender : null;
				for (Player player : Bukkit.getOnlinePlayers()) {
					if (senderPlayer == null || senderPlayer.canSee(player)) {
						results.add(player.getName());
					}
				}
			}

			return BukkitUtils.getCompletions(args, results);
		case 2:
			if (!args[0].equals("top") && sender.hasPermission(command.getPermission() + ".staff")) {
				return BukkitUtils.getCompletions(args, COMPLETIONS_SECOND);
			}
		default:
			return Collections.emptyList();
		}
	}
}
