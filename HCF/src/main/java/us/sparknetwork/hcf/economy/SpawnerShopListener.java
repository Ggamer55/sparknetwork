package us.sparknetwork.hcf.economy;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import net.md_5.bungee.api.ChatColor;
import us.sparknetwork.core.CoreConstants;
import us.sparknetwork.core.economy.EconomyManager;
import us.sparknetwork.hcf.ConfigurationService;
import us.sparknetwork.hcf.HCF;
import us.sparknetwork.util.ItemBuilder;

public class SpawnerShopListener implements Listener {

	Inventory ssinv;
	ItemBuilder Spawner;

	public SpawnerShopListener() {
		this.ssinv = Bukkit.createInventory(null, 27, ChatColor.GREEN + "SpawnerShop");
		for (int i = 0; i < ssinv.getSize(); i++) {
			ssinv.setItem(i, new ItemBuilder(Material.STAINED_GLASS_PANE).data((short) 15).displayName(" ").build());
		}
		this.Spawner = new ItemBuilder(Material.MOB_SPAWNER)
				.displayName(ChatColor.GREEN + ChatColor.BOLD.toString() + "Spawner");
		ssinv.setItem(11, Spawner.lore("Skeleton", "Price: $" + ConfigurationService.SPAWNER_PRICE).build());
		ssinv.setItem(13, Spawner.lore("Spider", "Price: $" + ConfigurationService.SPAWNER_PRICE).build());
		ssinv.setItem(15, Spawner.lore("Zombie", "Price: $" + ConfigurationService.SPAWNER_PRICE).build());
	}

	@EventHandler(priority = EventPriority.HIGH)
	public void onPlayerInteract(PlayerInteractEvent e) {
		if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			Block block = e.getClickedBlock();
			BlockState state = block.getState();

			if (state instanceof Sign) {
				Sign sign = (Sign) state;
				String[] lines = sign.getLines();
				if (lines[1].contains(ChatColor.GREEN.toString() + "[SpawnerShop]")
						&& lines[2].equals(ChatColor.BLACK + "Click to buy")) {
					e.getPlayer().openInventory(this.ssinv);
				}
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGH)
	public void onSignPlace(BlockPlaceEvent e) {
		Block bl = e.getBlock();
		Player pl = e.getPlayer();
		if (bl.getState() instanceof Sign) {
			Sign si = (Sign) bl.getState();
			String[] lines = si.getLines();
			if (lines[0].contains(ChatColor.GREEN + "[SpawnerShop]")) {
				if (!pl.hasPermission("hcf.spawnershop.place")) {
					lines[2] = ChatColor.RED + "Error";
					pl.sendMessage(ChatColor.RED + ChatColor.BOLD.toString() + "SparkNetwork" + ChatColor.DARK_GRAY
							+ ChatColor.BOLD.toString() + "� " + ChatColor.RED
							+ "You don't have permissions to create SpawnerShops.");
					return;
				}
				lines[2] = ChatColor.BLACK + "Click to buy";
				pl.sendMessage(CoreConstants.YELLOW + "Sucessfully placed a SpawnerShop.");
			}
		}
	}

	@SuppressWarnings("deprecation")
	@EventHandler(priority = EventPriority.HIGH)
	public void onSpawnerShop(InventoryClickEvent e) {
		if (!(e.getWhoClicked() instanceof Player))
			return;
		Inventory ssinv = e.getClickedInventory();
		if(ssinv == null) {
			return;
		}
		if (ssinv.equals(this.ssinv)) {
			ItemStack is = e.getCurrentItem();
			e.setCancelled(true);
			if (is.getType() == Material.MOB_SPAWNER) {
				if (is.getItemMeta().getDisplayName().equals(ChatColor.GREEN + ChatColor.BOLD.toString() + "Spawner")) {
					if (is.getItemMeta().getLore().contains("Skeleton")) {
						Player target = (Player) e.getWhoClicked();
						if (EconomyManager.getInstance()
								.getBalance(target.getUniqueId()) >= ConfigurationService.SPAWNER_PRICE) {
							target.getInventory()
									.addItem(new ItemBuilder(Material.MOB_SPAWNER)
											.displayName(ChatColor.GREEN + ChatColor.BOLD.toString() + "Spawner")
											.lore("Skeleton").data(EntityType.SKELETON.getTypeId()).build());
							EconomyManager.getInstance().withdrawBalance(target.getUniqueId(),
									ConfigurationService.SPAWNER_PRICE);
							target.sendMessage(CoreConstants.YELLOW + "Sucessfully purchased a spawner.");
							this.closeInventory(target);
							return;
						}
						target.sendMessage(ChatColor.RED + "Cannot afford");
						this.closeInventory(target);
						return;
					}
					if (is.getItemMeta().getLore().contains("Spider")) {
						Player target = (Player) e.getWhoClicked();
						if (EconomyManager.getInstance()
								.getBalance(target.getUniqueId()) >= ConfigurationService.SPAWNER_PRICE) {
							target.getInventory()
									.addItem(new ItemBuilder(Material.MOB_SPAWNER)
											.displayName(ChatColor.GREEN + ChatColor.BOLD.toString() + "Spawner")
											.lore("Spider").data(EntityType.SPIDER.getTypeId()).build());
							EconomyManager.getInstance().withdrawBalance(target.getUniqueId(),
									ConfigurationService.SPAWNER_PRICE);
							target.sendMessage(CoreConstants.YELLOW + "Sucessfully purchased a spawner.");
							this.closeInventory(target);
							return;
						}
						target.sendMessage(ChatColor.RED + "Cannot afford");
						this.closeInventory(target);
						return;
					}
					if (is.getItemMeta().getLore().contains("Zombie")) {
						Player target = (Player) e.getWhoClicked();
						if (EconomyManager.getInstance()
								.getBalance(target.getUniqueId()) >= ConfigurationService.SPAWNER_PRICE) {
							target.getInventory()
									.addItem(new ItemBuilder(Material.MOB_SPAWNER)
											.displayName(ChatColor.GREEN + ChatColor.BOLD.toString() + "Spawner")
											.lore("Zombie").data(EntityType.ZOMBIE.getTypeId()).build());
							EconomyManager.getInstance().withdrawBalance(target.getUniqueId(),
									ConfigurationService.SPAWNER_PRICE);
							target.sendMessage(CoreConstants.YELLOW + "Sucessfully purchased a spawner.");
							this.closeInventory(target);
							return;
						}
						target.sendMessage(ChatColor.RED + "Cannot afford");
						this.closeInventory(target);
						return;
					}
					if (is.getItemMeta().getLore().contains("Cave Spider")) {
						Player target = (Player) e.getWhoClicked();
						if (EconomyManager.getInstance()
								.getBalance(target.getUniqueId()) >= ConfigurationService.SPAWNER_PRICE) {
							target.getInventory()
									.addItem(new ItemBuilder(Material.MOB_SPAWNER)
											.displayName(ChatColor.GREEN + ChatColor.BOLD.toString() + "Spawner")
											.lore("Cave Spider").data(EntityType.CAVE_SPIDER.getTypeId()).build());
							EconomyManager.getInstance().withdrawBalance(target.getUniqueId(),
									ConfigurationService.SPAWNER_PRICE);
							target.sendMessage(CoreConstants.YELLOW + "Sucessfully purchased a spawner.");
							this.closeInventory(target);
							return;
						}
						target.sendMessage(ChatColor.RED + "Cannot afford");
						this.closeInventory(target);
						return;
					}
				}
			}
		}
	}

	public void closeInventory(Player player) {
		new BukkitRunnable() {

			@Override
			public void run() {
				player.closeInventory();
			}

		}.runTaskLater(HCF.getPlugin(), 2L);
	}

}
