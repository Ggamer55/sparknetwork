package com.doctordark.internal.com.doctordark.base;

import java.io.IOException;
import java.util.Random;

import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.plugin.java.JavaPlugin;

import us.sparknetwork.core.CorePlugin;
import us.sparknetwork.util.ISignHandler;
import us.sparknetwork.util.SignHandler;
import us.sparknetwork.util.cuboid.Cuboid;
import us.sparknetwork.util.cuboid.NamedCuboid;
import us.sparknetwork.util.itemdb.ItemDb;
import us.sparknetwork.util.itemdb.SimpleItemDb;

public class BasePlugin {
    private Random random;

    private static BasePlugin plugin;
    private JavaPlugin javaPlugin;

    private BasePlugin() {
        this.random = new Random();
    }

    public void init(final JavaPlugin plugin) {
        this.javaPlugin = plugin;

    } 

    public void disable() {
        this.javaPlugin = null;
        BasePlugin.plugin = null;
    }

    public Random getRandom() {
        return this.random;
    }

    public ItemDb getItemDb() {
        return CorePlugin.getInstance().getItemDb();
    }

    public ISignHandler getSignHandler() {
        return CorePlugin.getInstance().getSignHandler();
    }

    public static BasePlugin getPlugin() {
        return BasePlugin.plugin;
    }

    public JavaPlugin getJavaPlugin() {
        return this.javaPlugin;
    }

    static {
        BasePlugin.plugin = new BasePlugin();
    }
}
